# Chameleon Solution

## Overview

This solution design is based on a simplified version of the Microservices artchitecture. Microservices architecture involves a number of small, independent microservices that exchange messages among themselves. In the Visual Studio solution, Microservices exist in the Services solution folder. Each Microservice is contained in a dedicated solution sub-folder. An example of a pre-build Microservice is Identity. This is the Microservice responsible for identity, authorisation and authentication.

Each Microservice may have its own WebAPI and/or BackgroundService program. The former to be a typical web API, and the latter to be an always running program that handles background tasks and jobs.

### Stack

The primary technology stack for the solution is defined below:

- .Net Core / C#
- SQL Server and Entity Framework
- Ionic
- AngularJS
- Cordova

Secondary stack elements include:

- Docker, Nginx and Supervisor
- Azure Object Storage
- Azure Service Bus
- OneSignal (for notifications)

### Backend

The Microservices are built with .Net Core / C#. If database persistence is required then SQL Server and Entity Framework should be used. Microservices may share a database but must be separated by a namespace. See the `srcBackend` directory.

The Visual Studio solution is broken up into three sections, separated by solution folders:

- Global, this is where shared classes and interfaces reside.
- Modules, this section contains projects which encapsulate a 3rd party library or service. e.g. Azure (FileStorage), EntityFramework, OneSignal...
- Services, this is the section that contains the Microservices for the solution.

### Cloud Architecture

The solution is deployed to Azure as a Docker container. Instead of following best practice and building each service into an individual Docker container definition, all services will be bundled into one. The reason for this is to keep hosting costs low initially. If performance demands that a service be broken out into its own container, then this can be done, assuming the client is willing to pay the extra hosting costs.

The entry point in the container is the Supervisor program. Its primary role is to start all the services and keep them running. Nginx, one of the services managed by Supervisor, is set up as a reverse proxy so that web requests can be channelled to the Microservices services. For example:  
http://webapp.com/ - points to the nginx root listener  
http://webapp.com/identity/ - points to the Identity microservice  
http://webapp.com/other/ - points to the Other microservice  
...and so on.

In addition to the WebAPI configuration above, Supervisor can also manage the BackgroundService programs. These however won't be accessible via Nginx proxy.

### Frontend

The frontend SPA app is built on Ionic and AngularJS. See the `srcFrontend` directory.

The single Ionic frontend app should be able to target all platforms (Web, iOS, Android and Desktop) off the same codebase. It is necesary to do cordova platform checks to enable/disable device features accordingly.

## Development

### Setup

In order to get the solution up and running you must have these programs installed on your machine:

- Visual Studio 2019+
- SQL Server
- VS Code
- Git
- NodeJS / NPM
- NPM global packages; @angular/cli, cordova, ionic, typescript

The standard installation process is:

1. Install all necessary programs
2. Install the global NPM packages using the cmd prompt / terminal command:  
   `npm install -g @angular/cli cordova ionic typescript`
3. Pull the latest source code from the Git repo
4. Build the Visual Studio solution
5. Configure Visual Studio to multi-project startup mode, with all WebAPI projects set to start in Debug mode
6. Open cmd prompt / terminal, navigate to the `srcFrontend` directory and run these commands:  
   `npm install`  
   `ionic serve`

### Adding a new service

In order to add a new Microservice to the solution:

1. Add a new Solution folder in the Services section in the VS solution, name with the new Microservice name
2. Add the Microservice Core project, and WebApi or BackgroundService to the new folder. Ensure the project builds
3. Inside the /build folder, update the Build.ps1 to reference the new Microservice
4. Inside the /deploy folder, update the Dockerfile, nginx.conf and supervisor.conf to reference the new Microservice
5. Validate the deployment process works correctly after the updates

### Adding an EntityFrameworkCore data model

1. Add a new DataContext that inherits from the DataContextBase.
2. Add a new DataContextFactory class and include a valid test database connection string.
3. Add at least one new entity and configure it in the DataContext class. Also ensure that schema reflects the service name inside respective DataContext and DataContextFactory classes.
4. Ensure the dotnet-ef command line tool is installed, and run the command from the project root:
`dotnet ef migrations add InitialCreate`
5. Apply the first migration to the database by running the command:
`dotnet ef database update`
6. Repeats steps 4-5 as necessary.

### Frontend tooling

VS Code should be used for Frontend development, and the below extensions should be installed:

- TSLint
- Prettier
- Angular Language Service
- Angular Snippets
- angular2-inline

In order to maintain coding best practices and consistent formatting throughout the project, a commit hook is setup to run the prettier and ng lint libraries. This does the following at the time of commit:

1. prettier will format the changed files automatically re-stage them
2. Then, ng lint will be executed. If the code does not comply with the rules, the commit will be stopped.

Reference: https://medium.com/@killerchip0/code-style-and-best-practices-enforcement-in-angular-tslint-prettier-94e96f742e73

By installing the mentioned VS Code extensions, these two checks will happen in realtime rather than at the time of commit. However, these libraries can be triggered manually with:
`npx pretty-quick`
`ng lint`

## Deployment - Server

### Requirements

The following tools are needed to facilitate deployments:

- Azure CLI
- Docker Desktop

Also, an Azure account with adequate access is needed.

### Provisioning

The process of provisioning the Azure containerised Web App instance is done be the `ProvisionBackend.cmd` script. This script can be run by anyone who meets the criteria defined in the Requirements section above. This script should only be run once, the first time the Azure environment is set up.

### Deploying

After provisioning has been completed, any updates can be handled via the `DeployBackend.cmd` script.

## Deployment - PWA

Increment the app version number inside both the `srcFrontend\package.json` file and the `srcFrontend\ngsw-config.json` file. The latter is monitored by the service worker and is what will cause the "version update" notification to trigger.

The web app should be set up in the Firebase console, then initialised in the project:
`firebase init` 

Then additional staging environments can be added via the Hosting -> Mutliple websites area. To configure the environments as separate deployment targets use the example commands below:
`firebase target:apply hosting prod appx-prod`
`firebase target:apply hosting dev appx-development`

In addition to the above the firebase.json file must be adjusted to hold and array of projects for the "hosting" property, with the "target" property specified on each.

From command line in srcFrontnd directory run:  
 `ionic build --prod`  
 `firebase deploy --only hosting:prod`
 
