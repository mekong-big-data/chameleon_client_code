﻿
# publish Search project
dotnet publish "../srcBackend/Service.Search.WebApi/Service.Search.WebApi.csproj" -c Release -o "../Deploy/services/search"

# publish Manage project
dotnet publish "../srcBackend/Service.Manage.WebApi/Service.Manage.WebApi.csproj" -c Release -o "../Deploy/services/manage"

# publish Manage.Functions project
dotnet publish "../srcBackend/Service.Manage.Functions/Service.Manage.Functions.csproj" -c Release -o "../Deploy/services/managefunctions"

# publish Tenant project
dotnet publish "../srcBackend/Service.Tenant.WebApi/Service.Tenant.WebApi.csproj" -c Release -o "../Deploy/services/tenant"