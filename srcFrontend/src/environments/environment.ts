// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseServiceUrls: {
    search: 'https://localhost:44385/api',
    manage: 'https://localhost:44304/api',
    tenant: 'https://localhost:44365/api'
  },
  blobStorageBaseUrl: 'https://chameleonxyzstorage.blob.core.windows.net/',
  ipifyIpLookupUrl: 'https://api.ipify.org/?format=jsonp',
  oath2: {
    signupSigninOpts: {
      appId: 'e454b558-9e62-445b-9495-81697e278d16',
      authorizationBaseUrl: 'https://chameleonapplocal.b2clogin.com/chameleonapplocal.onmicrosoft.com/B2C_1_Chameleon_SignupSignin/oauth2/v2.0/authorize',
      accessTokenEndpoint: '',
      scope: 'openid offline_access https://chameleonapplocal.onmicrosoft.com/serviceapi/access',
      responseType: 'token',
      web: {
        // redirectUrl: 'http://localhost:8100/'
      }
    },
    passwordResetOpts: {
      appId: 'e454b558-9e62-445b-9495-81697e278d16',
      authorizationBaseUrl: 'https://chameleonapplocal.b2clogin.com/chameleonapplocal.onmicrosoft.com/B2C_1_Chameleon_PasswordReset/oauth2/v2.0/authorize',
      accessTokenEndpoint: '',
      scope: 'openid offline_access https://chameleonapplocal.onmicrosoft.com/serviceapi/access',
      responseType: 'token',
      web: { }
    },
    logoutUrl: 'https://chameleonapplocal.b2clogin.com/chameleonapplocal.onmicrosoft.com/B2C_1_Chameleon_SignupSignin/oauth2/v2.0/logout'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
