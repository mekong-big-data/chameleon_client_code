export const environment = {
  production: true,
  baseServiceUrls: {
    search: 'https://chameleonxyzapplication.azurewebsites.net/search/api',
    manage: 'https://chameleonxyzapplication.azurewebsites.net/manage/api',
    tenant: 'https://chameleonxyzapplication.azurewebsites.net/tenant/api'
  },
  blobStorageBaseUrl: 'https://chameleonxyzstorage.blob.core.windows.net/',
  ipifyIpLookupUrl: 'https://api.ipify.org/?format=jsonp',
  oath2: {
    signupSigninOpts: {
      appId: '012eec5a-8903-4c66-b20c-624baf0b174a',
      authorizationBaseUrl: 'https://chameleonapp.b2clogin.com/chameleonapp.onmicrosoft.com/B2C_1_Chameleon_SignupSignin/oauth2/v2.0/authorize',
      accessTokenEndpoint: '',
      scope: 'openid offline_access https://chameleonapp.onmicrosoft.com/serviceapi/access',
      responseType: 'token',
      web: {
        // redirectUrl: 'https://chameleon-pwa.web.app/'
      }
    },
    passwordResetOpts: {
      appId: '012eec5a-8903-4c66-b20c-624baf0b174a',
      authorizationBaseUrl: 'https://chameleonapp.b2clogin.com/chameleonapp.onmicrosoft.com/B2C_1_Chameleon_PasswordReset/oauth2/v2.0/authorize',
      accessTokenEndpoint: '',
      scope: 'openid offline_access https://chameleonapp.onmicrosoft.com/serviceapi/access',
      responseType: 'token',
      web: { }
    },
    logoutUrl: 'https://chameleonapp.b2clogin.com/chameleonapp.onmicrosoft.com/B2C_1_Chameleon_SignupSignin/oauth2/v2.0/logout'
  }
};
