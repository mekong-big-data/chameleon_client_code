import { Injectable } from '@angular/core';
import { timer, Observable, Subscription } from 'rxjs';

import { environment } from 'src/environments/environment';
import { ManageStateService } from './manage-state.service';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class UploadMonitorService {

  private activeUploadId = 0;
  private timerSub: Subscription = null;

  private startMonitoring(uploadId) {
    this.activeUploadId = uploadId;

    this.timerSub = timer(2000, 5000)
      .subscribe(this.onUpdate.bind(this), err => {
        this.stopMonitoring();
      })
  }

  private stopMonitoring() {
    if (this.timerSub) {
      this.timerSub.unsubscribe();
      this.timerSub = null;
    }
    this.activeUploadId = 0;
  }

  private async onUpdate() {
    try {
      if (!this.activeUploadId) {
        this.stopMonitoring();
        return;
      }

      const upload = await this.manageStateService.getCollectionUpload(this.activeUploadId).toPromise();
      if (upload == null) {
        this.stopMonitoring();
        return;
      }

      this.manageStateService.refreshCollections();
      
      if (upload.status === 2) { // complete
        this.stopMonitoring();

        const toast = await this.toastController.create({
          message: `Upload has completed successfully`,
          duration: 4000,
          color: 'success'
        });
        toast.present();
      }
    }
    catch (ex) {
      this.stopMonitoring();
    }
  }

  constructor(
    private manageStateService: ManageStateService,
    private toastController: ToastController
  ) {
  }

  monitorUpload(uploadId) {
    if (this.activeUploadId) {
      this.stopMonitoring();
    }
    this.startMonitoring(uploadId);
  }

}
