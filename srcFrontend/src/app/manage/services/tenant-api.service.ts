import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Tenant } from 'src/app/search/models/tenant';
import { TenantUser } from 'src/app/core/models/tenant-user';

@Injectable({
  providedIn: 'root',
})
export class TenantApiService {

  constructor(private http: HttpClient) {}
  
  getTenants(): Observable<Tenant[]> {
    const url = `${environment.baseServiceUrls.tenant}/tenant`;
    return this.http.get<Tenant[]>(url);
  }

  getTenant(tenantId: any): Observable<Tenant> {
    const url = `${environment.baseServiceUrls.tenant}/tenant/${tenantId}`;
    return this.http.get<Tenant>(url);
  }

  saveTenant(tenant: Tenant): Observable<Tenant>{
    const url = `${environment.baseServiceUrls.tenant}/tenant/`;
    return this.http.post<Tenant>(url, tenant);
  }

  updateTenant(tenant: Tenant): Observable<Tenant>{
    const url = `${environment.baseServiceUrls.tenant}/tenant/`;
    return this.http.put<Tenant>(url, tenant);
  }

  deleteTenant(tenantId: number){
    const url = `${environment.baseServiceUrls.tenant}/tenant/${tenantId}`;
    return this.http.delete<Tenant>(url);
  }

  getTenantAdminstrators(tenantId: number): Observable<any[]> {
    // const url = `${environment.baseServiceUrls.tenant}/tenantUser`;
    const url = `${environment.baseServiceUrls.tenant}/tenantUser/tenantUsersBytenantId/${tenantId}`;
    return this.http.get<any[]>(url);
  }

  getTenantAdminstrator(tenantUserId: any): Observable<TenantUser> {
    const url = `${environment.baseServiceUrls.tenant}/tenantUser/${tenantUserId}`;
    return this.http.get<TenantUser>(url);
  }

  saveTenantUser(tenantUser: TenantUser): Observable<TenantUser>{
    const url = `${environment.baseServiceUrls.tenant}/tenantUser`;
    return this.http.post<TenantUser>(url, tenantUser);
  }

  updateTenantUser(tenantUser: TenantUser): Observable<TenantUser>{
    const url = `${environment.baseServiceUrls.tenant}/tenantUser`;
    return this.http.put<TenantUser>(url, tenantUser);
  }

  deleteTenantUser(tenantUserId: number){
    const url = `${environment.baseServiceUrls.tenant}/tenantUser/${tenantUserId}`;
    return this.http.delete<TenantUser>(url);
  }

  getFileFromUrl(url: any){
    return this.http.get(url, {responseType: 'blob'});
  }
}
