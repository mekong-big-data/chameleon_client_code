import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Collection } from '../models/collection';
import { Image } from '../models/image';
import { HttpClient } from '@angular/common/http';
import * as data from '../../../data.json';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ManageStateService {

  public collections$: BehaviorSubject<any[]>;
  public images$: BehaviorSubject<any[]>;
  public collection$: BehaviorSubject<any>;
  public previewImage$: BehaviorSubject<any>;

  constructor(private httpClient: HttpClient) {
    this.collections$ = new BehaviorSubject<any[]>([]);    
    this.images$ = new BehaviorSubject<any[]>([]);
    this.collection$ = new BehaviorSubject<any>(0);
    this.previewImage$ = new BehaviorSubject<any>(0);
  }

  async refreshCollections() {
    const collections = await this.getCollections().toPromise();
    this.collections$.next(collections);
  }

  getImagePreview(previewImage: number) {
    this.previewImage$.next(previewImage);
  }

  getCollectionImages(collection: number) {
    this.collection$.next(collection);
  }

  getCollections(): Observable<any[]> {
    const url = `${environment.baseServiceUrls.manage}/collections`;
    return this.httpClient.get<any[]>(url);
  }

  uploadCollection(model: any): Observable<any>{
    const url = `${environment.baseServiceUrls.manage}/collectionUploads/`;
    return this.httpClient.post<any>(url, model);
  }
  
  getCollectionUpload(id: any): Observable<any>{
    const url = `${environment.baseServiceUrls.manage}/collectionUploads/${id}`;
    return this.httpClient.get<any>(url);
  }

  getCollectionImage(id: any): Observable<any> {
    const url = `${environment.baseServiceUrls.manage}/images/collectionImages/${id}`;
    return this.httpClient.get<any>(url);
  }

  getImage(id: any): Observable<any> {
    const url = `${environment.baseServiceUrls.manage}/images/${id}`;
    return this.httpClient.get<any>(url);
  }

  deleteCollection(collectionId: number){
    const url = `${environment.baseServiceUrls.manage}/collections/${collectionId}`;
    return this.httpClient.delete<any>(url);
  }

  deleteImage(id: number) {
    const url = `${environment.baseServiceUrls.manage}/images/${id}`;
    return this.httpClient.delete<any>(url);
  }
  
  getFileFromUrl(url: any){
    return this.httpClient.get(url, {responseType: 'blob'});
  }

}
