import { Image } from './image';

export class Collection {
  id: number;
  name: string;
  date: string;
  quantity: number;
  images: Image[];
}
