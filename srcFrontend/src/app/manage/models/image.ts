export class Image {
  id: number;
  imageUrl: string;
  size: string;
  name: string;
  date: string;
}
