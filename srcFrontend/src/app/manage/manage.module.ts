import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AuthSuperAdminGuard } from '../core/guards/auth-superadmin.guard';

import { ManagePage } from './components/manage-page/manage.page';
import { UploadComponent } from './components/upload-component/upload.component';
import { CollectionsComponent } from './components/collections-component/collections.component';
import { ImagesComponent } from './components/images-component/images.component';
import { PreviewComponent } from './components/preview-component/preview.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SearchPipe } from '../core/pipes/search.pipe';
import { TenantsComponent } from './components/tenants/tenants.component';
import { TenantComponent } from './components/tenants/tenant/tenant.component';
import { AddTenantUserComponent } from './components/tenants/add-tenant-user/add-tenant-user.component';
import { CopyClipboardDirective } from '../core/directives/copy-clipboard.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    NgxDropzoneModule,
    RouterModule.forChild([
      {
        path: '',
        component: ManagePage,
      },
      {
        path: 'tenants',
        canActivate: [AuthSuperAdminGuard],
        component: TenantsComponent,
      },
      {
        path: 'tenants/:id',
        canActivate: [AuthSuperAdminGuard],
        component: TenantComponent,
      },
    ]),
  ],
  declarations: [
    ManagePage,
    UploadComponent,
    CollectionsComponent,
    ImagesComponent,
    PreviewComponent,
    TenantsComponent,
    TenantComponent,
    SearchPipe,
    AddTenantUserComponent,
    CopyClipboardDirective
  ],
  entryComponents: [
    AddTenantUserComponent
  ]
})
export class ManageModule {}
