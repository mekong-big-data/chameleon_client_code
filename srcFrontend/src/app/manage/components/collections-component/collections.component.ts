import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ManageStateService } from '../../services/manage-state.service';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss'],
})
export class CollectionsComponent implements OnInit, OnDestroy {

  collections: any;
  searchQuery: any;
  collectionsSub: Subscription;

  private async notifyError(err: any) {
    const message = err.message || err;
    const toast = await this.toastController.create({
      message: `Collection error: ${message}`,
      duration: 4000,
    });
    toast.present();
  }

  constructor(
    private manageStateService: ManageStateService,
    public loadingController: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
  ) { }

  async ngOnInit() {
    this.collectionsSub = this.manageStateService.collections$
      .subscribe(x => {
        this.collections = x;
      });

    await this.getCollections();
  }

  ngOnDestroy() {
    if (this.collectionsSub) {
      this.collectionsSub.unsubscribe();
      this.collectionsSub = null;
    }
  }

  getImages(id: number) {
    this.manageStateService.getCollectionImages(id);
  }

  async getCollections() {
    await this.presentLoading();
    try {
      await this.manageStateService.refreshCollections();
    } catch (err) {
      this.notifyError(err);
      await this.loadingController.dismiss();
    } finally {
      await this.loadingController.dismiss();
    }
  }

  async deleteCollection(id: number) {
    const alert = await this.alertController.create({
      header: 'Are you sure?',
      message: 'Once deleted, you will not be able to recover this record!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: async () => {
            try {
              await this.presentLoading();
              this.manageStateService.deleteCollection(id).subscribe(async (response: any) => {                
                await this.loadingController.dismiss();
                this.getCollections();
              }, async err => {
                this.notifyError(err);
                await this.loadingController.dismiss();
              });
            } catch (err) {
              this.notifyError(err);
              await this.loadingController.dismiss();
            }
          }
        }
      ]
    });
    await alert.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    });
    await loading.present();
  }

}
