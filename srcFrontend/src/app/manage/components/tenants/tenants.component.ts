import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AppStateService } from 'src/app/core/services/appstate.service';
import { RoutingService } from 'src/app/core/services/routing.service';
import { ManageStateService } from '../../services/manage-state.service';
import { TenantApiService } from '../../services/tenant-api.service';

@Component({
  selector: 'app-tenants',
  templateUrl: './tenants.component.html',
  styleUrls: ['./tenants.component.scss'],
})
export class TenantsComponent implements OnInit {
  
  tenantName: string;
  collections: any;
  list: any;
  searchQuery: any;
  tenants: any[] = [];

  private async notifyError(err: any) {
    const message = err.message || err;
    const toast = await this.toastController.create({
      message: `Tenants error: ${message}`,
      duration: 4000,
    });
    toast.present();
  }
  
  constructor(
    private manageStateService: ManageStateService,
    public loadingController: LoadingController,
    private toastController: ToastController,
    private tenantApi: TenantApiService,
    private routingService: RoutingService,
    public modalController: ModalController,
    private alertController: AlertController,
    private router: Router,
    private appStateService: AppStateService,
  ) { 
    this.tenantName = this.appStateService.tenantName;
  }

  async ngOnInit() {
    
  }

  async ionViewDidEnter() {
    await this.getTenants();
  }

  async getTenants() {
    await this.presentLoading();
    try {
      this.tenants = await this.tenantApi.getTenants().toPromise();
    } catch (err) {
      await this.notifyError(err);
    } finally {
      await this.loadingController.dismiss();
    }
  }

  async navigateToCollections() {
    this.routingService.navigateForward('manage');
  }

  async deleteTenant(tenantId: number) {
    const alert = await this.alertController.create({
      header: 'Are you sure?',
      message: 'Once deleted, you will not be able to recover this record!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: async () => {
            try {
              await this.presentLoading();
              this.tenantApi.deleteTenant(tenantId).subscribe(async (response: any) => {                
                await this.getTenants();
                await this.loadingController.dismiss();
              }, async err => {
                this.notifyError(err);
                await this.loadingController.dismiss();
              });
            } catch (err) {
              this.notifyError(err);
            }
          }
        }
      ]
    });
    await alert.present();
  }
  
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    });
    await loading.present();
  }

  async logout() {
    await this.appStateService.logout();
  }

  getTenantManageLink(tenantName: string) {
    return `${location.protocol}//${location.host}/${tenantName}/manage`;
  }

  manageTenant(tenantName: string) {
    this.routingService.navigateToNewTenant(tenantName, 'manage');
  }
  
  async onLinkCopied(searchUrl: string) {
    const toast = await this.toastController.create({
      message: `Manage URL copied to clipboard: ${searchUrl}`,
      duration: 4000,
    });
    toast.present();
  }
}