import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { TenantUser } from 'src/app/core/models/tenant-user';
import { TenantApiService } from 'src/app/manage/services/tenant-api.service';

@Component({
  selector: 'app-add-tenant-user',
  templateUrl: './add-tenant-user.component.html',
  styleUrls: ['./add-tenant-user.component.scss'],
})
export class AddTenantUserComponent implements OnInit {

  private async notifyError(err: any) {
    const message = err.message || err;
    const toast = await this.toastController.create({
      message: `Error: ${message}`,
      duration: 4000,
    });
    toast.present();
  }

  tenantUserForm: FormGroup;
  @Input() tenantId: number = 0;
  @Input() tenantUserId: number = 0;
  tenantUser: TenantUser;

  public validation_messages = {
    roleType: [
      { type: 'required', message: 'Role is required.' }
    ],
    emailAddress: [
      { type: 'required', message: 'Email Address is required.' }
    ]
  };

  constructor(private formBuilder: FormBuilder,
    private loadingController: LoadingController,
    private tenantApi: TenantApiService,
    private toastController: ToastController,
    public modalController: ModalController) { }

  ngOnInit() {
    this.tenantUserForm = this.formBuilder.group({
      tenantUserId: [0],
      tenantId: [this.tenantId],
      roleType: ['', [Validators.required]],
      emailAddress: ['', [Validators.required, Validators.email]]
    });
    if (this.tenantUserId > 0) {
      this.getTenantUser();
    }
  }

  async getTenantUser() {
    const loading = await this.loadingController.create({
      message: 'loading...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    loading.present();
    loading.message = `Fetching Tenant User...`;
    try {
      this.tenantApi.getTenantAdminstrator(this.tenantUserId).subscribe((tenantUser: any) => {
        this.tenantUser = tenantUser;
        this.tenantUserForm.controls.tenantUserId.setValue(this.tenantUserId);
        this.tenantUserForm.controls.emailAddress.setValue(tenantUser.emailAddress);
        this.tenantUserForm.controls.roleType.setValue(tenantUser.role.toString());
      }, err => {
        this.notifyError(err);
        loading.dismiss();
      });;
    } catch (err) {
      this.notifyError(err);
    } finally {
      loading.dismiss();
    }
  }

  get t() {
    return this.tenantUserForm.controls;
  }
  
  async onTenantUserFormSubmit() {
    const loading = await this.loadingController.create({
      message: 'Saving...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    loading.present();
    if (this.tenantUserForm.invalid) {
      loading.dismiss();
      return;
    }
    const tenantUser = new TenantUser();
    tenantUser.tenantUserId = this.t.tenantUserId.value > 0 ? this.t.tenantUserId.value : 0;
    tenantUser.tenantId = this.tenantId;
    tenantUser.role = parseInt(this.t.roleType.value);
    tenantUser.emailAddress = this.t.emailAddress.value;
    try {
      if (this.tenantUserId > 0) {
        tenantUser.uniqueIdentifier = this.tenantUser.uniqueIdentifier;
        this.tenantApi.updateTenantUser(tenantUser).subscribe((response: any) => {
          this.modalController.dismiss();
          loading.dismiss();
        }, err => {
          loading.dismiss();
        });;
      } else {
        this.tenantApi.saveTenantUser(tenantUser).subscribe((response: any) => {
          this.modalController.dismiss();
          loading.dismiss();
        }, err => {
          loading.dismiss();
        });;
      }
    } catch (err) {
      this.notifyError(err);
    } finally {
      loading.dismiss();
    }
  }

  refreshForm() {
    this.tenantUserId = 0;
    this.tenantUserForm.reset();
    this.tenantUserForm.controls.tenantUserId.setValue(0);
    this.tenantUserForm.controls.tenantId.setValue(this.tenantId);
  }
}
