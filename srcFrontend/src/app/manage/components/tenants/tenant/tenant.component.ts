import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { TenantUser } from 'src/app/core/models/tenant-user';
import { BlobSharedViewStateService } from 'src/app/core/modules/azure-storage/services/blob-shared-view-state.service';
import { BlobUploadsViewStateService } from 'src/app/core/modules/azure-storage/services/blob-uploads-view-state.service';
import { BlobItemUpload } from 'src/app/core/modules/azure-storage/types/azure-storage';
import { AppStateService } from 'src/app/core/services/appstate.service';
import { RoutingService } from 'src/app/core/services/routing.service';
import { TenantApiService } from 'src/app/manage/services/tenant-api.service';
import { ImageSearch } from 'src/app/search/models/image-search';
import { Tenant } from 'src/app/search/models/tenant';
import { environment } from 'src/environments/environment';
import { AddTenantUserComponent } from '../add-tenant-user/add-tenant-user.component';

@Component({
  selector: 'app-tenant',
  templateUrl: './tenant.component.html',
  styleUrls: ['./tenant.component.scss'],
})
export class TenantComponent implements OnInit {

  private async notifyError(err: any) {
    const message = err.message || err;
    const toast = await this.toastController.create({
      message: `Tenants error: ${message}`,
      duration: 4000,
    });
    toast.present();
  }

  tenantName: string;
  tenantForm: FormGroup;
  tenantUserForm: FormGroup;
  files: File[] = [];
  certificateFiles: File[] = [];
  promotionalVideoFiles: File[] = [];
  file: File;
  loading: boolean;
  tenantId: number = 0;
  tenantUserId: number = 0;
  tenantAdminstrators: any[] = [];
  tenant: Tenant;
  tenantUser: TenantUser;
  addTenantUser: boolean;
  primaryLogoFilePath: any;
  certificateFilePath: any;
  promotionalVideoFilePath: any;

  private readonly tenantContainer = 'tenant';

  public validation_messages = {
    name: [
      { type: 'required', message: 'Short Name is required.' }
    ],
    displayName: [
      { type: 'required', message: 'Display Name is required.' }
    ],
    tinEyeAccountUserName: [
      { type: 'required', message: 'TinEye UserName is required.' }
    ],
    tinEyeAccountPassword: [
      { type: 'required', message: 'TinEye Password is required.' }
    ],
    roleType: [
      { type: 'required', message: 'Role is required.' }
    ],
    emailAddress: [
      { type: 'required', message: 'Email Address is required.' }
    ]
  };

  constructor(private formBuilder: FormBuilder, private routingService: RoutingService,
    private loadingController: LoadingController,
    private activatedRoute: ActivatedRoute,
    private tenantApi: TenantApiService,
    private toastController: ToastController,
    private alertController: AlertController,
    private appStateService: AppStateService,
    public modalController: ModalController,
    private blobUploadState: BlobUploadsViewStateService,
    private blobSharedState: BlobSharedViewStateService,) {
      this.tenantName = this.appStateService.tenantName;
    }

  async ngOnInit() {
    this.tenantForm = this.formBuilder.group({
      tenantId: [0],
      name: ['', [Validators.required]],
      displayName: ['', [Validators.required]],
      tinEyeAccountUserName: ['', [Validators.required]],
      tinEyeAccountPassword: ['', [Validators.required]],
      primaryLogoFilePath: [''],
      successfulMatchText: [''],
      failedMatchText: [''],
      websiteUrl: [''],
      certificateFilePath: ['']
    });

    this.tenantUserForm = this.formBuilder.group({
      tenantUserId: [0],
      tenantId: [this.tenantId],
      roleType: ['', [Validators.required]],
      emailAddress: ['', [Validators.required, Validators.email]]
    });

    this.tenantId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    if (this.tenantId > 0) {
      await this.getTenant();
      await this.getTenantAdminstrators();
    }
  }

  get f() {
    return this.tenantForm.controls;
  }

  get t() {
    return this.tenantUserForm.controls;
  }

  async navigateToTenants() {
    this.routingService.navigateBack('manage/tenants');
  }

  async onSelect(event) {
    this.loading = true;
    if (this.files.length > 0) {
      this.onRemove(this.files[0]);
    }
    this.primaryLogoFilePath = null;
    this.files.push(...event.addedFiles);
    const loading = await this.loadingController.create({
      message: 'Processing image...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    await loading.present();
    loading.message = `Uploading image...`;
    const uploadSub = this.blobUploadState.uploadedItems$
      .subscribe(async (uploads: BlobItemUpload[]) => {
        const upload = uploads[0];
        this.primaryLogoFilePath = upload.filename;
        loading.message = `Uploading image... ${upload.progress}%`;

        if (upload.progress === 100) {
          uploadSub.unsubscribe();
          loading.message = `Image uploaded successfully...`;
          await loading.dismiss();
        }
      }, async err => {
        uploadSub.unsubscribe();
        await loading.dismiss();
        this.notifyError(err);
      });
    this.blobSharedState.sasEndpointUrl = `${environment.baseServiceUrls.tenant}/sastoken`;
    this.blobSharedState.getContainerItems(this.tenantContainer);
    this.blobUploadState.uploadItems([event.addedFiles[0]]);
  }

  async getTenant() {
    const loading = await this.loadingController.create({
      message: 'Fetching tenants...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    await loading.present();
    try {
      this.tenant = await this.tenantApi.getTenant(this.tenantId).toPromise();
      this.tenantForm.controls.tenantId.setValue(this.tenantId);
      this.tenantForm.controls.name.setValue(this.tenant.name);
      this.tenantForm.controls.displayName.setValue(this.tenant.displayName);
      this.tenantForm.controls.tinEyeAccountUserName.setValue(this.tenant.tinEyeAccountUserName);
      this.tenantForm.controls.tinEyeAccountPassword.setValue(this.tenant.tinEyeAccountPassword);
      this.tenantForm.controls.successfulMatchText.setValue(this.tenant?.successfulMatchText);
      this.tenantForm.controls.failedMatchText.setValue(this.tenant?.failedMatchText);
      this.tenantForm.controls.websiteUrl.setValue(this.tenant?.websiteUrl);
      if (this.tenant.primaryLogoFilePath) {
        this.primaryLogoFilePath = this.tenant?.primaryLogoFileName.split('/')[1];
        await loading.present();
        await this.tenantApi.getFileFromUrl(this.tenant?.primaryLogoFilePath).subscribe(async file => {
          this.files.push(new File([file], this.primaryLogoFilePath, { type: file.type }));
          await loading.dismiss();
        });      
      }
      if (this.tenant.certificateFilePath) {
        this.certificateFilePath = this.tenant?.certificateFileName.split('/')[1];
        await loading.present();
        await this.tenantApi.getFileFromUrl(this.tenant?.certificateFilePath).subscribe(async file => {
          this.certificateFiles.push(new File([file], this.certificateFilePath, { type: file.type }));
          await loading.dismiss();
        });      
      }
      if (this.tenant.promotionalVideoFilePath) {
        this.promotionalVideoFilePath = this.tenant?.promotionalVideoFileName.split('/')[1];
        await loading.present();
        await this.tenantApi.getFileFromUrl(this.tenant?.promotionalVideoFilePath).subscribe(async file => {
          this.promotionalVideoFiles.push(new File([file], this.promotionalVideoFilePath, { type: file.type }));
          await loading.dismiss();
        });
      }
    } catch (err) {
      await this.notifyError(err);
    } finally {
      await loading.dismiss();
    }
  }

  async getTenantAdminstrators() {
    const loading = await this.loadingController.create({
      message: 'Fetching Tenant Adminstrators...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    await loading.present();
    try {
      this.tenantAdminstrators = await this.tenantApi.getTenantAdminstrators(this.tenantId).toPromise();
    } catch (err) {
      await this.notifyError(err);
    } finally {
      await loading.dismiss();
    }
  }

  async onRemove(event) {
    console.log(event);
    this.primaryLogoFilePath = null;
    this.files.splice(this.files.indexOf(event), 1);
  }

  async onTenantFormSubmit() {
    const loading = await this.loadingController.create({
      message: 'Saving...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    await loading.present();
    if (this.tenantForm.invalid) {
      await loading.dismiss();
      return;
    }
    const tenant = new Tenant();
    tenant.tenantId = this.f.tenantId.value > 0 ? this.f.tenantId.value : 0;
    tenant.name = this.f.name.value;
    tenant.displayName = this.f.displayName.value;
    tenant.tinEyeAccountUserName = this.f.tinEyeAccountUserName.value;
    tenant.tinEyeAccountPassword = this.f.tinEyeAccountPassword.value;
    tenant.successfulMatchText = this.f.successfulMatchText.value;
    tenant.failedMatchText = this.f.failedMatchText.value;
    tenant.primaryLogoFilePath = this.primaryLogoFilePath;
    tenant.websiteUrl = this.f.websiteUrl.value;
    tenant.certificateFilePath = this.certificateFilePath;
    tenant.promotionalVideoFilePath = this.promotionalVideoFilePath;
    try {
      if (this.tenantId > 0) {
        await this.tenantApi.updateTenant(tenant).toPromise();
      } else {
        await this.tenantApi.saveTenant(tenant).toPromise();
      }
      await this.navigateToTenants();
    } catch (err) {
      await this.notifyError(err);
    } finally {
      await loading.dismiss();
    }
  }

  async onTenantUserFormSubmit() {
    const loading = await this.loadingController.create({
      message: 'Saving...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    await loading.present();
    if (this.tenantUserForm.invalid) {
      await loading.dismiss();
      return;
    }
    const tenantUser = new TenantUser();
    tenantUser.tenantUserId = this.t.tenantUserId.value > 0 ? this.t.tenantUserId.value : 0;
    tenantUser.tenantId = this.tenantId;
    tenantUser.role = parseInt(this.t.roleType.value);
    tenantUser.emailAddress = this.t.emailAddress.value;
    try {
      if (this.tenantUserId > 0) {
        tenantUser.uniqueIdentifier = this.tenantUser.uniqueIdentifier;
        await this.tenantApi.updateTenantUser(tenantUser).subscribe(async (response: any) => {
          await this.getTenantAdminstrators();
          await loading.dismiss();
        }, async err => {
          await loading.dismiss();
        });;
      } else {
        await this.tenantApi.saveTenantUser(tenantUser).subscribe(async (response: any) => {
          await this.getTenantAdminstrators();
          await loading.dismiss();
        }, async err => {
          await loading.dismiss();
        });;
      }
      this.addTenantUser = false;
    } catch (err) {
      await this.notifyError(err);
    } finally {
      await loading.dismiss();
    }
  }

  async editTenantUser(tenantUserId: any) {
    this.tenantUserId = tenantUserId;
    await this.getTenantUser();
    this.addTenantUser = true;

    // if we want modal for add/edit Tenant User
    // this.addTenantUserModal(this.tenantUserId);

    // if we want to use modal then on page process should be stopped
    // this.addTenantUser = false;

  }

  async getTenantUser() {
    const loading = await this.loadingController.create({
      message: 'Fetching Tenant User...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    await loading.present();
    try {
      this.tenantUser = await this.tenantApi.getTenantAdminstrator(this.tenantUserId).toPromise();
      this.tenantUserForm.controls.tenantUserId.setValue(this.tenantUserId);
      this.tenantUserForm.controls.emailAddress.setValue(this.tenantUser.emailAddress);
      this.tenantUserForm.controls.roleType.setValue(this.tenantUser.role.toString());
    } catch (err) {
      await this.notifyError(err);
    } finally {
      await loading.dismiss();
    }
  }

  async deleteTenantUser(tenantUserId: number) {
    const alert = await this.alertController.create({
      header: 'Are you sure?',
      message: 'Once deleted, you will not be able to recover this record!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: async () => {
            try {
              const loading = await this.loadingController.create({
                message: 'Please wait...',
                cssClass: 'loading-custom-class',
                spinner: 'circles'
              });
              await loading.present();
              await this.tenantApi.deleteTenantUser(tenantUserId).subscribe(async (response: any) => {
                await this.getTenantAdminstrators();
                await this.loadingController.dismiss();
              }, async err => {
                console.log('err,', err);
                await this.notifyError(err);
                await this.loadingController.dismiss();
              });
            } catch (err) {
              await this.notifyError(err);
            }
          }
        }
      ]
    });
    await alert.present();
  }

  refreshForm() {
    this.tenantUserId = 0;
    this.tenantUserForm.reset();
    this.tenantUserForm.controls.tenantUserId.setValue(0);
    this.tenantUserForm.controls.tenantId.setValue(this.tenantId);
  }

  async logout() {
    await this.appStateService.logout();
  }

  async addTenantUserModal(tenantUserId: any) {
    const modal = await this.modalController.create({
      component: AddTenantUserComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        tenantId: this.tenantId,
        tenantUserId: tenantUserId
      }
    });
    modal.onDidDismiss().then(async data => {
      this.addTenantUser = false;
      await this.getTenantAdminstrators();
    })
    return await modal.present();
  }

  async onSelectCertificateFile(event) {
    this.loading = true;
    if (this.certificateFiles.length > 0) {
      this.onRemove(this.certificateFiles[0]);
    }
    this.certificateFilePath = null;
    this.certificateFiles.push(...event.addedFiles);
    const loading = await this.loadingController.create({
      message: 'Processing file...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    await loading.present();
    loading.message = `Uploading file...`;
    const uploadSub = this.blobUploadState.uploadedItems$
      .subscribe(async (uploads: BlobItemUpload[]) => {
        const upload = uploads[0];
        this.certificateFilePath = upload.filename;
        loading.message = `Uploading file... ${upload.progress}%`;

        if (upload.progress === 100) {
          uploadSub.unsubscribe();
          loading.message = `File uploaded successfully...`;
          await loading.dismiss();
        }
      }, async err => {
        uploadSub.unsubscribe();
        await loading.dismiss();
        await this.notifyError(err);
      });
    this.blobSharedState.sasEndpointUrl = `${environment.baseServiceUrls.tenant}/sastoken`;
    this.blobSharedState.getContainerItems(this.tenantContainer);
    this.blobUploadState.uploadItems([event.addedFiles[0]]);
  }

  async onRemoveCertificateFile(event) {
    console.log(event);
    this.certificateFilePath = null;
    this.certificateFiles.splice(this.certificateFiles.indexOf(event), 1);
  }

  async onSelectPromotionalVideoFile(event) {
    this.loading = true;
    if (this.promotionalVideoFiles.length > 0) {
      this.onRemove(this.promotionalVideoFiles[0]);
    }
    this.promotionalVideoFilePath = null;
    this.promotionalVideoFiles.push(...event.addedFiles);
    const loading = await this.loadingController.create({
      message: 'Processing file...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    await loading.present();
    loading.message = `Uploading file...`;
    const uploadSub = this.blobUploadState.uploadedItems$
      .subscribe(async (uploads: BlobItemUpload[]) => {
        const upload = uploads[0];
        this.promotionalVideoFilePath = upload.filename;
        loading.message = `Uploading file... ${upload.progress}%`;
        if (upload.progress === 100) {
          uploadSub.unsubscribe();
          loading.message = `File uploaded successfully...`;
          await loading.dismiss();
        }
      }, async err => {
        uploadSub.unsubscribe();
        await loading.dismiss();
        await this.notifyError(err);
      });
    this.blobSharedState.sasEndpointUrl = `${environment.baseServiceUrls.tenant}/sastoken`;
    this.blobSharedState.getContainerItems(this.tenantContainer);
    this.blobUploadState.uploadItems([event.addedFiles[0]]);
  }

  async onRemovePromotionalVideoFile(event) {
    console.log(event);
    this.promotionalVideoFilePath = null;
    this.promotionalVideoFiles.splice(this.promotionalVideoFiles.indexOf(event), 1);
  }
}
