import { Component, OnInit } from '@angular/core';
import { ManageStateService } from '../../services/manage-state.service';
import { Image } from '../../models/image';
import { LoadingController, ToastController } from '@ionic/angular';
import { Collection } from '../../models/collection';
@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss'],
})
export class PreviewComponent implements OnInit {
 
  private async notifyError(err: any) {
    const message = err.message || err;
    const toast = await this.toastController.create({
      message: `Image preview error: ${message}`,
      duration: 4000,
    });
    toast.present();
  }
  collection: Collection;
  image: any;
  certificateFilePath: any;

  constructor(
    private manageStateService: ManageStateService,
    private loadingController: LoadingController,
    private toastController: ToastController,
  ) { }

  async ngOnInit() {
    this.manageStateService.previewImage$.subscribe((id) => {
      if (id > 0) {
        this.getImagePreview(id);
      }
    });
  }

  async getImagePreview(id: any) {
    const loading = await this.loadingController.create({
      message: 'Fetching...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    loading.present();
    try {
      this.manageStateService.getImage(id).subscribe((result) => {
        this.image = result;
      });
    } catch (err) {
      this.notifyError(err);
      loading.dismiss();
    } finally {
      loading.dismiss();
    }
  }
}
