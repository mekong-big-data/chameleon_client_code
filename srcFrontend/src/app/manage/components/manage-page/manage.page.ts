import { Component, OnInit } from '@angular/core';
import { ManageStateService } from '../../services/manage-state.service';
import { OAuth2Service } from 'src/app/core/services/oauth2.service';
import { RoutingService } from 'src/app/core/services/routing.service';
import { AppStateService } from 'src/app/core/services/appstate.service';

@Component({
  selector: 'app-manage',
  templateUrl: 'manage.page.html',
  styleUrls: ['manage.page.scss'],
})
export class ManagePage implements OnInit {

  tenantName: string;
  canManageTenants: boolean;

  constructor(
    private manageStateService: ManageStateService,
    private oAuth2Service: OAuth2Service,
    private routingService: RoutingService,
    private appStateService: AppStateService) 
  {
    this.tenantName = this.appStateService.tenantName;
  }

  async ngOnInit() {    
    await this.oAuth2Service.authenticate()
      .then(isAuthenticated => {
        if (isAuthenticated) {
          this.routingService.navigateForward('manage');
        }
      })

    this.canManageTenants = this.appStateService.isSuperAdmin;
  }

  async navigateToTenants() {
    this.routingService.navigateForward('manage/tenants');
  }

  async logout() {
    await this.appStateService.logout();
  }
  
}
