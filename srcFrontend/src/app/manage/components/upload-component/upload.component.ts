import { Component, OnInit } from '@angular/core';
import { LoadingController,ToastController } from '@ionic/angular';
import { BlobSharedViewStateService } from 'src/app/core/modules/azure-storage/services/blob-shared-view-state.service';
import { BlobUploadsViewStateService } from 'src/app/core/modules/azure-storage/services/blob-uploads-view-state.service';
import { BlobItemUpload } from 'src/app/core/modules/azure-storage/types/azure-storage';
import { ManageStateService } from '../../services/manage-state.service';
import { UploadMonitorService } from '../../services/upload-monitor.service';
import { environment } from 'src/environments/environment';
import { Tenant } from 'src/app/search/models/tenant';
import { AppStateService } from 'src/app/core/services/appstate.service';
import { Collection } from '../../models/collection';
import { TenantUser } from 'src/app/core/models/tenant-user';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
})
export class UploadComponent implements OnInit {
  private async notifyError(err: any) {
    const message = err.message || err;
    const toast = await this.toastController.create({
      message: `Tenants error: ${message}`,
      duration: 4000,
    });
    toast.present();
  }

  files: File[] = [];
  loading: boolean;
  collectionFilePath: any;
  collectionFiles: File[] = [];
  primaryLogoFilePath: any;
  tenantUser: TenantUser;

  private readonly uploadContainer = 'upload';
  constructor(
     private manageStateService: ManageStateService,
     private uploadMonitorService: UploadMonitorService,
     private loadingController: LoadingController, 
     private toastController: ToastController,
     private blobUploadState: BlobUploadsViewStateService,
     private blobSharedState: BlobSharedViewStateService,
     private appStateService: AppStateService) {}

  ngOnInit() {
    this.appStateService.authContext$.subscribe((tenant)=> {
      this.tenantUser = tenant?.tenantUser;
    });
  }

  async onRemove(event) {
    console.log(event);
    this.primaryLogoFilePath = null;
    this.files.splice(this.files.indexOf(event), 1);
  }
  
  async onSelectCollectionFile(event) {
    this.loading = true;
    if (this.collectionFiles.length > 0) {
      this.onRemove(this.collectionFiles[0]);
    }
    this.collectionFilePath = null;
    this.collectionFiles.push(...event.addedFiles);
    const loading = await this.loadingController.create({
      message: 'Processing file...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    loading.present();
    loading.message = `Uploading file...`;
    const uploadSub = this.blobUploadState.uploadedItems$
      .subscribe(async (uploads: BlobItemUpload[]) => {
        const upload = uploads[0];
        this.collectionFilePath = upload.filename;        
        loading.message = `Uploading file... ${upload.progress}%`;

        if (upload.progress === 100) {
          uploadSub.unsubscribe();
          loading.message = `File uploaded successfully...`;
          this.loading = false;
          loading.dismiss();
        }
      }, err => {
        uploadSub.unsubscribe();
        loading.dismiss();
        this.notifyError(err);
      });
    this.blobSharedState.sasEndpointUrl = `${environment.baseServiceUrls.manage}/sastoken`;
    this.blobSharedState.getContainerItems(this.uploadContainer);
    this.blobUploadState.uploadItems([event.addedFiles[0]]);
  }

  async onRemoveCollectionFile(event) {
    console.log(event);
    this.collectionFilePath = null;
    this.collectionFiles.splice(this.collectionFiles.indexOf(event), 1);
  }

  async uploadCollection() {
    const loading = await this.loadingController.create({
      message: 'Saving...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    loading.present();
    
    const model =  {
      tenantUserId: this.tenantUser.tenantUserId,
      uploadFileName: this.collectionFilePath,
      status: 1
    }
    try {
      this.manageStateService.uploadCollection(model).subscribe((upload)=> {
        this.collectionFiles = [];
        this.collectionFilePath = null;
        this.uploadMonitorService.monitorUpload(upload.collectionUploadId);
      });
    } catch (err) {
      this.notifyError(err);
    } finally {
      loading.dismiss();
    }
  }

}
