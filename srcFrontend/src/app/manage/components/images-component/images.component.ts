import { Component, OnInit, ViewChild } from '@angular/core';
import { ManageStateService } from '../../services/manage-state.service';
import { Image } from '../../models/image';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss'],
})
export class ImagesComponent implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  collectionId: any;
  private async notifyError(err: any) {
    const message = err.message || err;
    const toast = await this.toastController.create({
      message: `Images error: ${message}`,
      duration: 4000,
    });
    toast.present();
  }
  images: Image[] = [];
  searchQuery: any;

  constructor(
    private manageStateService: ManageStateService,
    public loadingController: LoadingController,
    private toastController: ToastController,
    private alertController: AlertController,
  ) { }

  ngOnInit() {
    this.manageStateService.collection$.subscribe((id) => {
      if (id > 0) {
        this.collectionId = id;
        this.getImagesByCollection();
      }
    });
  }

  async getImagesByCollection() {
    await this.presentLoading();
    try {
      this.manageStateService.getCollectionImage(this.collectionId).subscribe((result) => {
        this.images = result.slice(0, 4);
      });
    } catch (err) {
      this.notifyError(err);
      await this.loadingController.dismiss();
    } finally {
      await this.loadingController.dismiss();
    }
  }

  getImage(id: number) {
    this.manageStateService.getImagePreview(id);
  }

  async deleteImage(id: number) {
    const alert = await this.alertController.create({
      header: 'Are you sure?',
      message: 'Once deleted, you will not be able to recover this record!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: async () => {
            try {
              await this.presentLoading();
              this.manageStateService.deleteImage(id).subscribe(async (response: any) => {                
                this.getImagesByCollection();
                await this.loadingController.dismiss();
              }, async err => {
                this.notifyError(err);
                await this.loadingController.dismiss();
              });
            } catch (err) {
              this.notifyError(err);
              await this.loadingController.dismiss();
            }
          }
        }
      ]
    });
    await alert.present();
  }

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.images.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      this.manageStateService.getCollectionImage(this.collectionId).subscribe((result) => {
        console.log("result", result);
        this.images = [];
        this.images = result;
      });
      console.log('Async operation has ended');
      infiniteScroll.target.complete();
      infiniteScroll.target.disabled = true;
    }, 500);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    });
    await loading.present();
  }
}
