import { Component, OnInit } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { OAuth2Service } from './core/services/oauth2.service';
import { UpdateAvailableService } from './core/services/update-available.service';

import { Plugins } from '@capacitor/core';
import { Router } from '@angular/router'
import { AppStateService } from './core/services/appstate.service';

const { Storage } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  public isUpdateAvailable: boolean;
  public displayOverlay: boolean;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private oAuth2Service: OAuth2Service,
    private appStateService: AppStateService,
    private updateAvailableService: UpdateAvailableService,
    private toastController: ToastController
  ) {
    oAuth2Service.register();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // if (this.platform.is('ios')) {
      //   // try prevent touch zoom - iOS Safari
      //   window.addEventListener('touchmove', (evt: any) => {
      //     if (evt.scale !== 1) { 
      //       evt.preventDefault(); 
      //     }
      //   }, { passive: false });
      // }

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async ngOnInit() {
    await this.appStateService.register();

    // await this.addAppToHomeScreen();

    this.updateAvailableService.checkForUpdates();
  }

  async addAppToHomeScreen() {
    // only prompt on iOS
    if (!this.platform.is('ios')) {
      return;
    }

    // exclude Chrome iOS
    if (navigator.userAgent.match('CriOS')) {
      return;
    }
    // check if prompt already dismissed
    const installPrompted = await Storage.get({ key: 'install_prompted' });
    if (installPrompted && installPrompted.value === 'true') {
      return;
    }

    const toast = await this.toastController.create({
      duration: 20000,
      color: 'dark',
      message:
        // tslint:disable-next-line: quotemark
        "Tap 'Share' (Up arrow icon) in the the bottom panel of your browser and then 'Add to Home Screen' to get full screen mode and faster loading times..",
      position: 'bottom',
      buttons: [
        {
          text: 'Ok',
          role: 'ShowToaster',
          cssClass: 'secondary',
          handler: async () => {
            await Storage.set({ key: 'install_prompted', value: 'true' });
            return true;
          },
        },
      ],
    });
    toast.present();
  }

}
