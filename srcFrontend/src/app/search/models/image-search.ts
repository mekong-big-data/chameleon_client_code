export class ImageSearch {
  imageSearchId: number;
  matchingImageId?: number;
  imageFileName: string;
  imageFileUrl: string;
  imageKey: string;
  score: number;
  matchPercent: number;
  queryOverlapPercent: number;
  targetOverlapPercent: number;
  matchFileName: string;
  matchFileUrl: string;
  ipAddress: string;
  latitude: number;
  longitude: number;  
  rotation: number;
  subjectName: string;
  subjectDescription: string;
  subjectUrl: string;
  subjectImageUrl: string
}