export class TenantUser {
    tenantUserId: number;
    tenantId: number;
    tenantName: string;
    role: any;
    uniqueIdentifier: string;
    emailAddress: string;
  }