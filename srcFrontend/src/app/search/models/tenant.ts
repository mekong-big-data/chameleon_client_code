export class Tenant {
    tenantId: number;
    name: string;
    displayName: string;
    tinEyeAccountUserName: string;
    tinEyeAccountPassword: string;
    primaryLogoFilePath: string;
    primaryLogoFileName: string;
    successfulMatchText: string;
    failedMatchText: string;
    websiteUrl: string;
    certificateFilePath: string;
    certificateFileName: string;
    promotionalVideoFilePath: string;
    promotionalVideoFileName: string;
  }