import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ImageSearch } from '../models/image-search';

@Injectable()
export class SearchApiService {

  constructor(
    private http: HttpClient,
  ) {}

  getImageSearch(imageSearchId: number): Observable<ImageSearch> {
    const url = `${environment.baseServiceUrls.search}/imagesearch/${imageSearchId}`;
    return this.http.get<ImageSearch>(url);
  }

  postImageSearch(imageSearch: ImageSearch): Observable<ImageSearch> {
    const url = `${environment.baseServiceUrls.search}/imagesearch/go`;
    return this.http.post<ImageSearch>(url, imageSearch);
  }

}
