import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { environment } from 'src/environments/environment';
import { ImageSearch } from '../../models/image-search';
import { RoutingService } from 'src/app/core/services/routing.service';
import { AppStateService } from 'src/app/core/services/appstate.service';
import { PopoverController, ModalController } from '@ionic/angular';
import { LogoPopoverComponent } from './logo-popover-component/logo-popover.component';
import { CertificatePopoverComponent } from './certificate-popover-component/certificate-popover.component';
import { PromotionalVideoComponent } from './promotional-video/promotional-video.component';
const { Storage } = Plugins;

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
})
export class ResultComponent implements OnInit {

  imageKey: string;
  tenantLogoUrl: string;
  tenantSuccessText: string;
  failedMatchText: string;
  imageSearch: ImageSearch;
  matchFound: boolean;
  isWatched: boolean = true;
  hasWebsiteUrl: boolean;
  hasCertificate: boolean;
  hasPromotionalVideo: boolean;

  get hasSubjectData(): boolean {
    if (!this.imageSearch) {
      return false;
    }
    return !!this.imageSearch.subjectName
      || !!this.imageSearch.subjectDescription
      || !!this.imageSearch.subjectUrl
      || !!this.imageSearch.subjectImageUrl;
  }

  constructor(
    private appStateService: AppStateService,
    private routingService: RoutingService,
    private activatedRoute: ActivatedRoute,
    public popoverController: PopoverController,
    private modalController: ModalController
  ) {
    this.imageKey = this.activatedRoute.snapshot.params.imageKey;
  }

  async ngOnInit() {
    const authContext = this.appStateService.authContext$.value;
    this.tenantLogoUrl = `${environment.blobStorageBaseUrl}${authContext.tenant.primaryLogoFilePath}`;
    this.hasWebsiteUrl = !!authContext.tenant.websiteUrl;
    this.hasCertificate = true; // !!authContext.tenant.certificateFilePath;
    this.hasPromotionalVideo = !!authContext.tenant.promotionalVideoFilePath;
    this.tenantSuccessText = this.appStateService.authContext$.value.tenant.successfulMatchText;
    this.failedMatchText = this.appStateService.authContext$.value.tenant.failedMatchText;

    const { value } = await Storage.get({ key: 'image_search' });
    this.imageSearch = JSON.parse(value) as ImageSearch;
    this.matchFound = !!(this.imageSearch && this.imageSearch.score && this.imageSearch.matchFileUrl);
    
    setTimeout(() => {
      this.isWatched = true;
    }, 2000);
  }

  back() {
    if (!this.imageKey) {
      this.routingService.navigateBack(['search', 'capture']);
    } else {
      this.routingService.navigateBack(['search', 'capture', this.imageKey]);
    }
  }

  async logout() {
    await this.appStateService.logout();
  }

  openSubjectWebsite(subjectUrl: string) {
    window.open(subjectUrl);
  }

  openWebsite() {
    const authContext = this.appStateService.authContext$.value;

    if (!authContext.tenant.websiteUrl) {
      return;
    }

    if (authContext.tenant.websiteUrl.substring(0, 4) !== 'http' 
      && authContext.tenant.websiteUrl.substring(0, 4) !== 'http') {
      window.open(`http://${authContext.tenant.websiteUrl}`, '_blank');
    } else {
      window.open(authContext.tenant.websiteUrl, '_blank');
    }
  }

  goToWebsite(){
    window.open('https://chameleoninnovations.com.au/', '_blank');
  }

  async presentLogoPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: LogoPopoverComponent,
      cssClass: 'popover-custom-class',
      event: ev
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
  }

  async presentCertificatePopover(ev: any) {
    const authContext = this.appStateService.authContext$.value;

    const popover = await this.modalController.create({
      component: CertificatePopoverComponent,
      componentProps: {
        certificateUrl: `${environment.blobStorageBaseUrl}${authContext.tenant.certificateFilePath}`,
        websiteUrl: authContext.tenant.websiteUrl,
        hasWebsiteUrl: this.hasWebsiteUrl
      }
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
  }

  async presentPromotionalVideoPopover(ev: any) {
    const authContext = this.appStateService.authContext$.value;

    const popover = await this.modalController.create({
      component: PromotionalVideoComponent,
      cssClass: 'modal-custom-class',
      componentProps: {
        promotionalVideoFileUrl: `${environment.blobStorageBaseUrl}${authContext.tenant.promotionalVideoFilePath}`,
        hasPromotionalVideo: this.hasPromotionalVideo
      }
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
  }

}
