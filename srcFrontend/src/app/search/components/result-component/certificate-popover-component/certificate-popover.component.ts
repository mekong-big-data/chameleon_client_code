import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppStateService } from 'src/app/core/services/appstate.service';

@Component({
  selector: 'app-certificate-popover',
  templateUrl: './certificate-popover.component.html',
  styleUrls: ['./certificate-popover.component.scss'],
})
export class CertificatePopoverComponent implements OnInit {

  @Input() certificateUrl: string;
  @Input() websiteUrl: string;
  @Input() hasWebsiteUrl: boolean;

  public showDefaultView = false;
  public showVBreatheView = false;
  public showPenfoldsView = false;

  sliderOpt = {
    zoom: {
      maxRatio: 3
    },
  };
  
  constructor(
    private appStateService: AppStateService,
    private modalController: ModalController) {

    const authContext = this.appStateService.authContext$.value;
    if (authContext.tenant.name === 'acme8') {
      this.showPenfoldsView = true;
    } else if (authContext.tenant.name === 'acme7') {
      this.showVBreatheView = true;
    } else {
      this.showDefaultView = true;
    }
  }

  ngOnInit() {}

  onClose() {
    this.modalController.dismiss();
  }

}
