import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-promotional-video',
  templateUrl: './promotional-video.component.html',
  styleUrls: ['./promotional-video.component.scss'],
})
export class PromotionalVideoComponent implements OnInit {
  
  @Input() promotionalVideoFileUrl: string;
  @Input() hasPromotionalVideo: boolean;
  
  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }

  onClose() {
    this.modalController.dismiss();
  }

}
