import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  Plugins,
  CameraResultType,
  CameraSource,
  CameraDirection,
  CameraPhoto,
} from '@capacitor/core';
import { FileHelperService } from 'src/app/core/services/filehelper.service';
import { BlobUploadsViewStateService } from 'src/app/core/modules/azure-storage/services/blob-uploads-view-state.service'
import { BlobSharedViewStateService } from 'src/app/core/modules/azure-storage/services/blob-shared-view-state.service';
import { BlobItemUpload } from 'src/app/core/modules/azure-storage/types/azure-storage';
import { LoadingController, ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { SearchApiService } from '../../services/search-api.service';
import { ImageSearch } from '../../models/image-search';
import { LocationService } from 'src/app/core/services/location.service';
import { RoutingService } from 'src/app/core/services/routing.service';
import { AppStateService } from 'src/app/core/services/appstate.service';
import jsQR from 'jsqr';

import { Subject } from 'rxjs';
import { auditTime, distinctUntilChanged } from 'rxjs/operators';

const { Geolocation } = Plugins;
const { Camera } = Plugins;
const { Storage } = Plugins;

@Component({
  selector: 'app-capture',
  templateUrl: './capture.component.html',
  styleUrls: ['./capture.component.scss'],
})
export class CaptureComponent implements OnInit {

  private readonly videoFramesPerSecond = 20;
  private readonly searchContainer = 'search';
  
  private findReady = new Subject<any>();
  private processingQrCode: boolean;
  private latitude: any;
  private longitude: any;
  private ipAddress: any;
  private lastAnimationFrameTimestamp: number;

  public imageKey: string;
  public tenantLogoUrl: string;
  public cameraInitialised = false;
  public isScanning = false;

  @ViewChild('videostream') videoStreamElement;
  @ViewChild('fullcanvas') fullCanvasElement;

  get video() {
    return !!this.videoStreamElement ? 
      this.videoStreamElement.nativeElement : 
      undefined;
  }

  get fullCanvas() {
    return !!this.fullCanvasElement ? 
      this.fullCanvasElement.nativeElement.getContext('2d') : 
      undefined;
  }

  private async notifyError(err: any) {
    const message = err.message || err;
    const toast = await this.toastController.create({
      message: `Capture error: ${message}`,
      duration: 4000,
    });
    toast.present();
  }

  private renderFrame(timestamp: number) {
    if (!this.video || !this.isScanning || this.processingQrCode) {
      return;
    }
    
    requestAnimationFrame(this.renderFrame.bind(this));

    const elapsed = timestamp - this.lastAnimationFrameTimestamp;
    if (elapsed < 1000 / this.videoFramesPerSecond) {
      return;
    }

    this.lastAnimationFrameTimestamp = timestamp;
    if (this.video.readyState === this.video.HAVE_ENOUGH_DATA) {
      this.fullCanvas.drawImage(
        this.video, 
        0, 
        0, 
        this.fullCanvasElement.nativeElement.width, 
        this.fullCanvasElement.nativeElement.height);

      this.findReady.next(true);
    }
  }

  private findQrCode() {
    if (!this.isScanning || this.processingQrCode) {
      return;
    }

    const fullImageData = this.fullCanvas.getImageData(
      0, 
      0, 
      this.fullCanvasElement.nativeElement.width, 
      this.fullCanvasElement.nativeElement.height);

    const hasCode: any = jsQR(
      fullImageData.data, 
      fullImageData.width, 
      fullImageData.height, 
      {
        inversionAttempts: 'dontInvert',
      });      

    if (!hasCode) {
      return;
    }

    this.processQrCode();
  }

  private processQrCode() {
    if (!this.isScanning || this.processingQrCode) {
      return;
    }

    console.log('Process QR code...');

    this.processingQrCode = true;

    this.pauseVideo();

    this.uploadRequest(this.fullCanvasElement.nativeElement);
  }

  constructor(
    public loadingController: LoadingController,
    private toastController: ToastController,
    private fileHelper: FileHelperService,
    private searchApi: SearchApiService,
    private blobUploadState: BlobUploadsViewStateService,
    private blobSharedState: BlobSharedViewStateService,
    private locationService: LocationService,
    private appStateService: AppStateService,
    private routingService: RoutingService,
    private activatedRoute: ActivatedRoute
  ) {
    this.imageKey = this.activatedRoute.snapshot.params.imageKey;
  }

  async ngOnInit() {
    const authContext = this.appStateService.authContext$.value;
    this.tenantLogoUrl = `${environment.blobStorageBaseUrl}${encodeURIComponent(authContext.tenant.primaryLogoFilePath)}`;

    this.findReady.pipe(
      auditTime(1000)
    ).subscribe(() => {
      this.findQrCode();
    })
  }

  ionViewDidEnter() {
    this.processingQrCode = false;

    setTimeout((() => {
      this.startCamera();
    }).bind(this), 1000);
  }

  ionViewWillLeave() {
    if (this.cameraInitialised) {
      this.pauseVideo();
      this.resetCanvas();
    }
  }

  async logout() {
    await this.appStateService.logout();
  }

  onCapture() {
    this.clear();
    this.startCamera();
  }

  async setItem(imageSearch: ImageSearch) {
    await Storage.set({
      key: 'image_search',
      value: JSON.stringify(imageSearch),
    });
  }

  async clear() {
    this.processingQrCode = false;
    await Storage.remove({ key: 'image_search' });
  }

  async getCurrentPosition(): Promise<any> {
    return await Geolocation.getCurrentPosition()
      .then((position) => {
        return {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        };
      }).catch((error) => {
        console.log('Failed to fetch current position');
        return null;
      });
  }

  async getIpAddress(): Promise<string> {
    return await this.locationService.getIPAddress().toPromise()
      .then((ipAddress: string): string => {
        return ipAddress;
      }).catch((error) => {
        console.log('Failed to fetch IP Address');
        return null;
      });
  }
    
  pauseVideo() {
    if (!!this.video) {
      this.video.pause();
    }
  }

  resetCanvas() {
    this.fullCanvas.clearRect(
      0, 
      0, 
      this.fullCanvasElement.nativeElement.width, 
      this.fullCanvasElement.nativeElement.height);
  }

  startCamera() {
    if (this.cameraInitialised) {
      this.video.play();
      requestAnimationFrame(this.renderFrame.bind(this));
      this.isScanning = true;
      return;
    }

    if (navigator.mediaDevices.getUserMedia) {
      const successCallback = (stream) => {
        this.video.srcObject = stream;

        this.video.onloadedmetadata = () => {
          const videoTrack = stream.getVideoTracks()[0];
          const videoTrackSettings = videoTrack.getSettings();

          this.fullCanvasElement.nativeElement.width = videoTrackSettings.width;
          this.fullCanvasElement.nativeElement.height = videoTrackSettings.height;
          
          this.cameraInitialised = true;
          this.video.play();
          
          requestAnimationFrame(this.renderFrame.bind(this));
          this.isScanning = true;
        }
      };

      const errorCallback = (error) => {
        console.log(error);
      };

      navigator.mediaDevices.getUserMedia({
        audio: false,
        video: { 
          facingMode: 'environment', 
          // aspectRatio: 0.5625, 
          // width: { ideal: 1920 }, height: { ideal: 1080 }, 
          width: { ideal: 800 }, height: { ideal: 400 }, 
          frameRate: this.videoFramesPerSecond 
        }
      }).then(successCallback, errorCallback);
    }
  }

  async uploadRequest(canvas: HTMLCanvasElement) {
    const loading = await this.loadingController.create({
      message: 'Authenticating product.',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    loading.present();

    const currentPosition = await this.getCurrentPosition();
    const currentIpAddress = '0.0.0.0'; // await this.getIpAddress();

    loading.message = `Authenticating product..`;

    canvas.toBlob((fileBlob: Blob) => {
      const imageFile = this.fileHelper.blobToFile(fileBlob, new Date().getTime().toString());
      this.blobSharedState.sasEndpointUrl = `${environment.baseServiceUrls.search}/sastoken`;
      this.blobSharedState.getContainerItems(this.searchContainer);
      this.blobUploadState.uploadItems([imageFile]);
    });
    const uploadSub = this.blobUploadState.uploadedItems$
      .subscribe(async (uploads: BlobItemUpload[]) => {
        const upload = uploads[0];
        if (upload.progress === 100) {
          uploadSub.unsubscribe();

          loading.message = `Authenticating product...`;

          try {
            const imageSearch = await this.searchApi.postImageSearch({
              imageKey: this.imageKey,
              imageFileName: upload.filename,
              ipAddress: currentIpAddress,
              latitude: currentPosition != null ? currentPosition.latitude : null,
              longitude: currentPosition != null ? currentPosition.longitude : null,
            } as ImageSearch).toPromise();
            await this.setItem(imageSearch);

            if (!this.imageKey) {
              this.routingService.navigateForward([ 'search', 'result' ]);
            } else {
              this.routingService.navigateForward([ 'search', 'result', this.imageKey]);
            }

          } catch (err) {
            this.notifyError(err);
            this.processingQrCode = false;
            // restart camera after 500ms
            setTimeout((() => {
              this.startCamera();
            }).bind(this), 500);
          } finally {
            loading.dismiss();
          }
        }
      }, err => {
        uploadSub.unsubscribe();
        loading.dismiss();
        this.notifyError(err);
        this.processingQrCode = false;
      });
  }
}
