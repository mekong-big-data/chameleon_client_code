import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SearchPage } from './components/search-page/search.page';
import { CaptureComponent } from './components/capture-component/capture.component';
import { ResultComponent } from './components/result-component/result.component';
import { LogoPopoverComponent } from './components/result-component/logo-popover-component/logo-popover.component';
import { CertificatePopoverComponent } from './components/result-component/certificate-popover-component/certificate-popover.component';

import { SearchApiService } from './services/search-api.service';
import { PromotionalVideoComponent } from './components/result-component/promotional-video/promotional-video.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      { 
        path: '', 
        redirectTo: 'capture', 
        pathMatch: 'full' 
      },
      {
        path: '',
        component: SearchPage,
        children: [
          {
            path: 'capture',
            component: CaptureComponent,
          },
          {
            path: 'capture/:imageKey',
            component: CaptureComponent,
          },
          {
            path: 'result',
            component: ResultComponent,
          },
          {
            path: 'result/:imageKey',
            component: ResultComponent,
          },
        ],
      },
    ]),
  ],
  declarations: [
    SearchPage, 
    CaptureComponent, 
    ResultComponent,
    LogoPopoverComponent,
    CertificatePopoverComponent,
    PromotionalVideoComponent
  ],
  providers: [
    SearchApiService
  ]
})
export class SearchModule {}
