import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  transform(items: any[], searchQuery: string): any[] {
    if (!items) {
      return [];
    }
    if (!searchQuery) {
      return items;
    }
    searchQuery = searchQuery.toLowerCase();
    return items.filter((item) => {
      return item.name.toLowerCase().includes(searchQuery);
    });
  }
}
