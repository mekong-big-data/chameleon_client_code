import { Component, OnInit } from '@angular/core';
import { RoutingService } from '../../services/routing.service';
import { AppStateService } from '../../services/appstate.service';

@Component({
  selector: 'app-splash',
  templateUrl: 'splash.page.html',
  styleUrls: ['splash.page.scss'],
})
export class SplashPage implements OnInit {

  constructor(
    private appStateService: AppStateService,
    private routingService: RoutingService
  ) {}

  async ngOnInit() {
    await this.appStateService.initialise();
    this.routingService.navigateToDefault();
  }

}
