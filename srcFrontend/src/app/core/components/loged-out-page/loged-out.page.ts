import { Component, OnInit } from '@angular/core';
import { RoutingService } from '../../services/routing.service';
import { AppStateService } from '../../services/appstate.service';

@Component({
  selector: 'app-loged-out',
  templateUrl: 'loged-out.page.html',
  styleUrls: ['loged-out.page.scss'],
})
export class LogedOutPage implements OnInit {

  constructor(
    private appStateService: AppStateService,
    private routingService: RoutingService
  ) {}

  ngOnInit() { }

  async login() {
    await this.appStateService.initialise();
    this.routingService.navigateToDefault();
  }

}
