export enum AuthState {
  Unauthenticated,
  Authenticated,
  TenantAuthenticated
}