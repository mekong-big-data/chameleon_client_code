import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { Subject, Observable, EMPTY, from } from 'rxjs';
import { OAuth2Service } from '../services/oauth2.service';
import { catchError, switchMap } from 'rxjs/operators';
import { AppStateService } from '../services/appstate.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class UserContextInterceptor implements HttpInterceptor {

  authService: OAuth2Service;
  appStateService: AppStateService;
  refreshTokenInProgress = false;

  tokenRefreshedSource = new Subject();
  tokenRefreshed$ = this.tokenRefreshedSource.asObservable();

  constructor(
    private injector: Injector) 
  { }

  addUserContextHeaders(request): Observable<HttpRequest<any>> {
    return new Observable(observer => {
      this.authService.getValidAccessToken()
        .then(authToken => {
          if (request.url.indexOf(environment.blobStorageBaseUrl) !== -1) {
            // do not change headers of blob requests
            observer.next(request);
            observer.complete();
            return;
          }
          const headers = {};
          if (authToken) {
            Object.assign(headers, { Authorization: `Bearer ${authToken}` });
          }
          const tenantName = this.appStateService.tenantName;
          if (tenantName) {
            Object.assign(headers, { Tenant: tenantName });
          }
          observer.next(request.clone({ setHeaders: headers }));
          observer.complete();
        })
    });
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    this.authService = this.injector.get(OAuth2Service);
    this.appStateService = this.injector.get(AppStateService);

    return from(this.addUserContextHeaders(request))
      .pipe(
        switchMap(requestWithTokenHeader => {
          return next.handle(requestWithTokenHeader).pipe(
            catchError(response => {
              if (response.status === 401) {
                this.appStateService.logout();
                return EMPTY;
              }

              if (response.error && response.error.errorMessage) {
                throw new Error(response.error.errorMessage);
              }
              throw response;
            })
          )
        })
      );

  }
}
