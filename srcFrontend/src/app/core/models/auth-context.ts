import { TenantUser } from './tenant-user';
import { Tenant } from 'src/app/search/models/tenant';

export class AuthContext {
  tenant: Tenant;
  tenantUser: TenantUser;
}