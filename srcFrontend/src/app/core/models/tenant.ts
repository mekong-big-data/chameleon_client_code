import { RoleType } from '../types/role-type';

export class Tenant {
  tenantId: number;
  name: string;
  displayName: string;
  primaryLogoFilePath: string;
  SuccessfulMatchText: string;
  failedMatchText: string;
  websiteUrl: string;
}