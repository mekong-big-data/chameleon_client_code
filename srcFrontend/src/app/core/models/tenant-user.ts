import { RoleType } from '../types/role-type';

export class TenantUser {
  tenantUserId: number;
  tenantId: number;
  role: RoleType;
  uniqueIdentifier: string;
  emailAddress: string;
}