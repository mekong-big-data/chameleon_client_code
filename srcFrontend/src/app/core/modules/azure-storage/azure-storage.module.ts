import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { azureBlobStorageFactory, BLOB_STORAGE_TOKEN } from './services/token';

@NgModule({
  imports: [BrowserModule, HttpClientModule],
  providers: [
    {
      provide: BLOB_STORAGE_TOKEN,
      useFactory: azureBlobStorageFactory
    }
  ]
})
export class AzureStorageModule {}
