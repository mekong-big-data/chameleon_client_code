import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BlobStorageRequest } from '../types/azure-storage';

@Injectable({
  providedIn: 'root'
})
export class SasGeneratorService {
  constructor(private http: HttpClient) {}

  getSasToken(sasEndpointUrl: string): Observable<BlobStorageRequest> {
    return this.http.get<BlobStorageRequest>(sasEndpointUrl);
  }
}
