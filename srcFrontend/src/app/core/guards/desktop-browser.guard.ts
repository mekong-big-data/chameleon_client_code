import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

import { Platform } from '@ionic/angular';
import { RoutingService } from '../services/routing.service';

@Injectable({
  providedIn: 'root',
})
export class DesktopBrowserGuard implements CanActivate {
  private isMobileDevice(): boolean {
    if (this.platform.is('mobileweb')) {
      return true;
    }
    return false;
  }

  constructor(
    private platform: Platform, 
    private routingService: RoutingService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.isMobileDevice()) {
      this.routingService.navigateToDefault(true);
      return false;
    }
    return true;
  }
}
