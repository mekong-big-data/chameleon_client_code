import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observer, Observable } from 'rxjs';

import { AppStateService } from '../services/appstate.service';
import { AuthState } from '../types/auth-state';
import { RoleType } from '../types/role-type';

@Injectable({
  providedIn: 'root',
})
export class AuthSuperAdminGuard implements CanActivate {

  constructor(
    private appStateService: AppStateService) 
  { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
    | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>| boolean| UrlTree {
      
    return new Observable((observer: Observer<boolean>) => {
      this.appStateService.authContext$
        .subscribe(authContext => {
          if (!authContext) {
            return;
          }
          const isSuperAdmin = authContext.tenantUser.role === RoleType.SuperAdmin;
          observer.next(isSuperAdmin);
          observer.complete();
        })
    });
  }
}
