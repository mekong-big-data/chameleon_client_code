import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observer, Observable } from 'rxjs';

import { AppStateService } from '../services/appstate.service';
import { AuthState } from '../types/auth-state';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {

  constructor(
    private appStateService: AppStateService) 
  { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
    | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>| boolean| UrlTree {
      
    return new Observable((observer: Observer<boolean>) => {
      this.appStateService.authState$
        .subscribe(authState => {
          if (!!authState) {
            observer.next(authState === AuthState.TenantAuthenticated);
            observer.complete();
          }
        })
    });
  }
}
