import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs'
import { HttpClient } from '@angular/common/http';
import { Storage } from '@capacitor/core';
import { environment } from 'src/environments/environment';
import { Router, ActivationStart, Event as NavigationEvent } from '@angular/router';
import { OAuth2Service } from './oauth2.service';
import { AuthState } from '../types/auth-state';
import { AuthContext } from '../models/auth-context';
import { RoleType } from '../types/role-type';
import { LoadingController } from '@ionic/angular';

@Injectable()
export class AppStateService {

  private isInitialised = false;
  private tenantNameKey = 'tenant_name';

  public tenantName: string;
  public authState$ = new BehaviorSubject<AuthState>(null);
  public authContext$ = new BehaviorSubject<AuthContext>(null);

  get isAdmin(): boolean {
    return !!this.authContext$.value &&
      (this.authContext$.value.tenantUser.role === RoleType.Admin 
        || this.authContext$.value.tenantUser.role === RoleType.SuperAdmin)
  } 

  get isSuperAdmin(): boolean {
    return !!this.authContext$.value && this.authContext$.value.tenantUser.role === RoleType.SuperAdmin
  } 

  get isSecureEnvironment(): boolean {
    return window.location.host === 'chameleon-pwa-admin.web.app'
      || window.location.host === 'localhost:8102';
  }
  
  private async persistTenant(tenant: string) {
    await Storage.set({ key: this.tenantNameKey, value: tenant })
  }

  private async recallTenant() {
    const { value } = await Storage.get({ key: this.tenantNameKey });
    return value;
  }

  private async onRouteChanged(event: NavigationEvent) {
    if (!(event instanceof ActivationStart)) {
      return;
    }

    const tenantParam = event.snapshot.paramMap.get('tenant');
    if (!!tenantParam && this.tenantName !== tenantParam) {
      this.tenantName = tenantParam;
      console.log('Tenant changed to', this.tenantName);
      await this.persistTenant(tenantParam);
    }

    if (!this.authState$.getValue()) {
      if (this.router.url.indexOf('login') === -1) {
        await this.initialise();
      }
    }
  }

  private loadAuthContext(): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      if (!this.tenantName) {
        console.log('Unknown tenant, cannot load tenant user');
        this.authContext$.next(null);
        resolve(false);
        return;
      }
      
      const meLookupUrl = this.isSecureEnvironment
        ? `${environment.baseServiceUrls.tenant}/tenantuser/me`
        : `${environment.baseServiceUrls.search}/tenantuser/me`;

      this.http.get<AuthContext>(meLookupUrl)
        .subscribe(authContext => {
          this.authContext$.next(authContext);
          console.log('Loaded tenant user');
          resolve(true);
        }, err => {
          console.log('Failed to fetch tenant user', err);
          this.authContext$.next(null);
          resolve(false);
        })
    })
  }

  private async loadAuthState() {
    if (this.isSecureEnvironment && !await this.authService.authenticate()) {
      this.authState$.next(AuthState.Unauthenticated);
      console.log('Initialised as Unauthenticated');
      return;
    } 

    if (!await this.loadAuthContext()) {
      this.authState$.next(AuthState.Authenticated);
      console.log('Initialised as Authenticated');
      return;
    }

    this.authState$.next(AuthState.TenantAuthenticated);
    console.log('Initialised as TenantAuthenticated');
    return;
  }

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: OAuth2Service,
    private loadingController: LoadingController
  ) { }

  async register(): Promise<void> {
    const persistedTenantName = await this.recallTenant();
    if (!!persistedTenantName) {
      this.tenantName = persistedTenantName;
      console.log('Tenant recalled', this.tenantName);
    }

    this.router.events
      .subscribe(this.onRouteChanged.bind(this));
  }

  async initialise() {
    const loading = await this.loadingController.create({
      message: 'Initialising...',
      cssClass: 'loading-custom-class',
      spinner: 'circles'
    }) as HTMLIonLoadingElement;
    loading.present();
    
    try {
      await this.loadAuthState();
    } catch (err) {
      console.log('Initialise error:', err);
    } finally {
      loading.dismiss();
    }
  }

  async logout(): Promise<void> {
    this.authService.logout(`${this.tenantName}/login`);
  }

}
