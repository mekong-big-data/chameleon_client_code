import { Injectable } from '@angular/core';
import { registerWebPlugin } from '@capacitor/core';
import { OAuth2Client } from '@byteowls/capacitor-oauth2';

import { Plugins, Storage, Browser } from '@capacitor/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class OAuth2Service {

  private hostUrl = `${window.location.protocol}//${window.location.host}/`;
  private signupSigninOpts: any;
  private passwordResetOpts: any;
  private accessTokenKey = 'oauth2_access_token';
  private expiresInKey = 'oauth2_expires_in';

  constructor() {
    this.signupSigninOpts = Object.assign({}, environment.oath2.signupSigninOpts);
    this.signupSigninOpts.web.redirectUrl = this.hostUrl;
    this.passwordResetOpts = Object.assign({}, environment.oath2.passwordResetOpts);
    this.passwordResetOpts.web.redirectUrl = this.hostUrl;
  }
  
  register() {
    registerWebPlugin(OAuth2Client);
  }

  async getValidAccessToken(): Promise<string> {
    const expiresInPair = await Storage.get({ key: this.expiresInKey });
    const expiryDiff = new Date(expiresInPair.value).getTime() - new Date().getTime();
    if (expiresInPair.value && expiryDiff > 0) {
      const accessTokenPair = await Storage.get({ key: this.accessTokenKey });
      return accessTokenPair.value;
    }
    return null;
  }

  async authenticate(): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      const accessToken = await this.getValidAccessToken();
      if (!!accessToken) {
        // continue using persisted access token
        console.log('Restored auth.');
        resolve(true);
      } else {
        // authenticate / re-authenticate

        console.log('Authenticating...');

        Plugins.OAuth2Client.authenticate(this.signupSigninOpts)
          .then(async signupSigninResp => {

            if (signupSigninResp.error_description && signupSigninResp.error_description.startsWith('AADB2C90118')) {
              // forgot password
              Plugins.OAuth2Client.authenticate(this.passwordResetOpts)
                .then(async passwordResetResp => {
                  await Storage.set({ key: this.accessTokenKey, value: passwordResetResp['access_token'] });
                  await Storage.set({ key: this.expiresInKey, value: passwordResetResp['expires_in'] });

                  console.log('Authenticated after password reset.');

                  resolve(true);
                }).catch(async reason => {
                  reject('OAuth authenticate faile for password reset: ' + reason);
                });

            } else {
              await Storage.set({ key: this.accessTokenKey, value: signupSigninResp['access_token'] });
              await Storage.set({ key: this.expiresInKey, value: signupSigninResp['expires_in'] });

              console.log('Authenticated.');

              resolve(true);
            }

          }).catch(async reason => {
            reject('OAuth authenticate failed: ' + reason);
          });
      }
    });
  }

  async refresh(): Promise<void> {
    return new Promise((resolve, reject) => {
      // not supported in Web context (@byteowls/capacitor-oauth2)
      reject();
    });
  }

  async logout(loginPath: string): Promise<void> {
    return new Promise(async (resolve, reject) => { 
      await Plugins.OAuth2Client.logout(this.signupSigninOpts);
      
      await Storage.remove({ key: this.accessTokenKey });
      await Storage.remove({ key: this.expiresInKey });
  
      const redirectUrl = `${this.hostUrl}/${loginPath}`
      window.location.replace(`${environment.oath2.logoutUrl}?post_logout_redirect_uri=${redirectUrl}`);
      // resolve();
    });
  }

}
