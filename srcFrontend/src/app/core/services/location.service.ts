import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class LocationService {

    constructor(private http: HttpClient) { }
    
    public getIPAddress(): Observable<string> {
      return this.http.jsonp(environment.ipifyIpLookupUrl, 'callback').pipe(
        map((response: any) => {
            return response.ip
          })
      );
    }
}
