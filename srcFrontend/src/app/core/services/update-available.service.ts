import { Injectable } from '@angular/core';
import { Subject, interval } from 'rxjs';
import { SwUpdate } from '@angular/service-worker';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class UpdateAvailableService {
  public isUpdateAvailable: Subject<boolean>;

  constructor(
    private swUpdate: SwUpdate,
    private toastController: ToastController
  ) {
    if (swUpdate.isEnabled) {
      interval(6 * 60 * 60).subscribe(() =>
        swUpdate.checkForUpdate().then(() => this.checkForUpdates())
      );
    }
  }

  public checkForUpdates(): void {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe((event) => {
        this.presentToast();
      });
    }
  }

  async presentToast() {
    const toast = await this.toastController.create({
      duration: 120000,
      color: 'dark',
      message: 'New Update available! Reload the app to see the latest updates.',
      position: 'bottom',
      buttons: [
        {
          text: 'Reload',
          role: 'ShowToaster',
          cssClass: 'secondary',
          handler: () => {
            this.reloadApp();
          },
        },
      ],
    });
    toast.present();
  }

  private reloadApp(): void {
    this.swUpdate.activateUpdate().then(() => document.location.reload());
  }

  checkUpdateAvailable() {
    // lazy way of disabling service workers while developing
    if (
      'serviceWorker' in navigator &&
      ['localhost', '8100'].indexOf(location.hostname) === -1
    ) {
      // register service worker file
      navigator.serviceWorker
        .register('ngsw-worker.js')
        .then((reg) => {
          reg.onupdatefound = () => {
            const installingWorker = reg.installing;
            installingWorker.onstatechange = () => {
              switch (installingWorker.state) {
                case 'installed':
                  if (navigator.serviceWorker.controller) {
                    // new update available
                    return this.isUpdateAvailable.next(true);
                  } else {
                    // no update available
                    return this.isUpdateAvailable.next(false);
                  }
                  break;
              }
            };
          };
        })
        .catch((err) => console.error('[SW ERROR]', err));
    }
  }
}
