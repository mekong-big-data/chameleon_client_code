import { Injectable } from '@angular/core';
import { AppStateService } from './appstate.service';
import { NavController } from '@ionic/angular';

@Injectable()
export class RoutingService {

  constructor(
    private appStateService: AppStateService,
    private navController: NavController
  ) { }

  navigateRoot(route: string | string[]) {
    const tenantName = this.appStateService.tenantName;
    if (!tenantName) {
      this.navController.navigateRoot('');
    } else {
      if (typeof route === 'string') {
        this.navController.navigateRoot(`/${tenantName}/${route}`);
      } else {
        const newRoute = [tenantName].concat(route);
        this.navController.navigateRoot(newRoute);
      }
    }
  }

  navigateForward(route: string | string[]) {
    const tenantName = this.appStateService.tenantName;
    if (!tenantName) {
      this.navController.navigateRoot('');
    } else {
      if (typeof route === 'string') {
        this.navController.navigateForward(`/${tenantName}/${route}`);
      } else {
        const newRoute = [tenantName].concat(route);
        this.navController.navigateForward(newRoute);
      }
    }
  }

  navigateBack(route: string | string[]) {
    const tenantName = this.appStateService.tenantName;
    if (!tenantName) {
      this.navController.navigateRoot('');
    } else {
      if (typeof route === 'string') {
        this.navController.navigateBack(`/${tenantName}/${route}`);
      } else {
        const newRoute = [tenantName].concat(route);
        this.navController.navigateBack(newRoute);
      }
    }
  }
  
  navigateToDefault(mobileOnly?: boolean) {
    const tenantName = this.appStateService.tenantName;
    if (!tenantName) {
      this.navController.navigateRoot('');
    } else if (this.appStateService.isAdmin && !mobileOnly) {
      this.navController.navigateForward(`/${tenantName}/manage`);
    } else {
      this.navController.navigateForward(`/${tenantName}/search`);
    }
  }
  
  navigateToNewTenant(tenantName: string, route: string | string[]) {
    if (!tenantName) {
      return;
    } else {
      this.navController.navigateForward(`/${tenantName}/${route}`);
    }
  }
}
