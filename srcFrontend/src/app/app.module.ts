import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { RouterModule } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule, HttpClientJsonpModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AzureStorageModule } from './core/modules/azure-storage/azure-storage.module';

import { environment } from '../environments/environment';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { DesktopBrowserGuard } from './core/guards/desktop-browser.guard';
import { FileHelperService } from './core/services/filehelper.service';

import { OAuth2Service } from './core/services/oauth2.service';
import { UserContextInterceptor } from './core/interceptors/user-context.interceptor';

import { AuthGuard } from './core/guards/auth.guard';
import { AuthAdminGuard } from './core/guards/auth-admin.guard';
import { AppStateService } from './core/services/appstate.service';
import { RoutingService } from './core/services/routing.service';

import { AppComponent } from './app.component';
import { LogedOutPage } from './core/components/loged-out-page/loged-out.page';
import { SplashPage } from './core/components/splash-page/splash.page';

@NgModule({
  declarations: [
    AppComponent,
    LogedOutPage,
    SplashPage
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,
    IonicModule.forRoot(),
    RouterModule.forRoot([
      {
        path: '',
        component: SplashPage,
      },
      {
        path: ':tenant/login',
        component: LogedOutPage,
      },
      {
        path: ':tenant/search',
        canActivate: [AuthGuard],
        loadChildren: () =>
          import('./search/search.module').then((m) => m.SearchModule),
      },
      {
        path: ':tenant/manage',
        canActivate: [AuthGuard, AuthAdminGuard, DesktopBrowserGuard],
        loadChildren: () =>
          import('./manage/manage.module').then((m) => m.ManageModule),
      },
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full',
      },
    ]),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      registrationStrategy: 'registerImmediately',
    }),
    AzureStorageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FileHelperService,
    OAuth2Service,
    AppStateService,
    RoutingService,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: UserContextInterceptor, multi: true },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
