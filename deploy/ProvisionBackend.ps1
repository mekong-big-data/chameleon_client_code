﻿param(
    [string] $solutionName,
    [string] $databasePassword
)

$random = 'Xyz';
$region = 'australiaeast';
$databaseAdminUsername = 'sqladmin';

$randomLowerCase = $($random).ToLower();
$solutionNameLowerCase = $($solutionName).ToLower();

$resourceGroupName = "$($solutionName)Rg";
$containerRegistryName = "$($solutionName)$($random)Registry";
$containerName = "default/$($solutionNameLowerCase):latest";
$appServicePlanName = "$($solutionName)AppServicePlan";
$applicationName = "$($solutionName)$($random)Application";
$containerWebHookName = "$($solutionName)ContainerWebHook";
$sqlServerName = "$($solutionNameLowerCase)$($randomLowerCase)sqlserver";
$sqlServerDatabaseName = "$($solutionNameLowerCase)database";
$sqlServerDatabasePassword = "$($databasePassword)";
$storageAccountName = "$($solutionNameLowerCase)$($randomLowerCase)storage";
$storageQueueName = "$($solutionNameLowerCase)$($randomLowerCase)queue";
$storageContainerName = "$($solutionNameLowerCase)container";

Write-Host 'Provisioning started...';


# ---
Write-Host 'Attempting login...';
try {
    az login
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


# ---
Write-Host 'Create the Resource Group and Container Registry...';
try {
    az group create --name $resourceGroupName --location $region

    az acr create -n $containerRegistryName -g $resourceGroupName --sku Basic --admin-enabled true

    az acr build -t $containerName -r $containerRegistryName .
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


# ---
Write-Host 'Create the App Service Plan and Web App...';
try {
    az appservice plan create -n $appServicePlanName -g $resourceGroupName --is-linux --sku B1

    $username = $(az acr credential show --name $containerRegistryName -g $resourceGroupName --query "username" --output tsv)
    $password = $(az acr credential show --name $containerRegistryName -g $resourceGroupName --query "passwords[0].value" --output tsv)
    az webapp create -n $applicationName -g $resourceGroupName -p $appServicePlanName -i "$($containerRegistryName).azurecr.io/$($containerName)" -s $username -w $password

    az webapp config appsettings set -g $resourceGroupName -n $applicationName --settings PORT=80
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


# ---
Write-Host 'Enable Continuous Delivery...';
try {
    az webapp deployment container config -n $applicationName -g $resourceGroupName -e true

    $continuousDeliveryUrl = $(az webapp deployment container show-cd-url -n $applicationName -g $resourceGroupName --query "CI_CD_URL")
    az acr webhook create -r $containerRegistryName -g $resourceGroupName -n $containerWebHookName --actions push --uri $continuousDeliveryUrl --scope $containerName
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


# ---
Write-Host 'Create the SQL Server Database...';
try {
    az sql server create -l $region -g $resourceGroupName -n $sqlServerName -u $databaseAdminUsername -p $sqlServerDatabasePassword
    
    az sql server firewall-rule create -g $resourceGroupName --server $sqlServerName -n AllowAllAzureIps --start-ip-address 0.0.0.0 --end-ip-address 0.0.0.0
    # todo: open up RDP port and whitelist own IP

    az sql db create -g $resourceGroupName -s $sqlServerName -n $sqlServerDatabaseName --service-objective Basic

    az webapp config appsettings set -g $resourceGroupName -n $applicationName --settings ConnectionString="Data Source=tcp:$($sqlServerName).database.windows.net,1433;Initial Catalog=$($sqlServerDatabaseName);User Id=$($databaseAdminUsername);Password=$($sqlServerDatabasePassword);"
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


# ---
Write-Host 'Create the Storage Account, Storage Queue and Container...';
try {
    az storage account create -n $storageAccountName -g $resourceGroupName -l $region --sku Standard_LRS

    $storageConnectionString = az storage account show-connection-string -n $storageAccountName -g $resourceGroupName --query connectionString -o tsv

    az webapp config appsettings set -g $resourceGroupName -n $applicationName --settings AZURE_STORAGE_ACCOUNT_CONNECTION=$storageConnectionString
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


Write-Host 'Provisioning complete.';

# clean up
#az group delete -n $resourceGroupName