﻿param(
    [string] $solutionName,
	[string] $serviceName
)

$random = 'Xyz';
$region = 'australiaeast';

$randomLowerCase = $($random).ToLower();
$solutionNameLowerCase = $($solutionName).ToLower();

$resourceGroupName = "$($solutionName)WindowsRg";
$functionAppName = "$($solutionName)$($serviceName)$($random)Functions";
$storageAccountName = "$($solutionNameLowerCase)$($randomLowerCase)storage";
$storageQueueName = "$($solutionNameLowerCase)$($randomLowerCase)queue";

Write-Host 'Provisioning Function started...';


# ---
Write-Host 'Attempting login...';
try {
    az login
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


# ---
Write-Host 'Create the Resource Group...';
try {
	az group create --name $resourceGroupName --location $region
}
catch {
	Write-Host 'Failed.';
	exit;
}
# ---


# ---
Write-Host 'Create Function app service...';
try {
	az functionapp create -n $functionAppName --storage-account $storageAccountName --consumption-plan-location $region --runtime dotnet -g $resourceGroupName --functions-version 3.1
	  
	az functionapp update -g $resourceGroupName -n $functionAppName --set dailyMemoryTimeQuota=50000
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


# ---
Write-Host 'Attach Storage Connection String to Function app...';
try {
    $storageConnectionString = az storage account show-connection-string -n $storageAccountName --query connectionString -o tsv

    az webapp config appsettings set -g $resourceGroupName -n $functionAppName --settings AzureWebJobsStorage=$storageConnectionString
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


Write-Host 'Provisioning Function complete.';

# clean up
#az group delete -n $resourceGroupName