﻿param(
    [string] $solutionName
)

$random = 'Xyz';
$solutionNameLowerCase = $($solutionName).ToLower();

$containerRegistryName = "$($solutionName)$($random)Registry";
$containerName = "default/$($solutionNameLowerCase)managefunctions:latest";

Write-Host 'Deployment started...';


# ---
Write-Host 'Attempting login...';
try {
    az login
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


# ---
Write-Host 'Building and uploading container...';
try {
    az acr build -t $containerName -r $containerRegistryName . -f DockerfileFunctions
}
catch {
    Write-Host 'Failed.';
    exit;
}
# ---


Write-Host 'Deployment complete.';