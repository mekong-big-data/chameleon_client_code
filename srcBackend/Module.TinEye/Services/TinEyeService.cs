﻿using Module.TinEye.Models;
using Refit;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Module.TinEye.Services
{
    public class TinEyeService : ITinEyeService
    {

        public async Task<ResultModel> AddImage(string url, string filepath, string username, string password, CancellationToken cancellationToken)
        {
            // todo: refactor out duplicate code
            var tinEyeCredentials = username + ":" + password;
            var tinEyeAuthorization = $"Basic {Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(tinEyeCredentials))}";
            var tinEyeService = RestService.For<IRefitTinEyeService>($"https://matchengine.tineye.com/{username}/rest");

            try
            {
                return await tinEyeService.AddImage(url, filepath, tinEyeAuthorization, cancellationToken);
            }
            catch (ApiException ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public async Task<ResultModel> DeleteImage(string filepath, string username, string password, CancellationToken cancellationToken)
        {
            // todo: refactor out duplicate code
            var tinEyeCredentials = username + ":" + password;
            var tinEyeAuthorization = $"Basic {Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(tinEyeCredentials))}";
            var tinEyeService = RestService.For<IRefitTinEyeService>($"https://matchengine.tineye.com/{username}/rest");

            try
            {
                return await tinEyeService.DeleteImage(filepath, tinEyeAuthorization, cancellationToken);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public async Task<SearchResultModel> SearchImage(string filepath, bool generateOverlay, string username, string password, CancellationToken cancellationToken)
        {
            // todo: refactor out duplicate code
            var tinEyeCredentials = username + ":" + password;
            var tinEyeAuthorization = $"Basic {Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(tinEyeCredentials))}";
            var tinEyeService = RestService.For<IRefitTinEyeService>($"https://matchengine.tineye.com/{username}/rest");

            try
            {
                return await tinEyeService.SearchImage(filepath, generateOverlay, tinEyeAuthorization, cancellationToken);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string GenerateImageUrl(string filePath, string tinEyeUsername)
        {
            return $"https://matchengine.tineye.com/{tinEyeUsername}/collection/?filepath={filePath}";
        }
    }
}
