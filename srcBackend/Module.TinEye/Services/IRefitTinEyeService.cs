﻿using Module.TinEye.Models;
using Refit;
using System.Threading;
using System.Threading.Tasks;

namespace Module.TinEye.Services
{
    public interface IRefitTinEyeService
    {
        // If we have to upload image directly then we have to pass image stream
        // For now we are using image url
        [Post("/add")]
        Task<ResultModel> AddImage(string url, string filepath, [Header("Authorization")] string authorization, CancellationToken cancellationToken);

        [Get("/delete")]
        Task<ResultModel> DeleteImage(string filepath, [Header("Authorization")] string authorization, CancellationToken cancellationToken);

        [Post("/search")]
        Task<SearchResultModel> SearchImage(string url, bool generate_overlay, [Header("Authorization")] string authorization, CancellationToken cancellationToken);

        string GenerateImageUrl(string filePath, string tinEyeUsername);
    }
}
