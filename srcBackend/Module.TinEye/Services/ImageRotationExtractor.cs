﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Module.TinEye.Services
{
    public static class ImageRotationExtractor
    {

        public static double? ExtractRotation(string overlayUrl)
        {
            // extract the m21 and m22 parameter values out of the overlay URL querystring returned by TinEye (example below)
            // overlay/?query=query.jpg&target=path/folder/match1.png&m11=1.00001&m12=-3.32728e-05&m13=0.00242224&m21=3.32728e-05&m22=1.00001&m23=-0.00711411
            if (string.IsNullOrEmpty(overlayUrl))
            {
                return null;
            }

            var overlayUrlParts = overlayUrl.Split('?');
            if (overlayUrlParts.Length != 2)
            {
                return null;
            }

            var queryStringParts = overlayUrlParts[1].Split('&').ToList();
            var m21 = ExtractMatrixValue(queryStringParts, "m21");
            var m22 = ExtractMatrixValue(queryStringParts, "m22");
            if (!m21.HasValue || !m22.HasValue)
            {
                return null;
            }

            // get angle as range -pi to pi
            var angleInRadians = Math.Atan2(m21.Value, m22.Value);
            // get angle in degrees
            var angleInDegrees = 180 * angleInRadians / Math.PI;

            return angleInDegrees;
        }

        private static double? ExtractMatrixValue(IList<string> queryStringParts, string key)
        {
            var param = queryStringParts.SingleOrDefault(x => x.StartsWith($"{key}="));
            if (param == null)
            {
                return null;
            }

            var paramValueString = param.Split('=')[1];
            if (double.TryParse(paramValueString, out var paramValue))
            {
                return paramValue;
            }

            return null;
        }

    }
}
