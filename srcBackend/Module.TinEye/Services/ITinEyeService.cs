﻿using System.Threading;
using System.Threading.Tasks;
using Module.TinEye.Models;

namespace Module.TinEye.Services
{
    public interface ITinEyeService
    {
        Task<ResultModel> AddImage(string url, string filepath, string username, string password, CancellationToken cancellationToken);
        Task<ResultModel> DeleteImage(string filepath, string username, string password, CancellationToken cancellationToken);
        Task<SearchResultModel> SearchImage(string filepath, bool generateOverlay, string username, string password, CancellationToken cancellationToken);
        string GenerateImageUrl(string filePath, string tinEyeUsername);
    }
}