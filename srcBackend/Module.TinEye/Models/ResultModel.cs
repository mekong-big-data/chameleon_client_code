﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Module.TinEye.Models
{
    public class ResultModel
    {
        public string status { get; set; }
        public List<object> error { get; set; }
        public string method { get; set; }
        public List<string> result { get; set; }
    }

    public class AddImage
    {
        public Stream Image { get; set; }
    }

    public class Result
    {
        public double match_percent { get; set; }
        public double score { get; set; }
        public double target_overlap_percent { get; set; }
        public double query_overlap_percent { get; set; }
        public string filepath { get; set; }
        public string overlay { get; set; }
    }

    public class SearchResultModel
    {
        public string status { get; set; }
        public List<object> error { get; set; }
        public string method { get; set; }
        public List<Result> result { get; set; }
    }


}
