﻿using System;
using Refit;
using System.Threading;
using System.Threading.Tasks;
using Service.Manage.Contracts.Models;

namespace Service.Manage.Contracts
{
    [Headers("Authorization: Basic")]
    public interface IManageServiceApi
    {
        [Get("/serviceapi/images")]
        Task<ImageModel> GetByImageFilePath(string imageFilePath, CancellationToken cancellationToken);
    }
}
