﻿namespace Service.Manage.Contracts.Types
{
    public enum CollectionUploadStatus
    {
        Uploaded,
        Processing,
        Complete,
        Discarded,
        Removing,
        Removed
    }
}
