﻿using System;

namespace Service.Manage.Contracts.Models
{
    public class ImageModel
    {
        public int ImageId { get; set; }
        public int CollectionId { get; set; }
        public string Name { get; set; }
        public string ImageFilePath { get; set; }
        public string ImageFileHash { get; set; }
        public string SubjectName { get; set; }
        public string SubjectDescription { get; set; }
        public string SubjectUrl { get; set; }
        public string SubjectImageUrl { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
    }
}
