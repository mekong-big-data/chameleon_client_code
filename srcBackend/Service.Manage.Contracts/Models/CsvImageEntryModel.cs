﻿namespace Service.Manage.Contracts.Models
{
    public class CsvImageEntryModel
    {
        public int Id { get; set; }
        public string SubjectName { get; set; }
        public string SubjectDescription { get; set; }
        public string SubjectUrl { get; set; }
        public string SubjectImageUrl { get; set; }
    }
}
