﻿using Service.Manage.Contracts.Types;

namespace Service.Manage.Contracts.Models
{
    public class CollectionUploadModel
    {
        public int CollectionUploadId { get; set; }
        public int TenantUserId { get; set; }
        public string UploadFileName { get; set; }
        public CollectionUploadStatus Status { get; set; }
    }
}
