﻿using System;

namespace Service.Manage.Contracts.Models
{
    public class CollectionModel
    {
        public int CollectionId { get; set; }
        public int TenantId { get; set; }
        public int CollectionUploadId { get; set; }
        public string Name { get; set; }
        public int? Quantity { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public string DownloadUrl { get; set; }
    }
}
