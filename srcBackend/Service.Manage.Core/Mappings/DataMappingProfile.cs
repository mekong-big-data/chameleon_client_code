﻿using AutoMapper;
using Service.Manage.Core.Database.Entities;
using Service.Manage.Contracts.Models;

namespace Service.Manage.Core.Mappings
{
    public class DataMappingProfile : Profile
    {
        public DataMappingProfile()
        {
            CreateMap<CollectionUploadModel, CollectionUpload>()
               .ForMember(dest => dest.Collection, opt => opt.Ignore())
               .ForMember(dest => dest.CreatedAt, opt => opt.Ignore())
               .ForMember(dest => dest.UpdatedAt, opt => opt.Ignore())
               .ForMember(dest => dest.DeletedAt, opt => opt.Ignore());
            CreateMap<CollectionUpload, CollectionUploadModel>();

            CreateMap<CollectionModel, Collection>()
              .ForMember(dest => dest.Images, opt => opt.Ignore())
              .ForMember(dest => dest.CollectionUpload, opt => opt.Ignore())
              .ForMember(dest => dest.CreatedAt, opt => opt.Ignore())
              .ForMember(dest => dest.UpdatedAt, opt => opt.Ignore())
              .ForMember(dest => dest.DeletedAt, opt => opt.Ignore());
            CreateMap<Collection, CollectionModel>()
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Images.Count))
                .ForMember(dest => dest.DownloadUrl, opt => opt.Ignore());

            CreateMap<ImageModel, Image>()
              .ForMember(dest => dest.Collection, opt => opt.Ignore())
              .ForMember(dest => dest.CreatedAt, opt => opt.Ignore())
              .ForMember(dest => dest.UpdatedAt, opt => opt.Ignore())
              .ForMember(dest => dest.DeletedAt, opt => opt.Ignore());
            CreateMap<Image, ImageModel>();
        }
    }
}
