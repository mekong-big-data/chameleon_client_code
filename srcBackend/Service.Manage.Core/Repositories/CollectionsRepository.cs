﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Service.Manage.Core.Database;
using Service.Manage.Core.Database.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Service.Manage.Contracts.Models;
using Service.Manage.Contracts.Types;
using Service.Manage.Core.Repositories.Interfaces;

namespace Service.Manage.Core.Repositories
{
    public class CollectionsRepository : ICollectionsRepository
    {
        private readonly ManageDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _mapperConfig;

        public CollectionsRepository(
            ManageDataContext dataContext,
            IMapper mapper,
            MapperConfiguration mapperConfig)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _mapperConfig = mapperConfig;
        }

        public async Task<CollectionModel> CreateCollection(CollectionModel model)
        {
            var collection = new Collection();
            _mapper.Map(model, collection);

            await _dataContext.AddAsync(collection);

            await _dataContext.SaveChangesAsync();

            _mapper.Map(collection, model);

            return model;
        }

        public async Task<IList<CollectionModel>> GetCollectionsByTenantId(int tenantId)
        {
            var response = new List<CollectionModel>();
            var collections = await _dataContext.Collections
                .Include(x => x.Images)
                .Where(x => x.TenantId == tenantId && 
                            (x.CollectionUpload.Status == CollectionUploadStatus.Uploaded
                             || x.CollectionUpload.Status == CollectionUploadStatus.Processing
                             || x.CollectionUpload.Status == CollectionUploadStatus.Complete))
                .ProjectTo<CollectionModel>(_mapperConfig).ToListAsync();
            _mapper.Map(collections, response);
            return response;
        }

        public async Task<CollectionModel> GetCollection(int collectionId)
        {
            var response = new CollectionModel();
            var collection = await _dataContext.Collections.FindAsync(collectionId);
            if (collection == null)
            {
                return null;
            }
            _mapper.Map(collection, response);
            return response;
        }

        public async Task<CollectionModel> GetCollectionByCollectionUploadId(int collectionUploadId)
        {
            var response = new CollectionModel();
            var collection = await _dataContext.Collections
                .SingleOrDefaultAsync(c => c.CollectionUploadId == collectionUploadId);
            if (collection == null)
            {
                return null;
            }
            _mapper.Map(collection, response);
            return response;
        }

        public async Task<bool> DeleteCollection(int collectionId)
        {
            var collection = await _dataContext.Collections.FirstOrDefaultAsync(x => x.CollectionId == collectionId);
            if (collection == null)
            {
                throw new ValidationException("Collection not found!");
            }

            _dataContext.Collections.Remove(collection);
            await _dataContext.SaveChangesAsync();

            return true;
        }
    }
}
