﻿using System.Threading.Tasks;
using Service.Manage.Contracts.Models;

namespace Service.Manage.Core.Repositories.Interfaces
{
    public interface ICollectionUploadsRepository
    {
        Task<CollectionUploadModel> GetCollectionUpload(int collectionUploadId);
        Task<CollectionUploadModel> CreateCollectionUpload(CollectionUploadModel model);
        Task<CollectionUploadModel> UpdateCollectionUpload(CollectionUploadModel model);
    }
}