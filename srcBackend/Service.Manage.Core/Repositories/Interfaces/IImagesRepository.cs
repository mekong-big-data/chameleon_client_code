﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Manage.Contracts.Models;

namespace Service.Manage.Core.Repositories.Interfaces
{
    public interface IImagesRepository
    {
        Task<IList<ImageModel>> GetImages(int collectionId);
        Task<ImageModel> GetImage(int imageId);
        Task<ImageModel> GetImageByImageFilePath(string imageFilePath);
        Task<ImageModel> GetImageByImageHash(int tenantId, string imageHash);
        Task<ImageModel> CreateImage(ImageModel model);
        Task<ImageModel> UpdateImage(ImageModel model);
        Task<bool> DeleteImage(int imageId);
    }
}