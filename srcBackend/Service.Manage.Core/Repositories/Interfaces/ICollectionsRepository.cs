﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Manage.Contracts.Models;

namespace Service.Manage.Core.Repositories.Interfaces
{
    public interface ICollectionsRepository
    {
        Task<CollectionModel> CreateCollection(CollectionModel model);
        Task<IList<CollectionModel>> GetCollectionsByTenantId(int tenantId);
        Task<CollectionModel> GetCollection(int collectionId);
        Task<CollectionModel> GetCollectionByCollectionUploadId(int collectionUploadId);
        Task<bool> DeleteCollection(int collectionId);
    }
}