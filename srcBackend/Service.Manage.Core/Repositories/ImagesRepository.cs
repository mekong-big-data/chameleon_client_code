﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Service.Manage.Core.Database;
using Service.Manage.Core.Database.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Service.Manage.Contracts.Models;
using Service.Manage.Core.Repositories.Interfaces;

namespace Service.Manage.Core.Repositories
{
    public class ImagesRepository : IImagesRepository
    {
        private readonly ManageDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _mapperConfig;

        public ImagesRepository(
            ManageDataContext dataContext,
            IMapper mapper,
            MapperConfiguration mapperConfig)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _mapperConfig = mapperConfig;
        }

        public async Task<IList<ImageModel>> GetImages(int collectionId)
        {
            var response = new List<ImageModel>();
            var images = await _dataContext.Images
                .Where(i => i.CollectionId == collectionId)
                .ProjectTo<ImageModel>(_mapperConfig)
                .ToListAsync();
            _mapper.Map(images, response);
            return response;
        }

        public async Task<ImageModel> GetImage(int imageId)
        {
            var image = await _dataContext.Images.FindAsync(imageId);
            if (image == null)
            {
                return null;
            }
            var response = new ImageModel();
            _mapper.Map(image, response);
            return response;
        }

        public async Task<ImageModel> GetImageByImageFilePath(string imageFilePath)
        {
            var image = await _dataContext.Images.SingleOrDefaultAsync(x => x.ImageFilePath == imageFilePath);
            if (image == null)
            {
                return null;
            }
            var response = new ImageModel();
            _mapper.Map(image, response);
            return response;
        }

        public async Task<ImageModel> GetImageByImageHash(int tenantId, string imageHash)
        {
            var image = await _dataContext.Images
                .SingleOrDefaultAsync(x => x.Collection.TenantId == tenantId && x.ImageFileHash == imageHash);
            if (image == null)
            {
                return null;
            }
            var response = new ImageModel();
            _mapper.Map(image, response);
            return response;
        }

        public async Task<bool> DoesImageFileHashExist(string imageFileHash)
        {
            return await _dataContext.Images.AnyAsync(x => x.ImageFileHash == imageFileHash);
        }

        public async Task<ImageModel> CreateImage(ImageModel model)
        {
            var image = new Image();
            _mapper.Map(model, image);

            await _dataContext.AddAsync(image);
            await _dataContext.SaveChangesAsync();

            _mapper.Map(image, model);

            return model;
        }

        public async Task<ImageModel> UpdateImage(ImageModel model)
        {
            var image = await _dataContext.Images.FirstOrDefaultAsync(x => x.ImageId == model.ImageId);
            if (image == null)
            {
                throw new ValidationException("Image not found!");
            }

            _mapper.Map(model, image);

            await _dataContext.SaveChangesAsync();

            return model;
        }

        public async Task<bool> DeleteImage(int imageId)
        {
            var image = await _dataContext.Images.FirstOrDefaultAsync(x => x.ImageId == imageId);
            if (image == null)
            {
                throw new ValidationException("Image not found!");
            }

            _dataContext.Images.Remove(image);
            await _dataContext.SaveChangesAsync();

            return true;
        }
    }
}
