﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Service.Manage.Core.Database;
using Service.Manage.Core.Database.Entities;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Service.Manage.Contracts.Models;
using Service.Manage.Core.Repositories.Interfaces;

namespace Service.Manage.Core.Repositories
{
    public class CollectionUploadsRepository : ICollectionUploadsRepository
    {
        private readonly ManageDataContext _dataContext;
        private readonly MapperConfiguration _mapperConfig;
        private readonly IMapper _mapper;

        public CollectionUploadsRepository(
            ManageDataContext dataContext,
            MapperConfiguration mapperConfig,
            IMapper mapper)
        {
            _dataContext = dataContext;
            _mapperConfig = mapperConfig;
            _mapper = mapper;
        }


        public async Task<CollectionUploadModel> GetCollectionUpload(int collectionUploadId)
        {
            var upload = await _dataContext.CollectionUploads.FindAsync(collectionUploadId);
            if (upload == null)
            {
                return null;
            }
            var response = new CollectionUploadModel();
            _mapper.Map(upload, response);
            return response;
        }

        public async Task<CollectionUploadModel> CreateCollectionUpload(CollectionUploadModel model)
        {
            var collectionUpload = new CollectionUpload();
            _mapper.Map(model, collectionUpload);

            await _dataContext.AddAsync(collectionUpload);

            await _dataContext.SaveChangesAsync();

            _mapper.Map(collectionUpload, model);

            return model;
        }

        public async Task<CollectionUploadModel> UpdateCollectionUpload(CollectionUploadModel model)
        {
            var collectionUpload = await _dataContext.CollectionUploads.FirstOrDefaultAsync(x => x.CollectionUploadId == model.CollectionUploadId);
            if (collectionUpload == null)
            {
                throw new ValidationException("CollectionUpload not found!");
            }
            _mapper.Map(model, collectionUpload);

            await _dataContext.SaveChangesAsync();

            return model;
        }
    }
}
