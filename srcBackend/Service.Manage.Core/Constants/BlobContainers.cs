﻿namespace Service.Manage.Core.Constants
{
    public static class BlobContainers
    {
        public static string ManageContainer = "manage";
        public static string UploadContainer = "upload";
        public static string CollectionContainer = "collection";
    }
}
