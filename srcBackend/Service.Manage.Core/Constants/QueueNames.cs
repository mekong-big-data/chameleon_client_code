﻿namespace Service.Manage.Core.Constants
{
    public static class QueueNames
    {
        public const string CollectionUpload = "manage-function-collectionupload";
        public const string CollectionRemoval = "manage-function-collectionremoval";
    }
}
