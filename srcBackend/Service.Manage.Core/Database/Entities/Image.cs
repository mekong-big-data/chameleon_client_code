﻿using System.ComponentModel.DataAnnotations;
using Module.EntityFramework;

namespace Service.Manage.Core.Database.Entities
{
    public class Image : EntityBase
    {
        [Key]
        public int ImageId { get; set; }
        public int CollectionId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(250)]
        public string ImageFilePath { get; set; }
        [MaxLength(44)]
        public string ImageFileHash { get; set; }
        public string SubjectName { get; set; }
        public string SubjectDescription { get; set; }
        public string SubjectUrl { get; set; }
        public string SubjectImageUrl { get; set; }

        public virtual Collection Collection { get; set; }
    }
}
