﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Module.EntityFramework;

namespace Service.Manage.Core.Database.Entities
{
    public class Collection : EntityBase
    {
        [Key]
        public int CollectionId { get; set; }
        public int CollectionUploadId { get; set; }
        public int TenantId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }

        public virtual CollectionUpload CollectionUpload { get; set; }
        public virtual ICollection<Image> Images { get; set; }
    }
}
