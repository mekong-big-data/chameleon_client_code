﻿using System.ComponentModel.DataAnnotations;
using Module.EntityFramework;
using Service.Manage.Contracts.Types;

namespace Service.Manage.Core.Database.Entities
{
    public class CollectionUpload : EntityBase
    {
        [Key]
        public int CollectionUploadId { get; set; }
        public int TenantUserId { get; set; }
        [MaxLength(100)]
        public string UploadFileName { get; set; }
        public CollectionUploadStatus Status { get; set; }

        public virtual Collection Collection { get; set; }

    }
}
