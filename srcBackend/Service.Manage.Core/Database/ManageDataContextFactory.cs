﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Manage.Core.Database
{

    public class ManageDataContextFactory : IDesignTimeDbContextFactory<ManageDataContext>
    {
        public ManageDataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ManageDataContext>();
            var connectionString = @"Server=localhost;Initial Catalog=chameleon-local;Integrated Security=True";
            optionsBuilder.UseSqlServer(
                new SqlConnection(connectionString),
                x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, ManageDataContext.ContextSchema));
            return new ManageDataContext(optionsBuilder.Options);
        }
    }
}
