﻿using Microsoft.EntityFrameworkCore;
using Module.EntityFramework;
using Service.Manage.Core.Database.Entities;

namespace Service.Manage.Core.Database
{
    public class ManageDataContext : DataContextBase<ManageDataContext>
    {

        public static string ContextSchema = "manage";

        public DbSet<Collection> Collections { get; set; }
        public DbSet<CollectionUpload> CollectionUploads { get; set; }
        public DbSet<Image> Images { get; set; }

        public ManageDataContext(DbContextOptions<ManageDataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(ContextSchema);

            modelBuilder.Entity<CollectionUpload>()
                .HasOne(x => x.Collection)
                .WithOne(x => x.CollectionUpload)
                .HasForeignKey<Collection>(x => x.CollectionUploadId);

            modelBuilder.Entity<Collection>()
                .HasMany(x => x.Images)
                .WithOne(x => x.Collection)
                .HasForeignKey(x => x.CollectionId);

            modelBuilder.Entity<CollectionUpload>().HasQueryFilter(p => !p.DeletedAt.HasValue);
            modelBuilder.Entity<Collection>().HasQueryFilter(p => !p.DeletedAt.HasValue);
            modelBuilder.Entity<Image>().HasQueryFilter(p => !p.DeletedAt.HasValue);
        }

    }
}
