﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Manage.Core.Migrations
{
    public partial class AddedTenantFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TenantUserId",
                schema: "manage",
                table: "CollectionUploads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                schema: "manage",
                table: "Collections",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenantUserId",
                schema: "manage",
                table: "CollectionUploads");

            migrationBuilder.DropColumn(
                name: "TenantId",
                schema: "manage",
                table: "Collections");
        }
    }
}
