﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Manage.Core.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "manage");

            migrationBuilder.CreateTable(
                name: "CollectionUploads",
                schema: "manage",
                columns: table => new
                {
                    CollectionUploadId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(nullable: true),
                    UploadFileName = table.Column<string>(maxLength: 100, nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CollectionUploads", x => x.CollectionUploadId);
                });

            migrationBuilder.CreateTable(
                name: "Collections",
                schema: "manage",
                columns: table => new
                {
                    CollectionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(nullable: true),
                    CollectionUploadId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Collections", x => x.CollectionId);
                    table.ForeignKey(
                        name: "FK_Collections_CollectionUploads_CollectionUploadId",
                        column: x => x.CollectionUploadId,
                        principalSchema: "manage",
                        principalTable: "CollectionUploads",
                        principalColumn: "CollectionUploadId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Images",
                schema: "manage",
                columns: table => new
                {
                    ImageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(nullable: true),
                    CollectionId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    ImageFilePath = table.Column<string>(maxLength: 250, nullable: true),
                    ImageFileHash = table.Column<string>(maxLength: 44, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.ImageId);
                    table.ForeignKey(
                        name: "FK_Images_Collections_CollectionId",
                        column: x => x.CollectionId,
                        principalSchema: "manage",
                        principalTable: "Collections",
                        principalColumn: "CollectionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Collections_CollectionUploadId",
                schema: "manage",
                table: "Collections",
                column: "CollectionUploadId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Images_CollectionId",
                schema: "manage",
                table: "Images",
                column: "CollectionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Images",
                schema: "manage");

            migrationBuilder.DropTable(
                name: "Collections",
                schema: "manage");

            migrationBuilder.DropTable(
                name: "CollectionUploads",
                schema: "manage");
        }
    }
}
