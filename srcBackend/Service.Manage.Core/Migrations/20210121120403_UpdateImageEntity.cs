﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Manage.Core.Migrations
{
    public partial class UpdateImageEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SubjectDescription",
                schema: "manage",
                table: "Images",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SubjectImageUrl",
                schema: "manage",
                table: "Images",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SubjectName",
                schema: "manage",
                table: "Images",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SubjectUrl",
                schema: "manage",
                table: "Images",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubjectDescription",
                schema: "manage",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "SubjectImageUrl",
                schema: "manage",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "SubjectName",
                schema: "manage",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "SubjectUrl",
                schema: "manage",
                table: "Images");
        }
    }
}
