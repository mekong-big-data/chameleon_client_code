﻿using Microsoft.EntityFrameworkCore;
using Module.EntityFramework;
using Service.Search.Core.Database.Entities;

namespace Service.Search.Core.Database
{
    public class SearchDataContext : DataContextBase<SearchDataContext>
    {

        public static string ContextSchema = "search";

        public DbSet<ImageSearch> ImageSearches { get; set; }

        public SearchDataContext(DbContextOptions<SearchDataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(ContextSchema);

            modelBuilder.Entity<ImageSearch>().HasQueryFilter(p => !p.DeletedAt.HasValue);
        }

    }
}
