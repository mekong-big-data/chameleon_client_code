﻿using System.ComponentModel.DataAnnotations;
using Module.EntityFramework;
using NetTopologySuite.Geometries;

namespace Service.Search.Core.Database.Entities
{
    public class ImageSearch : EntityBase
    {
        public int ImageSearchId { get; set; }
        public int TenantUserId { get; set; }
        public int? MatchingImageId { get; set; }
        [MaxLength(250)]
        public string ImageFileName { get; set; }
        public double Score { get; set; }
        public double MatchPercent { get; set; }
        public double QueryOverlapPercent { get; set; }
        public double TargetOverlapPercent { get; set; }
        [MaxLength(250)]
        public string MatchFileName { get; set; }
        [MaxLength(32)]
        public string IpAddress { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public Geometry Location { get; set; }
        public double? Rotation { get; set; }

    }
}
