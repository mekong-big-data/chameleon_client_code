﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Search.Core.Database
{
    public class SearchDataContextFactory : IDesignTimeDbContextFactory<SearchDataContext>
    {
        public SearchDataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SearchDataContext>();
            var connectionString = @"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=ChameleonDb;Integrated Security=SSPI";
            optionsBuilder.UseSqlServer(
                new SqlConnection(connectionString),
                x =>
                {
                    x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, SearchDataContext.ContextSchema);
                    x.UseNetTopologySuite();
                });

            return new SearchDataContext(optionsBuilder.Options);
        }
    }
}
