﻿using System.Threading.Tasks;
using AutoMapper;
using Service.Search.Contracts.Models;
using Service.Search.Core.Database;
using Service.Search.Core.Database.Entities;
using Service.Search.Core.Repositories.Interfaces;

namespace Service.Search.Core.Repositories
{
    public class ImageSearchRepository : IImageSearchRepository
    {

        private readonly SearchDataContext _dataContext;
        private readonly IMapper _mapper;

        public ImageSearchRepository(
            SearchDataContext dataContext,
            IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<ImageSearchModel> GetImageSearch(int imageSearchId)
        {
            var imageSearch = await _dataContext.ImageSearches.FindAsync(imageSearchId);

            var model = new ImageSearchModel();
            _mapper.Map(imageSearch, model);

            return model;
        }

        public async Task<ImageSearchModel> CreateImageSearch(ImageSearchModel model)
        {
            var imageSearch = new ImageSearch();
            _mapper.Map(model, imageSearch);

            await _dataContext.AddAsync(imageSearch);
            await _dataContext.SaveChangesAsync();

            _mapper.Map(imageSearch, model);

            return model;
        }
    }
}
