﻿using System.Threading.Tasks;
using Service.Search.Contracts.Models;

namespace Service.Search.Core.Repositories.Interfaces
{
    public interface IImageSearchRepository
    {
        Task<ImageSearchModel> GetImageSearch(int imageSearchId);
        Task<ImageSearchModel> CreateImageSearch(ImageSearchModel model);
    }
}