﻿using AutoMapper;
using Common.Core.Utilities;
using NetTopologySuite.Geometries;
using Service.Search.Contracts.Models;
using Service.Search.Core.Database.Entities;

namespace Service.Search.Core.Mappings
{
    public class DataMappingProfile : Profile
    {

        public DataMappingProfile()
        {
            CreateMap<ImageSearchModel, ImageSearch>()
                .ForMember(dest => dest.Location, opt =>
                    opt.MapFrom(src => src.Latitude != null && src.Longitude != null ? (Geometry)((new Coordinate(src.Longitude.Value, src.Latitude.Value)).ToPoint()) : null))
                .ForMember(dest => dest.ImageSearchId, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedAt, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedAt, opt => opt.Ignore())
                .ForMember(dest => dest.DeletedAt, opt => opt.Ignore());
            CreateMap<ImageSearch, ImageSearchModel>()
                .ForMember(dest => dest.ImageKey, opt => opt.Ignore())
                 .ForMember(dest => dest.Latitude, opt =>
                    opt.MapFrom(src => src.Location == null ? (double?)null : src.Location.Coordinates[0].Y))
                .ForMember(dest => dest.Longitude, opt =>
                    opt.MapFrom(src => src.Location == null ? (double?)null : src.Location.Coordinates[0].X))
                .ForMember(dest => dest.MatchFileUrl, opt => opt.Ignore());
        }
    }

}
