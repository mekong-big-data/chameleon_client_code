﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Search.Core.Migrations
{
    public partial class AddedTenantFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TenantUserId",
                schema: "search",
                table: "ImageSearches",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenantUserId",
                schema: "search",
                table: "ImageSearches");
        }
    }
}
