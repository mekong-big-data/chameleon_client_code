﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Search.Core.Migrations
{
    public partial class AddImageSearchRotationProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Rotation",
                schema: "search",
                table: "ImageSearches",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rotation",
                schema: "search",
                table: "ImageSearches");
        }
    }
}
