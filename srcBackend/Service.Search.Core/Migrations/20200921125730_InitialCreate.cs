﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace Service.Search.Core.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "search");

            migrationBuilder.CreateTable(
                name: "ImageSearches",
                schema: "search",
                columns: table => new
                {
                    ImageSearchId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedAt = table.Column<DateTimeOffset>(nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(nullable: true),
                    MatchingImageId = table.Column<int>(nullable: true),
                    ImageFileName = table.Column<string>(maxLength: 250, nullable: true),
                    Score = table.Column<double>(nullable: false),
                    MatchPercent = table.Column<double>(nullable: false),
                    QueryOverlapPercent = table.Column<double>(nullable: false),
                    TargetOverlapPercent = table.Column<double>(nullable: false),
                    MatchFileName = table.Column<string>(maxLength: 250, nullable: true),
                    IpAddress = table.Column<string>(maxLength: 32, nullable: true),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true),
                    Location = table.Column<Geometry>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageSearches", x => x.ImageSearchId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImageSearches",
                schema: "search");
        }
    }
}
