﻿namespace Service.Search.WebApi.Models
{
    public class ImageSearchResultModel
    {
        public int ImageSearchId { get; set; }
        public int TenantUserId { get; set; }
        public int? MatchingImageId { get; set; }
        public string ImageFileName { get; set; }
        public string ImageFileUrl { get; set; }
        public double Score { get; set; }
        public double MatchPercent { get; set; }
        public double QueryOverlapPercent { get; set; }
        public double TargetOverlapPercent { get; set; }
        public string MatchFileName { get; set; }
        public string MatchFileUrl { get; set; }
        public string IpAddress { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public double? Rotation { get; set; }
        public string SubjectName { get; set; }
        public string SubjectDescription { get; set; }
        public string SubjectUrl { get; set; }
        public string SubjectImageUrl { get; set; }
        public string WebsiteUrl { get; set; }
    }
}
