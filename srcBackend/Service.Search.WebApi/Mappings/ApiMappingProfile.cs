﻿using AutoMapper;
using Service.Search.Contracts.Models;
using Service.Search.WebApi.Models;

namespace Service.Search.WebApi.Mappings
{
    public class ApiMappingProfile : Profile
    {

        public ApiMappingProfile()
        {
            CreateMap<ImageSearchModel, ImageSearchResultModel>()
                .ForMember(dest => dest.ImageFileUrl, opt => opt.Ignore())
                .ForMember(dest => dest.SubjectName, opt => opt.Ignore())
                .ForMember(dest => dest.SubjectDescription, opt => opt.Ignore())
                .ForMember(dest => dest.SubjectUrl, opt => opt.Ignore())
                .ForMember(dest => dest.SubjectImageUrl, opt => opt.Ignore())
                .ForMember(dest => dest.WebsiteUrl, opt => opt.Ignore());
        }
    }
}
