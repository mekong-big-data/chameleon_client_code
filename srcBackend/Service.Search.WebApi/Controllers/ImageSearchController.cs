using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Common.Core.Storage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Module.DaeMatch.Services;
using Module.ImageCropMatch.Services;
using Module.ImageMatch.Services;
using Module.TinEye.Services;
using Service.Manage.Contracts;
using Service.Manage.Contracts.Models;
using Service.Search.Contracts.Models;
using Service.Search.Core.Constants;
using Service.Search.Core.Repositories;
using Service.Search.Core.Repositories.Interfaces;
using Service.Search.WebApi.Models;
using Service.Tenant.Contracts;
using Service.Tenant.Contracts.Models;
using Service.WebApi.Services;

namespace Service.Search.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageSearchController : ControllerBase
    {
        private const int SearchScoreCutoff = 10;

        private readonly IBlobStorage _blobStorage;
        private readonly ITenantServiceApi _tenantServiceApi;
        private readonly IManageServiceApi _manageServiceApi;
        private readonly IAuthContextService _userContextService;
        private readonly ITinEyeService _tinEyeService;
        private readonly IImageMatchService _imageMatchService;
        private readonly IDaeMatchService _daeMatchService;
        private readonly IImageCropMatchService _imageCropMatchService;
        private readonly IImageSearchRepository _imageSearchRepository;
        private readonly IMapper _mapper;

        public ImageSearchController(
            IBlobStorage blobStorage,
            ITenantServiceApi tenantServiceApi,
            IManageServiceApi manageServiceApi,
            IAuthContextService userContextService,
            ITinEyeService tinEyeService,
            IImageMatchService imageMatchService,
            IDaeMatchService daeMatchService,
            IImageCropMatchService imageCropMatchService,
            IImageSearchRepository imageSearchRepository,
            IMapper mapper)
        {
            _tenantServiceApi = tenantServiceApi;
            _manageServiceApi = manageServiceApi;
            _userContextService = userContextService;
            _blobStorage = blobStorage;
            _tinEyeService = tinEyeService;
            _imageMatchService = imageMatchService;
            _daeMatchService = daeMatchService;
            _imageCropMatchService = imageCropMatchService;
            _imageSearchRepository = imageSearchRepository;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet]
        [Route("{imageSearchId}")]
        [ProducesResponseType(200, Type = typeof(ImageSearchModel))]
        public async Task<IActionResult> Get(int imageSearchId)
        {
            var model = await _imageSearchRepository.GetImageSearch(imageSearchId);

            return Ok(model);
        }

        [Obsolete]
        [Authorize]
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(ImageSearchResultModel))]
        public async Task<IActionResult> PostLegacy(ImageSearchModel model)
        {
            if (string.IsNullOrEmpty(model.ImageFileName))
            {
                return BadRequest("Image name missing");
            }

            var imageFileUrl = _blobStorage.GenerateBlobUrl(BlobContainers.SearchContainer, model.ImageFileName);

            var tenantId = _userContextService.AuthContext.Tenant.TenantId;
            var tenant = await _tenantServiceApi.GetTenant(tenantId, CancellationToken.None);

            ImageSearchResultModel result;
            if (tenant.Name == "acme1")
            {
                result = await PerformImageMatch(imageFileUrl, tenant, model);
            }
            else if (tenant.Name == "acme2")
            {
                result = await PerformDaeMatch(imageFileUrl, tenant, model);
            }
            else if (tenant.Name == "acme5")
            {
                result = await PerformImageCropMatch(imageFileUrl, tenant, model);
            }
            else
            {
                result = await PerformTinEyeMatch(imageFileUrl, tenant, model);
            }

            return Ok(result);
        }

        [Authorize]
        [HttpPost("go")]
        [ProducesResponseType(200, Type = typeof(ImageSearchResultModel))]
        public async Task<IActionResult> Post(ImageSearchModel model)
        {
            if (string.IsNullOrEmpty(model.ImageFileName))
            {
                return BadRequest("Image name missing");
            }

            var imageFileUrl = _blobStorage.GenerateBlobUrl(BlobContainers.SearchContainer, model.ImageFileName);

            var tenantId = _userContextService.AuthContext.Tenant.TenantId;
            var tenant = await _tenantServiceApi.GetTenant(tenantId, CancellationToken.None);

            ImageSearchResultModel result;
            if (tenant.Name == "acme5")
            {
                result = await PerformImageCropMatch(imageFileUrl, tenant, model);
            }
            else
            {
                result = await PerformImageCropTinEyeMatch(imageFileUrl, tenant, model);
            }

            return Ok(result);
        }


        private async Task<ImageSearchResultModel> PerformTinEyeMatch(string imageFileUrl, TenantModel tenant, ImageSearchModel model)
        {
            var searchResult = await _tinEyeService.SearchImage(
                imageFileUrl,
                true,
                tenant.TinEyeAccountUserName,
                tenant.TinEyeAccountPassword,
                CancellationToken.None);

            var bestResult = searchResult.result
                .OrderByDescending(r => r.score)
                .FirstOrDefault(r => r.score > SearchScoreCutoff);

            model.TenantUserId = _userContextService.AuthContext.TenantUser.TenantUserId;

            ImageModel image = null;
            if (bestResult != null)
            {
                model.MatchPercent = bestResult.match_percent;
                model.Score = bestResult.score;
                model.QueryOverlapPercent = bestResult.query_overlap_percent;
                model.TargetOverlapPercent = bestResult.target_overlap_percent;
                model.MatchFileName = bestResult.filepath;
                model.MatchFileUrl = _blobStorage.GenerateBlobUrl(bestResult.filepath);
                model.Rotation = ImageRotationExtractor.ExtractRotation(bestResult.overlay);

                var encodedFilePath = HttpUtility.UrlEncode(bestResult.filepath);
                image = await _manageServiceApi.GetByImageFilePath(encodedFilePath, CancellationToken.None);
                if (image != null && image.ImageId != 0)
                {
                    model.MatchingImageId = image.ImageId;
                }
            }

            model = await _imageSearchRepository.CreateImageSearch(model);

            var result = new ImageSearchResultModel();
            _mapper.Map(model, result);
            result.ImageFileUrl = imageFileUrl;
            if (image != null)
            {
                result.SubjectName = image.SubjectName;
                result.SubjectDescription = image.SubjectDescription;
                result.SubjectUrl = image.SubjectUrl;
                result.SubjectImageUrl = image.SubjectImageUrl;
            }
            result.WebsiteUrl = tenant.WebsiteUrl;

            return result;
        }

        private async Task<ImageSearchResultModel> PerformImageMatch(string imageFileUrl, TenantModel tenant, ImageSearchModel model)
        {
            model.TenantUserId = _userContextService.AuthContext.TenantUser.TenantUserId;

            var searchResult = await _imageMatchService.SearchImage(
                imageFileUrl,
                tenant.Name,
                model.ImageKey);

            ImageModel image = null;
            if (searchResult.Status == "success" 
                && searchResult.Errors.Length == 0 
                && searchResult.Result.Length > 0
                && searchResult.Result[0].Distance < 0.63m //&& searchResult.Result[0].IsMatch == "True"
                )
            {
                var successMatch = searchResult.Result[0];

                model.MatchPercent = 100;
                model.Score = 100;
                //model.QueryOverlapPercent = 100;
                //model.TargetOverlapPercent = 100;
                //model.MatchFileName = "manage/fc456318d6ec4f689a62cdbae54e8940";
                //model.MatchFileUrl = _blobStorage.GenerateBlobUrl("manage/fc456318d6ec4f689a62cdbae54e8940");
                model.MatchFileUrl = $"https://storageaccountchamea7f8.blob.core.windows.net/{successMatch.ImageGenerated.Path}";
                //model.Rotation = 0;
                //model.MatchingImageId = 235;

                //var encodedFilePath = HttpUtility.UrlEncode("manage/fc456318d6ec4f689a62cdbae54e8940");
                //image = await _manageServiceApi.GetByImageFilePath(encodedFilePath, CancellationToken.None);

                //model.MatchPercent = bestResult.match_percent;
                //model.Score = bestResult.score;
                //model.QueryOverlapPercent = bestResult.query_overlap_percent;
                //model.TargetOverlapPercent = bestResult.target_overlap_percent;
                //model.MatchFileName = bestResult.filepath;
                //model.MatchFileUrl = _blobStorage.GenerateBlobUrl(bestResult.filepath);
                //model.Rotation = ImageRotationExtractor.ExtractRotation(bestResult.overlay);

                //var encodedFilePath = HttpUtility.UrlEncode(bestResult.filepath);
                //image = await _manageServiceApi.GetByImageFilePath(encodedFilePath, CancellationToken.None);
                //if (image != null && image.ImageId != 0)
                //{
                //    model.MatchingImageId = image.ImageId;
                //}
            }

            model = await _imageSearchRepository.CreateImageSearch(model);

            var result = new ImageSearchResultModel();
            _mapper.Map(model, result);
            result.ImageFileUrl = imageFileUrl;
            if (image != null)
            {
                result.SubjectName = image.SubjectName;
                result.SubjectDescription = image.SubjectDescription;
                result.SubjectUrl = image.SubjectUrl;
                result.SubjectImageUrl = image.SubjectImageUrl;
            }
            result.WebsiteUrl = tenant.WebsiteUrl;

            return result;
        }

        private async Task<ImageSearchResultModel> PerformDaeMatch(string imageFileUrl, TenantModel tenant, ImageSearchModel model)
        {
            model.TenantUserId = _userContextService.AuthContext.TenantUser.TenantUserId;

            var searchResult = await _daeMatchService.SearchImage(
                imageFileUrl,
                tenant.Name,
                model.ImageKey);

            ImageModel image = null;
            if (searchResult.Status == "success" && searchResult.Errors.Length == 0)
            {
                model.MatchPercent = 100;
                model.Score = 100;
                model.QueryOverlapPercent = 100;
                model.TargetOverlapPercent = 100;
                model.MatchFileName = "manage/fc456318d6ec4f689a62cdbae54e8940";
                model.MatchFileUrl = _blobStorage.GenerateBlobUrl("manage/fc456318d6ec4f689a62cdbae54e8940");
                model.Rotation = 0;
                model.MatchingImageId = 235;

                var encodedFilePath = HttpUtility.UrlEncode("manage/fc456318d6ec4f689a62cdbae54e8940");
                image = await _manageServiceApi.GetByImageFilePath(encodedFilePath, CancellationToken.None);

                //model.MatchPercent = bestResult.match_percent;
                //model.Score = bestResult.score;
                //model.QueryOverlapPercent = bestResult.query_overlap_percent;
                //model.TargetOverlapPercent = bestResult.target_overlap_percent;
                //model.MatchFileName = bestResult.filepath;
                //model.MatchFileUrl = _blobStorage.GenerateBlobUrl(bestResult.filepath);
                //model.Rotation = ImageRotationExtractor.ExtractRotation(bestResult.overlay);

                //var encodedFilePath = HttpUtility.UrlEncode(bestResult.filepath);
                //image = await _manageServiceApi.GetByImageFilePath(encodedFilePath, CancellationToken.None);
                //if (image != null && image.ImageId != 0)
                //{
                //    model.MatchingImageId = image.ImageId;
                //}
            }

            model = await _imageSearchRepository.CreateImageSearch(model);

            var result = new ImageSearchResultModel();
            _mapper.Map(model, result);
            result.ImageFileUrl = imageFileUrl;
            if (image != null)
            {
                result.SubjectName = image.SubjectName;
                result.SubjectDescription = image.SubjectDescription;
                result.SubjectUrl = image.SubjectUrl;
                result.SubjectImageUrl = image.SubjectImageUrl;
            }
            result.WebsiteUrl = tenant.WebsiteUrl;

            return result;
        }

        private async Task<ImageSearchResultModel> PerformImageCropMatch(string imageFileUrl, TenantModel tenant, ImageSearchModel model)
        {
            model.TenantUserId = _userContextService.AuthContext.TenantUser.TenantUserId;

            var imageFileUrlParts = imageFileUrl.Split('/');
            var imageName = imageFileUrlParts[imageFileUrlParts.Length - 1];

            var searchResult = await _imageCropMatchService.SearchImage(
                imageName,
                tenant.Name);
            
            var result = new ImageSearchResultModel();
            if (searchResult.Status == "success"
                && searchResult.Errors.Length == 0
                && searchResult.Result.Length > 0)
            {
                var highestMatch = searchResult.Result[0];

                if (highestMatch.IsMatch == "True"
                    && highestMatch.ImageGenerated.KeyRaw == model.ImageKey)
                {
                    model.MatchPercent = 100;
                    model.Score = 100;
                }

                model.MatchFileUrl = $"https://storageaccountchamea1af.blob.core.windows.net/image-generator-storage/{tenant.Name}/crop_full/{imageName}.png";
                result.ImageFileUrl = $"https://storageaccountchamea1af.blob.core.windows.net/image-generator-storage/{tenant.Name}/crop_sp/{imageName}.png";
            }
            else
            {
                var failureMessage = searchResult.Status == "failed" && searchResult.Errors.Length > 0
                    ? searchResult.Errors[0]
                    : "Failed to perform match. Try: increase resolution|check if QR exists|check QR clarity";

                throw new Exception(failureMessage);
            }

            model = await _imageSearchRepository.CreateImageSearch(model);
            _mapper.Map(model, result);
            result.WebsiteUrl = tenant.WebsiteUrl;

            return result;
        }

        private async Task<ImageSearchResultModel> PerformImageCropTinEyeMatch(string imageFileUrl, TenantModel tenant, ImageSearchModel model)
        {
            var imageFileUrlParts = imageFileUrl.Split('/');
            var imageName = imageFileUrlParts[imageFileUrlParts.Length - 1];

            var cropResult = await _imageCropMatchService.CropImage(
                imageName,
                tenant.Name);

            ImageModel image = null;
            if (cropResult.Status == "success"
                && cropResult.Errors.Length == 0
                && cropResult.Result.Length > 0)
            {
                var searchResult = await _tinEyeService.SearchImage(
                    $"https://storageaccountchamea1af.blob.core.windows.net/image-generator-storage/{tenant.Name}/crop_sp/{imageName}.png",
                    true,
                    tenant.TinEyeAccountUserName,
                    tenant.TinEyeAccountPassword,
                    CancellationToken.None);

                var bestResult = searchResult.result
                    .OrderByDescending(r => r.score)
                    .FirstOrDefault(r => r.score > SearchScoreCutoff);

                model.TenantUserId = _userContextService.AuthContext.TenantUser.TenantUserId;

                if (bestResult != null)
                {
                    model.MatchPercent = bestResult.match_percent;
                    model.Score = bestResult.score;
                    model.QueryOverlapPercent = bestResult.query_overlap_percent;
                    model.TargetOverlapPercent = bestResult.target_overlap_percent;
                    model.MatchFileName = bestResult.filepath;
                    model.MatchFileUrl = _blobStorage.GenerateBlobUrl(bestResult.filepath);
                    model.Rotation = ImageRotationExtractor.ExtractRotation(bestResult.overlay);

                    var encodedFilePath = HttpUtility.UrlEncode(bestResult.filepath);
                    image = await _manageServiceApi.GetByImageFilePath(encodedFilePath, CancellationToken.None);
                    if (image != null && image.ImageId != 0)
                    {
                        model.MatchingImageId = image.ImageId;
                    }
                }
            }

            model = await _imageSearchRepository.CreateImageSearch(model);

            var result = new ImageSearchResultModel();
            _mapper.Map(model, result);
            result.ImageFileUrl = imageFileUrl;
            if (image != null)
            {
                result.SubjectName = image.SubjectName;
                result.SubjectDescription = image.SubjectDescription;
                result.SubjectUrl = image.SubjectUrl;
                result.SubjectImageUrl = image.SubjectImageUrl;
            }
            result.WebsiteUrl = tenant.WebsiteUrl;

            return result;
        }

    }
}