﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Service.Search.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IndexController : ControllerBase
    {

        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Get()
        {
            var application = Startup.ApplicationName;
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            return Content($"{application} {environment} OK");
        }

    }
}