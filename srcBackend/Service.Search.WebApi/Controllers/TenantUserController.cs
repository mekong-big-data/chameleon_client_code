﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service.WebApi.Services;

namespace Service.Search.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TenantUserController : ControllerBase
    {
        private readonly IAuthContextService _userContextService;

        public TenantUserController(
            IAuthContextService userContextService)
        {
            _userContextService = userContextService;
        }

        [HttpGet]
        [Authorize]
        [Route("me")]
        [ProducesResponseType(200)]
        public IActionResult GetMe()
        {
            if (_userContextService.AuthContext == null)
            {
                return Unauthorized();
            }

            return Ok(_userContextService.AuthContext);
        }
        
    }
}
