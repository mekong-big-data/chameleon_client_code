﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Functions.Http
{
    public class ServiceApiBasicAuthHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var headers = request.Headers;
            var authHeader = headers.Authorization;

            if (authHeader == null)
            {
                return await base.SendAsync(request, cancellationToken);
            }

            var base64Credentials = Environment.GetEnvironmentVariable("SERVICE_API_AUTH_CREDENTIALS");
            headers.Authorization = new AuthenticationHeaderValue(authHeader.Scheme, base64Credentials);

            return await base.SendAsync(request, cancellationToken);
        }
    }
}
