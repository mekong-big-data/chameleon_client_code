﻿using System.Collections.Generic;
using System.Linq;
using Common.WebApi.Attributes;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.Swagger;

namespace Common.WebApi
{
    public static class WebApiInitialisation
    {

        public static void Registration(IServiceCollection services, IConfiguration configuration, bool isDevelopment)
        {
            // controllers
            services.AddControllers();

            if (isDevelopment)
            {
                // swagger
                services.AddSwaggerGen(c => {
                    c.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Title = ""
                    });
                    c.AddSecurityDefinition(
                        "Bearer",
                        new OpenApiSecurityScheme()
                        {
                            In = ParameterLocation.Header,
                            Description = "Please enter into field the word 'Bearer' following by space and token",
                            Name = "Authorization",
                            Type = SecuritySchemeType.ApiKey
                        });

                    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                },
                                Scheme = "oauth2",
                                Name = "Bearer",
                                In = ParameterLocation.Header,
                            },
                            new List<string>()
                        }
                    });

                });
            }

            // CORS
            services.AddCors();

            var serviceApiConfig = new ServiceApiConfig();
            configuration.GetSection("ServiceApi").Bind(serviceApiConfig);
            services.AddSingleton(serviceApiConfig);
        }
        
    }
}
