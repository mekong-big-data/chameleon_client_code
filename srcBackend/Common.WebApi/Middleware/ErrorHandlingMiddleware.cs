﻿using System;
using System.Net;
using System.Threading.Tasks;
using Common.Core.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Common.WebApi.Middleware
{
    public class ErrorHandlingMiddleware
    {

        private readonly ILogger _logger;

        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next, ILogger logger)
        {
            _logger = logger;
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                var message = $"{ex.Message}{(ex.InnerException != null ? " | Inner: " + ex.InnerException.Message : "")}";

                _logger.LogWarning(message);

                var result = JsonConvert.SerializeObject(new ErrorHttpResponse(false, message));
                context.Response.ContentType = "application/json";
                var httpStatusCode = ex.GetType() == typeof(ValidationException) ? HttpStatusCode.BadRequest : HttpStatusCode.InternalServerError;
                context.Response.StatusCode = (int)httpStatusCode;
                await context.Response.WriteAsync(result);
            }
        }

    }

    public class ErrorHttpResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }
        [JsonProperty(PropertyName = "errorMessage")]
        public string ErrorMessage { get; set; }

        public ErrorHttpResponse(bool success, string errorMessage)
        {
            Success = success;
            ErrorMessage = errorMessage;
        }
    }
}
