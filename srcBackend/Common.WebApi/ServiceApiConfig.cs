﻿namespace Common.WebApi
{
    public class ServiceApiConfig
    {
        public string AllowedIpRanges { get; set; }
        public string ManageServiceApiUrl { get; set; }
        public string SearchServiceApiUrl { get; set; }
        public string TenantServiceApiUrl { get; set; }
    }
}
