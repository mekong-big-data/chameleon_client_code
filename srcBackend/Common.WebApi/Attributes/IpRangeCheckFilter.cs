﻿using System;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using NetTools;

namespace Common.WebApi.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class IpRangeCheckAttribute : TypeFilterAttribute
    {
        public IpRangeCheckAttribute() : base(typeof(IpRangeCheckFilter))
        {
        }
    }

    public class IpRangeCheckFilter : ActionFilterAttribute
    {

        private readonly ILogger _logger;
        private readonly ServiceApiConfig _config;

        public IpRangeCheckFilter(
            ILogger logger,
            ServiceApiConfig config)
        {
            _logger = logger;
            _config = config;
        }


        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (_config == null)
            {
                throw new ArgumentException("IsInSubnetCheckConfig is not configured");
            }

            var remoteIp = context.HttpContext.Connection.RemoteIpAddress;

            _logger.LogDebug($"Remote IpAddress: {remoteIp}");

            var badRemoteAddress = true;

            foreach (var range in _config.AllowedIpRanges.Split(','))
            {
                var ipRange = IPAddressRange.Parse(range);
                if (!ipRange.Contains(remoteIp))
                {
                    continue;
                }
                badRemoteAddress = false;
                break;
            }

            if (badRemoteAddress)
            {
                _logger.LogWarning($"Forbidden Request from IP: {remoteIp}");
                context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
                return;
            }

            base.OnActionExecuting(context);
        }

    }
}
