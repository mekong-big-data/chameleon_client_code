﻿using System;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Server.HttpSys;

namespace Common.WebApi.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ServiceApiBasicAuthAttribute : TypeFilterAttribute
    {
        public ServiceApiBasicAuthAttribute() : base(typeof(ServiceApiBasicAuthFilter))
        {
        }
    }

    public class ServiceApiBasicAuthFilter : IAuthorizationFilter
    {

        private readonly string _basicAuthCredentials;

        public ServiceApiBasicAuthFilter()
        {
            _basicAuthCredentials = Environment.GetEnvironmentVariable("SERVICE_API_AUTH_CREDENTIALS");
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                string authHeader = context.HttpContext.Request.Headers["Authorization"];
                if (authHeader != null)
                {
                    var authHeaderValue = AuthenticationHeaderValue.Parse(authHeader);
                    if (authHeaderValue.Scheme.Equals(AuthenticationSchemes.Basic.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        if (authHeaderValue.Parameter == _basicAuthCredentials)
                        {
                            return;
                        }
                    }
                }

                context.Result = new UnauthorizedResult();
            }
            catch (FormatException)
            {
                context.Result = new UnauthorizedResult();
            }
        }

    }
}
