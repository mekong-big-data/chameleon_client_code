using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Core.Storage;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Module.Azure.BlobStorage;
using Module.Azure.QueueStorage;
using Module.TinEye.Services;
using Service.Manage.Contracts.Models;
using Service.Manage.Contracts.Types;
using Service.Manage.Core.Constants;
using Service.Manage.Core.Repositories;
using Service.Manage.Core.Repositories.Interfaces;
using Service.Tenant.Contracts;

namespace Service.Manage.Functions
{
    public class CollectionRemovalFunction
    {

        private readonly IBlobStorage _blobStorage;
        private readonly ITenantServiceApi _tenantServiceApi;
        private readonly ITinEyeService _tinEyeService;
        private readonly ICollectionsRepository _collectionsRepository;
        private readonly ICollectionUploadsRepository _collectionUploadsRepository;
        private readonly IImagesRepository _imagesRepository;


        public CollectionRemovalFunction(
            IBlobStorage blobStorage,
            ITenantServiceApi tenantServiceApi,
            ITinEyeService tinEyeService,
            ICollectionsRepository collectionsRepository,
            ICollectionUploadsRepository collectionUploadsRepository,
            IImagesRepository imagesRepository)
        {
            _blobStorage = blobStorage;
            _tenantServiceApi = tenantServiceApi;
            _tinEyeService = tinEyeService;
            _collectionsRepository = collectionsRepository;
            _collectionUploadsRepository = collectionUploadsRepository;
            _imagesRepository = imagesRepository;
        }


        [FunctionName("CollectionRemovalFunction")]
        public async Task Run(
            [QueueTrigger(QueueNames.CollectionRemoval)]string queueMessage, 
            ILogger logger)
        {
            var discardedUpload = QueueMessageUtility.DeserialiseMessage<CollectionUploadModel>(queueMessage);

            logger.LogInformation($"New discard collection upload received {discardedUpload.UploadFileName}. Processing...");

            discardedUpload.Status = CollectionUploadStatus.Removing;
            await _collectionUploadsRepository.UpdateCollectionUpload(discardedUpload);

            var collection = await _collectionsRepository.GetCollectionByCollectionUploadId(discardedUpload.CollectionUploadId);

            if (collection == null)
            {
                logger.LogWarning($"No collection found.");
                return;
            }

            var tenantUser = await _tenantServiceApi.GetTenantUser(discardedUpload.TenantUserId, CancellationToken.None);
            var tenant = await _tenantServiceApi.GetTenant(tenantUser.TenantId, CancellationToken.None);

            var images = await _imagesRepository.GetImages(collection.CollectionId);

            var imagesDeleted = 0;
            foreach (var image in images)
            {
                var deleteResult = await _tinEyeService.DeleteImage(
                    image.ImageFilePath,
                    tenant.TinEyeAccountUserName,
                    tenant.TinEyeAccountPassword, 
                    CancellationToken.None);

                if (deleteResult.error.Any())
                {
                    logger.LogWarning($"Failed to delete image on TinEye: {string.Join(',', deleteResult.error)}.");
                }
                
                await _imagesRepository.DeleteImage(image.ImageId);

                imagesDeleted++;
            }

            await _collectionsRepository.DeleteCollection(collection.CollectionId);

            discardedUpload.Status = CollectionUploadStatus.Removed;
            await _collectionUploadsRepository.UpdateCollectionUpload(discardedUpload);

            logger.LogInformation($"Processed removal: {discardedUpload.UploadFileName}. {imagesDeleted} images delete.");
        }
    }
}
