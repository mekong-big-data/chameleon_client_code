using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Abstractions;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Common.Core.Http;
using Common.Core.Storage;
using Common.Core.Utilities;
using CsvHelper;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Module.Azure.BlobStorage;
using Module.Azure.QueueStorage;
using Module.ImageCropMatch.Services;
using Module.TinEye.Services;
using Newtonsoft.Json;
using Service.Manage.Contracts.Models;
using Service.Manage.Contracts.Types;
using Service.Manage.Core.Constants;
using Service.Manage.Core.Repositories;
using Service.Manage.Core.Repositories.Interfaces;
using Service.Tenant.Contracts;

namespace Service.Manage.Functions
{
    public class CollectionUploadFunction
    {

        private readonly IBlobStorage _blobStorage;
        private readonly ITenantServiceApi _tenantServiceApi;
        private readonly IFileSystem _fileSystem;
        private readonly ITinEyeService _tinEyeService;
        private readonly IImageCropMatchService _imageCropMatchService;
        private readonly IDownloader _downloader;
        private readonly ICollectionsRepository _collectionsRepository;
        private readonly ICollectionUploadsRepository _collectionUploadsRepository;
        private readonly IImagesRepository _imagesRepository;


        public CollectionUploadFunction(
            IBlobStorage blobStorage,
            ITenantServiceApi tenantServiceApi,
            IFileSystem fileSystem,
            ITinEyeService tinEyeService,
            IImageCropMatchService imageCropMatchService,
            IDownloader downloader,
            ICollectionsRepository collectionsRepository,
            ICollectionUploadsRepository collectionUploadsRepository,
            IImagesRepository imagesRepository)
        {
            _blobStorage = blobStorage;
            _tenantServiceApi = tenantServiceApi;
            _fileSystem = fileSystem;
            _tinEyeService = tinEyeService;
            _imageCropMatchService = imageCropMatchService;
            _downloader = downloader;
            _collectionsRepository = collectionsRepository;
            _collectionUploadsRepository = collectionUploadsRepository;
            _imagesRepository = imagesRepository;
        }


        [FunctionName("CollectionUploadFunction")]
        public async Task Run(
            [QueueTrigger(QueueNames.CollectionUpload)] string queueMessage,
            ILogger logger)
        {
            var newUpload = QueueMessageUtility.DeserialiseMessage<CollectionUploadModel>(queueMessage);

            logger.LogInformation($"New upload received {newUpload.UploadFileName}. Processing...");

            newUpload.Status = CollectionUploadStatus.Processing;
            await _collectionUploadsRepository.UpdateCollectionUpload(newUpload);

            var uploadFileName = newUpload.UploadFileName;

            logger.LogInformation($"Processing upload: {uploadFileName}...");

            // download from blob storage
            var downloadPath = _fileSystem.Path.Combine(_fileSystem.Path.GetTempPath(), uploadFileName);

            if (!_fileSystem.File.Exists(downloadPath))
            {
                try
                {
                    using (var packageStream = await _blobStorage.Retrieve(BlobContainers.UploadContainer, uploadFileName))
                    using (var fileStream = _fileSystem.File.Create(downloadPath))
                    {
                        await packageStream.CopyToAsync(fileStream);
                    }
                }
                catch (BlobStorageException bEx)
                {
                    logger.LogWarning($"Package download failed: {bEx.Message}.");
                    return;
                }

                if (!_fileSystem.File.Exists(downloadPath))
                {
                    // account for possible cancellation
                    logger.LogWarning("Package download missing.");
                    return;
                }
            }

            // extract zip
            var extractedDownloadPath = $"{_fileSystem.Path.Combine(_fileSystem.Path.GetTempPath(), uploadFileName.Replace(".zip", ""))}";
            if (!_fileSystem.Directory.Exists(extractedDownloadPath))
            {
                ZipFile.ExtractToDirectory(downloadPath, extractedDownloadPath);
            }

            var csvPath = _fileSystem.Directory.GetFiles(extractedDownloadPath, $"*.csv", SearchOption.AllDirectories).ToList();
            var csvImageEntries = new List<CsvImageEntryModel>();
            if (csvPath.Any())
            {
                using (var reader = new StreamReader(csvPath.First()))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    try
                    {
                        csvImageEntries = csv.GetRecords<CsvImageEntryModel>().ToList();
                    }
                    catch (Exception ex)
                    {
                        logger.LogInformation($"Exception: {ex.Message}");
                    }
                }
            }

            logger.LogInformation($"Found {csvImageEntries.Count} parameters....");

            if (csvImageEntries.Count == 0)
            {
                newUpload.Status = CollectionUploadStatus.Complete;
                await _collectionUploadsRepository.UpdateCollectionUpload(newUpload);

                return;
            }

            var tenantUser = await _tenantServiceApi.GetTenantUser(newUpload.TenantUserId, CancellationToken.None);
            var tenant = await _tenantServiceApi.GetTenant(tenantUser.TenantId, CancellationToken.None);

            // create collection record
            var newCollection = new CollectionModel
            {
                CollectionUploadId = newUpload.CollectionUploadId,
                TenantId = tenantUser.TenantId,
                Name = RandomUtility.GetRandomString(12)
            };
            await _collectionsRepository.CreateCollection(newCollection);

            var collectionPackageName = $"{newCollection.Name}_{newCollection.CollectionId}";
            var collectionPackagePath = $"{_fileSystem.Path.Combine(_fileSystem.Path.GetTempPath(), collectionPackageName)}";
            if (_fileSystem.Directory.Exists(collectionPackagePath))
            {
                _fileSystem.Directory.Delete(collectionPackagePath, true);
            }
            _fileSystem.Directory.CreateDirectory(collectionPackagePath);

            // process each image
            var imagesAdded = 0;
            var imageDuplicates = 0;
            foreach (var csvImageEntry in csvImageEntries)
            {
                var seed = csvImageEntry.Id + 1000000000;
                var imageKey = $"{seed}_600_600_20_200_10_50_1_6_10";

                var existingImage = await _imagesRepository.GetImageByImageHash(tenant.TenantId, imageKey);

                if (existingImage == null)
                {
                    var environment = tenant.TinEyeAccountUserName == "none" ? "prod" : "prod-te";

                    var generatedImage = await _imageCropMatchService.GenerateImage(
                        tenant.Name,
                        environment,
                        imageKey);

                    if (generatedImage.Status != "success")
                    {
                        logger.LogWarning($"Failed to generate image: {string.Join(',', generatedImage.Errors)}.");
                        
                        continue;
                    }

                    var pairImageUrl = generatedImage.Result[0].ImageGenerated.Paths.QrSpPath;
                    var splatCodeImageUrl = generatedImage.Result[0].ImageGenerated.Paths.SpPath;

                    if (environment == "dev-te" || environment == "prod-te")
                    {
                        var addResult = await _tinEyeService.AddImage(
                            splatCodeImageUrl,
                            UrlToPath(splatCodeImageUrl),
                            tenant.TinEyeAccountUserName,
                            tenant.TinEyeAccountPassword,
                            CancellationToken.None);

                        if (addResult.error.Any())
                        {
                            logger.LogWarning($"Failed to add image to TinEye: {string.Join(',', addResult.error)}.");

                            continue;
                        }
                    }

                    var pairImageLocalPath = _fileSystem.Path.Combine(collectionPackagePath, $"{imageKey}.png");
                    await _downloader.DownloadFile(new Uri(pairImageUrl), pairImageLocalPath);

                    // create image record
                    var newImage = new ImageModel
                    {
                        CollectionId = newCollection.CollectionId,
                        Name = seed.ToString(),
                        ImageFilePath = splatCodeImageUrl,
                        ImageFileHash = imageKey,
                        SubjectName = csvImageEntry.SubjectName ?? null,
                        SubjectDescription = csvImageEntry.SubjectDescription ?? null,
                        SubjectImageUrl = csvImageEntry.SubjectImageUrl ?? null,
                        SubjectUrl = csvImageEntry.SubjectUrl ?? null,
                    };
                    await _imagesRepository.CreateImage(newImage);

                    imagesAdded++;
                }
                else
                {
                    imageDuplicates++;

                    existingImage.SubjectName = csvImageEntry.SubjectName ?? null;
                    existingImage.SubjectDescription = csvImageEntry.SubjectDescription ?? null;
                    existingImage.SubjectImageUrl = csvImageEntry.SubjectImageUrl ?? null;
                    existingImage.SubjectUrl = csvImageEntry.SubjectUrl ?? null;

                    await _imagesRepository.UpdateImage(existingImage);

                    var pairImageLocalPath = _fileSystem.Path.Combine(collectionPackagePath, $"{imageKey}.png");
                    var pairImageUrl = existingImage.ImageFilePath.Replace("/generated/", "/pairs/");
                    await _downloader.DownloadFile(new Uri(pairImageUrl), pairImageLocalPath);
                }
            }

            var collectionPackageCompressedPath = $"{_fileSystem.Path.Combine(_fileSystem.Path.GetTempPath(), $"{collectionPackageName}.zip")}";
            
            if (imagesAdded > 0)
            {
                ZipFile.CreateFromDirectory(collectionPackagePath, collectionPackageCompressedPath);

                using (var stream = new FileStream(collectionPackageCompressedPath, FileMode.Open, FileAccess.Read))
                {
                    await _blobStorage.Store(
                        BlobContainers.CollectionContainer,
                        $"{collectionPackageName}.zip",
                        stream);
                }
            }

            newUpload.Status = CollectionUploadStatus.Complete;
            await _collectionUploadsRepository.UpdateCollectionUpload(newUpload);

            logger.LogInformation($"Processed upload: {uploadFileName}. {imagesAdded} images added. {imageDuplicates} duplicates skipped.");

#if !DEBUG
            // cleanup
            if (_fileSystem.File.Exists(downloadPath))
            {
                _fileSystem.File.Delete(downloadPath);
            }

            if (_fileSystem.Directory.Exists(extractedDownloadPath))
            {
                _fileSystem.Directory.Delete(extractedDownloadPath, true);
            }

            if (_fileSystem.Directory.Exists(collectionPackagePath))
            {
                _fileSystem.Directory.Delete(collectionPackagePath, true);
            }

            if (_fileSystem.File.Exists(collectionPackageCompressedPath))
            {
                _fileSystem.File.Delete(collectionPackageCompressedPath);
            }
#endif
        }


        private static string UrlToPath(string url)
        {
            url = url.Replace("https://", "").Replace("http://", "");
            var urlParts = url.Split('/');
            var pathParts = urlParts.Skip(1).ToArray();
            return string.Join("/", pathParts);
        }
    }
}
