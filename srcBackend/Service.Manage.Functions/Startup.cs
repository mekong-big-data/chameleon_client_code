﻿using System;
using System.IO.Abstractions;
using AutoMapper;
using Common.Core.Http;
using Common.Core.Storage;
using Common.Functions.Http;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Module.Azure.BlobStorage;
using Module.ImageCropMatch.Services;
using Module.TinEye.Services;
using Refit;
using Service.Manage.Core.Database;
using Service.Manage.Core.Mappings;
using Service.Manage.Core.Repositories;
using Service.Manage.Core.Repositories.Interfaces;
using Service.Tenant.Contracts;

[assembly: FunctionsStartup(typeof(Service.Manage.Functions.Startup))]
namespace Service.Manage.Functions
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            var services = builder.Services;

#if !DEBUG
            var root = $"{Environment.GetEnvironmentVariable("HOME")}/site/wwwroot";
#else
            var root = Environment.GetEnvironmentVariable("AzureWebJobsScriptRoot");
#endif

            var configuration = new ConfigurationBuilder()
                .SetBasePath(root)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", true)
                .AddEnvironmentVariables()
                .Build();

            // auto-mapper
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DataMappingProfile>();
            });
            services.AddSingleton(mapperConfig);
            services.AddSingleton(mapperConfig.CreateMapper());

            mapperConfig.AssertConfigurationIsValid();

            // entity framework
            services.AddDbContext<ManageDataContext>(options =>
            {
                options.UseSqlServer(new SqlConnection(Environment.GetEnvironmentVariable("EF_CONNECTION_STRING")),
                    x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, ManageDataContext.ContextSchema));
            });

            // file system
            services.AddSingleton<IFileSystem, FileSystem>();

            // services
            services.AddScoped<IBlobStorage, BlobStorageService>();
            services.AddScoped<ITinEyeService, TinEyeService>();
            services.AddScoped<IImageCropMatchService, ImageCropMatchService>();
            services.AddScoped<IDownloader, WebClientDownloader>();

            // serviceapi
            services.AddTransient<ServiceApiBasicAuthHandler>();
            services.AddRefitClient<ITenantServiceApi>()
                .ConfigureHttpClient(c => c.BaseAddress = new Uri(configuration.GetValue<string>("ServiceApi:TenantServiceApiUrl")))
                .AddHttpMessageHandler<ServiceApiBasicAuthHandler>();

            // repos
            services.AddScoped<IImagesRepository, ImagesRepository>();
            services.AddScoped<ICollectionsRepository, CollectionsRepository>();
            services.AddScoped<ICollectionUploadsRepository, CollectionUploadsRepository>();
        }
    }
}
