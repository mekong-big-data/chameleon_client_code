﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Common.Core.BlobStorage;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Module.Azure.BlobStorage;
using Module.TinEye.Middleware;
using Module.TinEye.Services;
using Refit;
using Service.Manage.BackgroundTasks.TimedTasks;
using Service.Manage.Core.Database;
using Service.Manage.Core.Mappings;
using Service.Manage.Core.Repositories;

namespace Service.Manage.BackgroundTasks
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            using (var host = Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    // logging
                    var loggerFactory = LoggerFactory.Create(builder =>
                    {
                        // todo: configure external logging for prod

                        builder.AddConsole();
                    });
                    services.AddSingleton(loggerFactory);
                    services.AddSingleton(loggerFactory.CreateLogger(typeof(Program)));

                    // auto-mapper
                    var mapperConfig = new MapperConfiguration(cfg =>
                    {
                        cfg.AddProfile<DataMappingProfile>();
                    });
                    services.AddSingleton(mapperConfig);
                    services.AddSingleton(mapperConfig.CreateMapper());

                    mapperConfig.AssertConfigurationIsValid();

                    // entity framework
                    services.AddDbContext<ManageDataContext>(options =>
                    {
                        options.UseSqlServer(new SqlConnection(Environment.GetEnvironmentVariable("EF_CONNECTION_STRING")),
                            x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, ManageDataContext.ContextSchema));
                    });

                    // TinEye
                    services.AddTransient<TinEyeAuthorizationMessageHandler>();
                    services.AddRefitClient<ITinEyeService>()
                        .ConfigureHttpClient(c => c.BaseAddress = new Uri($"https://matchengine.tineye.com/{Environment.GetEnvironmentVariable("TINEYE_USERNAME")}/rest"))
                        .AddHttpMessageHandler<TinEyeAuthorizationMessageHandler>();
                    services.Decorate<ITinEyeService, TinEyeService>();

                    // services
                    services.AddScoped<IBlobStorage, BlobStorageService>();

                    services.AddScoped<ImagesRepository>();
                    services.AddScoped<CollectionsRepository>();
                    services.AddScoped<CollectionUploadsRepository>();

                    // hosted
                    services.AddHostedService<CollectionUploaderTask>();
                    services.AddHostedService<CollectionRemovalTask>();
                })
                .Build())
            {
                await host.StartAsync();

                await host.WaitForShutdownAsync();
            } 
        }
    }
}
