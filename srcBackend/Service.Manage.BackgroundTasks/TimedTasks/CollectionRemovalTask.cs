﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.BackgroundTasks.TimedTasks;
using Common.Core.BlobStorage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Module.Azure.BlobStorage;
using Module.TinEye.Services;
using Service.Manage.Core.Repositories;
using Service.Manage.Core.Types;

namespace Service.Manage.BackgroundTasks.TimedTasks
{
    public class CollectionRemovalTask : TimedTaskBase<CollectionRemovalTask>
    {

        protected override string TaskName => "CollectionRemoval";
        protected override TimeSpan Interval => TimeSpan.FromSeconds(5);

        public CollectionRemovalTask(
            IServiceProvider services, 
            ILogger<CollectionRemovalTask> logger) 
            : base(services, logger)
        { }

        protected override async Task InvokeTask(IServiceScope scope, CancellationToken cancellationToken)
        {
            Logger.LogInformation("CollectionRemoval task is being invoked");
            
            var collectionUploadsRepository = scope.ServiceProvider.GetRequiredService<CollectionUploadsRepository>();
            
            var discardedUploads = collectionUploadsRepository.GetCollectionUploadsByStatus(CollectionUploadStatus.Discarded).Result;

            var discardedCount = discardedUploads.Count;
            if (discardedCount == 0)
            {
                return;
            }

            Logger.LogInformation($"Found {discardedCount} to remove. Processing...");

            var blobStorage = scope.ServiceProvider.GetRequiredService<IBlobStorage>();
            var collectionsRepository = scope.ServiceProvider.GetRequiredService<CollectionsRepository>();
            var imagesRepository = scope.ServiceProvider.GetRequiredService<ImagesRepository>();
            var tinEyeService = scope.ServiceProvider.GetRequiredService<ITinEyeService>();

            foreach (var discardedUpload in discardedUploads)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    Logger.LogInformation("Cancellation requested. Exiting...");
                    break;
                }

                discardedUpload.Status = CollectionUploadStatus.Removing;
                await collectionUploadsRepository.UpdateCollectionUpload(discardedUpload);

                var collection = await collectionsRepository.GetCollectionByCollectionUploadId(discardedUpload.CollectionUploadId);

                var images = await imagesRepository.GetImages(collection.CollectionId);

                var imagesDeleted = 0;
                foreach (var image in images)
                {
                    var deleteResult = await tinEyeService.DeleteImage(image.ImageFilePath, cancellationToken);

                    if (deleteResult.error.Any())
                    {
                        Logger.LogWarning($"Failed to delete image on TinEye: {string.Join(',', deleteResult.error)}.");
                        continue;
                    }

                    var blobName = blobStorage.RetrieveBlobName(BlobContainers.ManageContainer, image.ImageFilePath);

                    try
                    {
                        await blobStorage.Delete(BlobContainers.ManageContainer, blobName);
                    }
                    catch (BlobStorageException bEx)
                    {
                        Logger.LogWarning($"Failed to delete blob: {bEx.Message}.");
                        continue;
                    }

                    await imagesRepository.DeleteImage(image.ImageId);
                    
                    imagesDeleted++;
                }

                discardedUpload.Status = CollectionUploadStatus.Removed;
                await collectionUploadsRepository.UpdateCollectionUpload(discardedUpload);

                Logger.LogInformation($"Processed removal: {discardedUpload.UploadFileName}. {imagesDeleted} images delete.");
            }

        }

    }
}
