﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.BackgroundTasks.TimedTasks;
using Common.Core.BlobStorage;
using Common.Core.Utilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Module.Azure.BlobStorage;
using Module.TinEye.Services;
using Service.Manage.Core;
using Service.Manage.Core.Models;
using Service.Manage.Core.Repositories;
using Service.Manage.Core.Types;

namespace Service.Manage.BackgroundTasks.TimedTasks
{
    public class CollectionUploaderTask : TimedTaskBase<CollectionUploaderTask>
    {
        private static readonly List<string> ImageSearchPatterns = new List<string> { "jpg", "jpeg", "png" };

        protected override string TaskName => "CollectionUploader";
        protected override TimeSpan Interval => TimeSpan.FromSeconds(5);

        public CollectionUploaderTask(
            IServiceProvider services, 
            ILogger<CollectionUploaderTask> logger) 
            : base(services, logger)
        { }

        protected override async Task InvokeTask(IServiceScope scope, CancellationToken cancellationToken)
        {
            Logger.LogInformation("CollectionUploader task is being invoked");
            
            var collectionUploadsRepository = scope.ServiceProvider.GetRequiredService<CollectionUploadsRepository>();
            
            var newUploads = collectionUploadsRepository.GetCollectionUploadsByStatus(CollectionUploadStatus.Uploaded).Result;

            var newCount = newUploads.Count;
            if (newCount == 0)
            {
                return;
            }

            Logger.LogInformation($"Found {newCount} new uploads. Processing...");

            var blobStorage = scope.ServiceProvider.GetRequiredService<IBlobStorage>();
            var collectionsRepository = scope.ServiceProvider.GetRequiredService<CollectionsRepository>();
            var imagesRepository = scope.ServiceProvider.GetRequiredService<ImagesRepository>();
            var tinEyeService = scope.ServiceProvider.GetRequiredService<ITinEyeService>();

            foreach (var newUpload in newUploads)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    Logger.LogInformation("Cancellation requested. Exiting...");
                    break;
                }

                newUpload.Status = CollectionUploadStatus.Processing;
                await collectionUploadsRepository.UpdateCollectionUpload(newUpload);

                var uploadFileName = newUpload.UploadFileName;

                Logger.LogInformation($"Processing upload: {uploadFileName}...");

                // download from blob storage
                var downloadPath = Path.Combine(Path.GetTempPath(), uploadFileName);

                if (!File.Exists(downloadPath))
                {
                    try
                    {
                        using (var packageStream = await blobStorage.Retrieve(BlobContainers.UploadContainer, uploadFileName))
                        using (var fileStream = File.Create(downloadPath))
                        {
                            await packageStream.CopyToAsync(fileStream, cancellationToken);
                        }
                    }
                    catch (BlobStorageException bEx)
                    {
                        Logger.LogWarning($"Package download failed: {bEx.Message}.");
                        continue;
                    }

                    if (!File.Exists(downloadPath))
                    {
                        // account for possible cancellation
                        Logger.LogWarning("Package download missing.");
                        continue;
                    }
                }

                // extract zip
                var extractedDownloadPath = $"{Path.Combine(Path.GetTempPath(), uploadFileName.Replace(".zip", ""))}";
                if (!Directory.Exists(extractedDownloadPath))
                {
                    ZipFile.ExtractToDirectory(downloadPath, extractedDownloadPath);
                }

                // find images in the package...
                var imagePaths = new List<string>();
                foreach (var pattern in ImageSearchPatterns)
                {
                    imagePaths.AddRange(Directory
                        .GetFiles(extractedDownloadPath, $"*.{pattern}", SearchOption.AllDirectories)
                        .ToList());
                }

                Logger.LogInformation($"Found {imagePaths.Count} images. Iterating...");

                if (imagePaths.Count == 0)
                {
                    continue;
                }

                // create collection record
                var newCollection = new CollectionModel
                {
                    CollectionUploadId = newUpload.CollectionUploadId,
                    Name = RandomUtility.GetRandomString(12)
                };
                await collectionsRepository.CreateCollection(newCollection);

                // process each image
                var imagesAdded = 0;
                var imageDuplicates = 0;
                foreach (var imagePath in imagePaths)
                {
                    // store image as blob
                    var imageKey = Guid.NewGuid().ToString("N");
                    string imageHash;

                    try
                    {
                        using (var imageStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
                        {
                            // check if image exists in collection already
                            var imageBytes = StreamUtility.ReadFully(imageStream);
                            imageHash = CryptoUtility.GetFileSHA256Hash(imageBytes);

                            if (await imagesRepository.DoesImageFileHashExist(imageHash))
                            {
                                imageDuplicates++;
                                Logger.LogWarning($"Image hash already exists in collection.");
                                continue;
                            }

                            imageStream.Position = 0;
                            await blobStorage.Store(
                                BlobContainers.ManageContainer,
                                imageKey, 
                                imageStream);
                        }
                        
                    }
                    catch (BlobStorageException bEx)
                    {
                        Logger.LogWarning($"Failed to store blob: {bEx.Message}.");
                        continue;
                    }
                    
                    // add to TinEye
                    var imageBlobUrl = blobStorage.GenerateBlobUrl(BlobContainers.ManageContainer, imageKey);
                    var imageBlobPath = blobStorage.GenerateBlobPath(BlobContainers.ManageContainer, imageKey);

                    var addResult = await tinEyeService.AddImage(imageBlobUrl, imageBlobPath, cancellationToken);

                    if (addResult.error.Any())
                    {
                        Logger.LogWarning($"Failed to add image to TinEye: {string.Join(',', addResult.error)}.");

                        try
                        {
                            await blobStorage.Delete(BlobContainers.ManageContainer, imageKey);
                        }
                        catch (BlobStorageException bEx)
                        {
                            Logger.LogWarning($"Failed to delete blob: {bEx.Message}.");
                        }

                        continue;
                    }

                    // create image record
                    var imagePathParts = imagePath.Split(Path.DirectorySeparatorChar);
                    var newImage = new ImageModel
                    {
                        CollectionId = newCollection.CollectionId,
                        Name = imagePathParts[imagePathParts.Length - 1],
                        ImageFilePath = imageBlobPath,
                        ImageFileHash = imageHash
                    };
                    await imagesRepository.CreateImage(newImage);

                    imagesAdded++;
                }

                newUpload.Status = CollectionUploadStatus.Complete;
                await collectionUploadsRepository.UpdateCollectionUpload(newUpload);

                Logger.LogInformation($"Processed upload: {uploadFileName}. {imagesAdded} images added. {imageDuplicates} duplicates skipped.");


                if (!IsDevelopmentEnvironment)
                {
                    // cleanup
                    if (File.Exists(downloadPath))
                    {
                        File.Delete(downloadPath);
                    }

                    if (Directory.Exists(extractedDownloadPath))
                    {
                        Directory.Delete(extractedDownloadPath);
                    }
                }

            }

        }

    }
}
