﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Module.Azure.ActiveDirectory
{
    public static class StartupAuthentication
    {

        public static void Register(
            IServiceCollection services, 
            IConfiguration configuration,
            Action<AuthorizationOptions> authConfiguration)
        {
            IdentityModelEventSource.ShowPII = true;

            var azureAdConfig = new AzureAdConfig();
            configuration.GetSection("AzureAd").Bind(azureAdConfig);
            services.AddSingleton(azureAdConfig);

            services
                .AddAuthentication(options =>
                {
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.Authority = $"https://{azureAdConfig.Domain}.b2clogin.com/" +
                                        $"{azureAdConfig.Domain}.onmicrosoft.com/" +
                                        $"{azureAdConfig.SignupSigninUserFlow}/" +
                                        $"v2.0/";
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidAudiences = new List<string> { azureAdConfig.ClientId.ToString(), azureAdConfig.AppIdUri.ToString() },
                        ValidIssuer = $"https://login.microsoftonline.com/{azureAdConfig.TenantId}/v2.0"
                    };
#if DEBUG
                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            // todo: log warning
                            return Task.FromResult(context.Exception.Message);
                        }
                    };
#endif
                });

            services.AddAuthorization(authConfiguration);
        }

    }
}
