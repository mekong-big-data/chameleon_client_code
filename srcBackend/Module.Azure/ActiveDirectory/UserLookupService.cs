﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Common.Core.Authentication;
using Common.Core.Authentication.Responses;
using Microsoft.Graph;
using Microsoft.Identity.Client;

namespace Module.Azure.ActiveDirectory
{
    public class UserLookupService : IUserLookupService
    {

        private readonly AzureAdConfig _azureAdConfig;
        private readonly string _clientSecret;

        public UserLookupService(
            AzureAdConfig azureAdConfig)
        {
            _azureAdConfig = azureAdConfig;
            _clientSecret = Environment.GetEnvironmentVariable("AZURE_ADB2C_CLIENT_SECRET");
        }


        public async Task<UserLookupResponse> LookupUser(Guid id)
        {
            var cca = ConfidentialClientApplicationBuilder
                .Create(_azureAdConfig.ClientId.ToString())
                .WithClientSecret(_clientSecret)
                .WithTenantId(_azureAdConfig.TenantId.ToString())
                .Build();

            var graphServiceClient = new GraphServiceClient(
                new DelegateAuthenticationProvider(async (requestMessage) => {
                    var authResult = await cca.AcquireTokenForClient(new List<string> { "https://graph.microsoft.com/.default" }).ExecuteAsync();
                    requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", authResult.AccessToken);
                })
            );

            var result = await graphServiceClient
                .Users[id.ToString()]
                .Request()
                .Select(e => new { e.Id, e.DisplayName, e.Identities })
                .GetAsync();

            if (result == null)
            {
                return null;
            }

            return new UserLookupResponse
            {
                Id = new Guid(result.Id),
                EmailAddress = result.Identities.First().IssuerAssignedId
            };
        }

    }
}
