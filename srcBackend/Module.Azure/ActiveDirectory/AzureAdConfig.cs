﻿using System;

namespace Module.Azure.ActiveDirectory
{
    public class AzureAdConfig
    {
        public string Domain { get; set; }
        public string SignupSigninUserFlow { get; set; }
        public Guid ClientId { get; set; }
        public Guid TenantId { get; set; }
        public string AppIdUri { get; set; }
    }
}
