﻿using System;
using System.IO;
using System.Threading.Tasks;
using Azure.Storage;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Sas;
using Common.Core.Storage;
using Microsoft.Extensions.Logging;

namespace Module.Azure.BlobStorage
{
    public class BlobStorageService : IBlobStorage
    {
        private readonly string _accountKey;
        private readonly string _accountName;
        private readonly string _blobStorageUri;


        public BlobStorageService()
        {
            _accountKey = Environment.GetEnvironmentVariable("AZURE_ACCOUNT_KEY");
            _accountName = Environment.GetEnvironmentVariable("AZURE_STORAGE_ACCOUNT_NAME");
            _blobStorageUri = $"https://{_accountName}.blob.core.windows.net";
        }

        public async Task<bool> Store(string containerName, string blobName, Stream inputStream)
        {
            var sharedKeyCredentials = new StorageSharedKeyCredential(_accountName, _accountKey);

            try
            {
                var containerClient = new BlobContainerClient(
                    new Uri($"{_blobStorageUri}/{containerName}"), 
                    sharedKeyCredentials);

                await containerClient.CreateIfNotExistsAsync(PublicAccessType.Blob);

                var blobClient = containerClient.GetBlobClient(blobName);

                await blobClient.UploadAsync(inputStream, true);
                blobClient.SetHttpHeaders(new BlobHttpHeaders
                {
                    ContentDisposition = $"inline; filename={blobName}"
                });

                return true;
            }
            catch (Exception ex)
            {
                throw new BlobStorageException($"Blob storage exception: {ex.Message}");
            }
        }
        
        public async Task<Stream> Retrieve(string containerName, string blobName)
        {
            var sharedKeyCredentials = new StorageSharedKeyCredential(_accountName, _accountKey);

            try
            {
                var containerClient = new BlobContainerClient(new Uri($"{_blobStorageUri}/{containerName}"), sharedKeyCredentials);

                if (!(await containerClient.ExistsAsync()))
                {
                    throw new BlobStorageException("Container does not exist");
                }

                var blobClient = containerClient.GetBlobClient(blobName);

                if (!(await blobClient.ExistsAsync()))
                {
                    throw new BlobStorageException("Blob does not exist");
                }

                var blobDownload = await blobClient.DownloadAsync();

                return blobDownload.Value.Content;
            }
            catch (Exception ex)
            {
                throw new BlobStorageException($"Blob retrieval exception: {ex.Message}");
            }
        }

        public async Task<bool> Delete(string containerName, string blobName)
        {
            var sharedKeyCredentials = new StorageSharedKeyCredential(_accountName, _accountKey);

            try
            {
                var containerClient = new BlobContainerClient(
                    new Uri($"{_blobStorageUri}/{containerName}"),
                    sharedKeyCredentials);

                if (!(await containerClient.ExistsAsync()))
                {
                    throw new BlobStorageException("Container does not exist");
                }

                await containerClient.GetBlobClient(blobName).DeleteIfExistsAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new BlobStorageException($"Blob storage exception: {ex.Message}");
            }
        }

        public BlobSasToken GenerateSasToken(double minuteExpiry = 5)
        {
            var sharedKeyCredentials = new StorageSharedKeyCredential(_accountName, _accountKey);
            var sasBuilder = new AccountSasBuilder()
            {
                StartsOn = DateTimeOffset.UtcNow,
                ExpiresOn = DateTimeOffset.UtcNow.AddMinutes(minuteExpiry),
                Services = AccountSasServices.Blobs,
                ResourceTypes = AccountSasResourceTypes.All,
                Protocol = SasProtocol.Https
            };
            sasBuilder.SetPermissions(AccountSasPermissions.All);

            var sasToken = sasBuilder.ToSasQueryParameters(sharedKeyCredentials).ToString();

            return new BlobSasToken
            {
                StorageUri = _blobStorageUri,
                StorageAccessToken = sasToken
            };
        }

        public string GenerateBlobPath(string containerName, string blobName)
        {
            return $"{containerName}/{blobName}";
        }

        public string GenerateBlobUrl(string containerName, string blobName)
        {
            return $"{_blobStorageUri}/{containerName}/{blobName}";
        }

        public string GenerateBlobUrl(string blobPath)
        {
            return $"{_blobStorageUri}/{blobPath}";
        }

        public string RetrieveBlobName(string containerName, string blobPath)
        {
            return blobPath.Replace($"{containerName}/", "");
        }
    }
}
