﻿using System;

namespace Module.Azure.BlobStorage
{
    public class BlobStorageException : Exception
    {

        public BlobStorageException(string exceptionMessage) 
            : base(exceptionMessage)
        { }

    }
}
