﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Azure.Storage;
using Azure.Storage.Queues;
using Common.Core.Storage;

namespace Module.Azure.QueueStorage
{
    public class QueueStorageService : IQueueStorage
    {
        private readonly string _accountKey;
        private readonly string _accountName;
        private readonly string _queueStorageUri;

        private readonly ILogger _logger;

        public QueueStorageService(
            ILogger logger)
        {
            _logger = logger;
            _accountKey = Environment.GetEnvironmentVariable("AZURE_ACCOUNT_KEY");
            _accountName = Environment.GetEnvironmentVariable("AZURE_STORAGE_ACCOUNT_NAME");
            _queueStorageUri = $"https://{_accountName}.queue.core.windows.net";
        }

        public async Task<bool> QueueMessage(string queueName, string message)
        {
            var sharedKeyCredentials = new StorageSharedKeyCredential(_accountName, _accountKey);

            try
            {
                var queueClient = new QueueClient(
                    new Uri($"{_queueStorageUri}/{queueName}"),
                    sharedKeyCredentials);

                if (queueClient.Exists())
                {
                    await queueClient.SendMessageAsync(message);
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw new QueueStorageException($"Queue storage exception: {ex.Message}");
            }
        }

    }
}
