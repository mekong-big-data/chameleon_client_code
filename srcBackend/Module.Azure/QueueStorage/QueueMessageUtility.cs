﻿using Newtonsoft.Json;

namespace Module.Azure.QueueStorage
{
    public static class QueueMessageUtility
    {

        public static string SerialiseMessage<T>(T model)
        {
            var serialised = JsonConvert.SerializeObject(model);
            var bytes = System.Text.Encoding.UTF8.GetBytes(serialised);
            return System.Convert.ToBase64String(bytes);
        }

        public static T DeserialiseMessage<T>(string message)
        {
            return JsonConvert.DeserializeObject<T>(message);
        }
    }
}
