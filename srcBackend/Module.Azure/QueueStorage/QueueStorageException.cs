﻿using System;

namespace Module.Azure.QueueStorage
{
    public class QueueStorageException : Exception
    {

        public QueueStorageException(string exceptionMessage) 
            : base(exceptionMessage)
        { }

    }
}
