﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Module.EntityFramework
{
    public class EntityBase
    {
        [Required]
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public DateTimeOffset? DeletedAt { get; set; }
    }
}
