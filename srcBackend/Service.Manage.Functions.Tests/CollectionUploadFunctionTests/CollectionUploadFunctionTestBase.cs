﻿using System.IO;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Reflection;
using System.Threading.Tasks;
using Common.Core.Http;
using Common.Core.Storage;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Module.ImageCropMatch.Services;
using Module.TinEye.Services;
using Moq;
using Service.Manage.Core.Constants;
using Service.Manage.Core.Repositories;
using Service.Manage.Core.Repositories.Interfaces;
using Service.Tenant.Contracts;

namespace Service.Manage.Functions.Tests.CollectionUploadFunctionTests
{
    [TestClass]
    public class CollectionUploadFunctionTestBase
    {
        private Assembly _executingAssembly;

        protected Mock<ILogger> Logger = new Mock<ILogger>();
        protected Mock<IBlobStorage> BlobStorage = new Mock<IBlobStorage>();
        protected Mock<ITenantServiceApi> TenantServiceApi = new Mock<ITenantServiceApi>();
        protected IFileSystem FileSystem = new FileSystem();
        protected Mock<ITinEyeService> TinEyeService = new Mock<ITinEyeService>();
        protected Mock<IImageCropMatchService> ImageCropMatchService = new Mock<IImageCropMatchService>();
        protected Mock<IDownloader> Downloader = new Mock<IDownloader>();
        protected Mock<ICollectionsRepository> CollectionsRepository = new Mock<ICollectionsRepository>();
        protected Mock<ICollectionUploadsRepository> CollectionUploadsRepository = new Mock<ICollectionUploadsRepository>();
        protected Mock<IImagesRepository> ImagesRepository = new Mock<IImagesRepository>();

        [TestInitialize]
        public void Setup()
        {
            _executingAssembly = Assembly.GetExecutingAssembly();

            BlobStorage
                .Setup(x => x.Retrieve(It.IsAny<string>(), It.IsAny<string>()))
                .Returns((string containerName, string blobName) => Task.FromResult(GetResourceFile(blobName)));

        }


        [TestCleanup]
        public void Cleanup()
        {

        }

        protected Stream GetResourceFile(string name)
        {
            var resourceName = $"Service.Manage.Functions.Tests.CollectionUploadFunctionTests.Resources.{name}";
            return _executingAssembly.GetManifestResourceStream(resourceName);
        }

    }
}
