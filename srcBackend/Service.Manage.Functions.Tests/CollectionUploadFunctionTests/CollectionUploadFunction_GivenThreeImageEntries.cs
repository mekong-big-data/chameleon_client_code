﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Module.Azure.QueueStorage;
using Module.ImageCropMatch.Models;
using Module.TinEye.Models;
using Moq;
using Newtonsoft.Json;
using Service.Manage.Contracts.Models;
using Service.Manage.Contracts.Types;
using Service.Manage.Core.Constants;
using Service.Tenant.Contracts.Models;

namespace Service.Manage.Functions.Tests.CollectionUploadFunctionTests
{
    [TestClass]
    public class CollectionUploadFunction_GivenThreeValidImages : CollectionUploadFunctionTestBase
    {

        [TestMethod]
        public async Task WhenFunctionIsTriggered_AllImagesAreUploadedSuccessfully()
        {
            // Arrange
            var newUpload = JsonConvert.SerializeObject(new CollectionUploadModel
            {
                CollectionUploadId = 1,
                TenantUserId = 77,
                Status = CollectionUploadStatus.Uploaded,
                UploadFileName = "ThreeImageEntries.zip"
            });

            TenantServiceApi
                .Setup(x => x.GetTenantUser(77, CancellationToken.None))
                .Returns(() => Task.FromResult(new TenantUserModel
                {
                    TenantUserId = 77,
                    TenantId = 34
                }));

            TenantServiceApi
                .Setup(x => x.GetTenant(34, CancellationToken.None))
                .Returns(() => Task.FromResult(new TenantModel
                {
                    TenantId = 34,
                    TinEyeAccountUserName = "tinuser",
                    TinEyeAccountPassword = "tinpass"
                }));

            ImageCropMatchService
                .Setup(x => x.GenerateImage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((string tenantName, string environment, string key) => Task.FromResult(new ImageCropMatchGenerationResponse
                {
                    Status = "success",
                    Errors = new string[0],
                    Result = new ImageCropMatchGenerationResult[]
                    {
                        new ImageCropMatchGenerationResult
                        {
                            ImageGenerated = new ImageCropMatchGenerationResultImageGenerated
                            {
                                Paths = new ImageCropMatchGenerationResultImageGeneratedPaths
                                {
                                    QrSpPath = $"https://pairs.com/{key}",
                                    SpPath = $"https://generated.com/{key}"
                                }
                            }
                        }
                    }
                }));

            TinEyeService
                .Setup(x => x.AddImage("https://generated.com/1000000001_300_300_30_100_40_20_1_6_10", It.IsAny<string>(), "tinuser", "tinpass", CancellationToken.None))
                .Returns(() => Task.FromResult(new ResultModel
                    { status = "OK", error = new List<object>() }));

            TinEyeService
                .Setup(x => x.AddImage("https://generated.com/1000000002_300_300_30_100_40_20_1_6_10", It.IsAny<string>(), "tinuser", "tinpass", CancellationToken.None))
                .Returns(() => Task.FromResult(new ResultModel
                    { status = "OK", error = new List<object>() }));

            TinEyeService
                .Setup(x => x.AddImage("https://generated.com/1000000003_300_300_30_100_40_20_1_6_10", It.IsAny<string>(), "tinuser", "tinpass", CancellationToken.None))
                .Returns(() => Task.FromResult(new ResultModel
                    { status = "OK", error = new List<object>() }));

            ImagesRepository
                .Setup(x => x.GetImageByImageHash(34, It.IsAny<string>()))
                .Returns(() => Task.FromResult((ImageModel)null));

            // Act
            var function = new CollectionUploadFunction(
                BlobStorage.Object,
                TenantServiceApi.Object,
                FileSystem,
                TinEyeService.Object,
                ImageCropMatchService.Object,
                Downloader.Object,
                CollectionsRepository.Object,
                CollectionUploadsRepository.Object,
                ImagesRepository.Object);
            await function.Run(newUpload, Logger.Object);

            // Assert
            CollectionsRepository.Verify(
                x => x.CreateCollection(It.IsAny<CollectionModel>()), 
                Times.Exactly(1));

            CollectionUploadsRepository.Verify(
                x => x.UpdateCollectionUpload(It.IsAny<CollectionUploadModel>()),
                Times.Exactly(2));

            TinEyeService.Verify(
                x => x.AddImage(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    "tinuser",
                    "tinpass",
                    CancellationToken.None),
                Times.Exactly(3));
            
            Downloader.Verify(
                x => x.DownloadFile(It.IsAny<Uri>(), It.IsAny<string>()),
                Times.Exactly(3));

            ImagesRepository.Verify(
                x => x.CreateImage(It.IsAny<ImageModel>()),
                Times.Exactly(3));

            BlobStorage.Verify(
                x => x.Store(BlobContainers.CollectionContainer,
                    It.IsAny<string>(),
                    It.IsAny<Stream>()),
                Times.Exactly(1));

        }

        [TestMethod]
        public async Task WhenTinEyeAddFails_NoImageRecordSaved()
        {
            // Arrange
            var newUpload = JsonConvert.SerializeObject(new CollectionUploadModel
            {
                CollectionUploadId = 1,
                TenantUserId = 77,
                Status = CollectionUploadStatus.Uploaded,
                UploadFileName = "ThreeImageEntries.zip"
            });

            TenantServiceApi
                .Setup(x => x.GetTenantUser(77, CancellationToken.None))
                .Returns(() => Task.FromResult(new TenantUserModel
                {
                    TenantUserId = 77,
                    TenantId = 34
                }));

            TenantServiceApi
                .Setup(x => x.GetTenant(34, CancellationToken.None))
                .Returns(() => Task.FromResult(new TenantModel
                {
                    TenantId = 34,
                    TinEyeAccountUserName = "tinuser",
                    TinEyeAccountPassword = "tinpass"
                }));

            ImageCropMatchService
                .Setup(x => x.GenerateImage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((string tenantName, string environment, string key) => Task.FromResult(new ImageCropMatchGenerationResponse
                {
                    Status = "success",
                    Errors = new string[0],
                    Result = new ImageCropMatchGenerationResult[]
                    {
                        new ImageCropMatchGenerationResult
                        {
                            ImageGenerated = new ImageCropMatchGenerationResultImageGenerated
                            {
                                Paths = new ImageCropMatchGenerationResultImageGeneratedPaths
                                {
                                    QrSpPath = $"https://pairs.com/{key}",
                                    SpPath = $"https://generated.com/{key}"
                                }
                            }
                        }
                    }
                }));

            TinEyeService
                .Setup(x => x.AddImage(It.IsAny<string>(), It.IsAny<string>(), "tinuser", "tinpass", CancellationToken.None))
                .Returns(() => Task.FromResult(new ResultModel
                {
                    status = "Failed",
                    error = new List<object> { "Failed to add image" }
                }));

            ImagesRepository
                .Setup(x => x.GetImageByImageHash(34, It.IsAny<string>()))
                .Returns(() => Task.FromResult((ImageModel)null));

            // Act
            var function = new CollectionUploadFunction(
                BlobStorage.Object,
                TenantServiceApi.Object,
                FileSystem,
                TinEyeService.Object,
                ImageCropMatchService.Object,
                Downloader.Object,
                CollectionsRepository.Object,
                CollectionUploadsRepository.Object,
                ImagesRepository.Object);
            await function.Run(newUpload, Logger.Object);

            // Assert
            CollectionsRepository.Verify(
                x => x.CreateCollection(It.IsAny<CollectionModel>()),
                Times.Exactly(1));

            CollectionUploadsRepository.Verify(
                x => x.UpdateCollectionUpload(It.IsAny<CollectionUploadModel>()),
                Times.Exactly(2));

            TinEyeService.Verify(
                x => x.AddImage(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    "tinuser",
                    "tinpass",
                    CancellationToken.None),
                Times.Exactly(3));

            Downloader.Verify(
                x => x.DownloadFile(It.IsAny<Uri>(), It.IsAny<string>()),
                Times.Exactly(0));

            ImagesRepository.Verify(
                x => x.CreateImage(It.IsAny<ImageModel>()),
                Times.Exactly(0));

        }

        [TestCleanup]
        public void Cleanup()
        {
            var zipPath = FileSystem.Path.Combine(FileSystem.Path.GetTempPath(), "ThreeImageEntries.zip");
            if (FileSystem.File.Exists(zipPath))
            {
                FileSystem.File.Delete(zipPath);
            }
            var unzipPath = FileSystem.Path.Combine(FileSystem.Path.GetTempPath(), "ThreeImageEntries");
            if (FileSystem.Directory.Exists(unzipPath))
            {
                FileSystem.Directory.Delete(unzipPath, true);
            }
        }
    }
}
