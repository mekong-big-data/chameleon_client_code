﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Module.Azure.QueueStorage;
using Module.TinEye.Models;
using Moq;
using Newtonsoft.Json;
using Service.Manage.Contracts.Models;
using Service.Manage.Contracts.Types;
using Service.Manage.Core.Constants;
using Service.Tenant.Contracts.Models;

namespace Service.Manage.Functions.Tests.CollectionUploadFunctionTests
{
    [TestClass]
    public class CollectionUploadFunction_GivenOneTextFile : CollectionUploadFunctionTestBase
    {

        [TestMethod]
        public async Task WhenFunctionIsTriggered_NoFilesAreUploaded()
        {
            // Arrange
            var newUpload = JsonConvert.SerializeObject(new CollectionUploadModel
            {
                CollectionUploadId = 1,
                TenantUserId = 77,
                Status = CollectionUploadStatus.Uploaded,
                UploadFileName = "OneTextFile.zip"
            });

            // Act
            var function = new CollectionUploadFunction(
                BlobStorage.Object,
                TenantServiceApi.Object,
                FileSystem,
                TinEyeService.Object,
                ImageCropMatchService.Object,
                Downloader.Object,
                CollectionsRepository.Object,
                CollectionUploadsRepository.Object,
                ImagesRepository.Object);
            await function.Run(newUpload, Logger.Object);

            // Assert
            CollectionsRepository.Verify(
                x => x.CreateCollection(It.IsAny<CollectionModel>()), 
                Times.Exactly(0));

            CollectionUploadsRepository.Verify(
                x => x.UpdateCollectionUpload(It.IsAny<CollectionUploadModel>()),
                Times.Exactly(2));

            BlobStorage.Verify(
                x => x.Store(BlobContainers.ManageContainer, 
                    It.IsAny<string>(),
                    It.IsAny<Stream>()),
                Times.Exactly(0));

            TinEyeService.Verify(
                x => x.AddImage(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    "tinuser",
                    "tinpass",
                    CancellationToken.None),
                Times.Exactly(0));

            Downloader.Verify(
                x => x.DownloadFile(It.IsAny<Uri>(), It.IsAny<string>()),
                Times.Exactly(0));


            ImagesRepository.Verify(
                x => x.CreateImage(It.IsAny<ImageModel>()),
                Times.Exactly(0));

        }

        [TestCleanup]
        public void Cleanup()
        {
            var zipPath = FileSystem.Path.Combine(FileSystem.Path.GetTempPath(), "OneTextFile.zip");
            if (FileSystem.File.Exists(zipPath))
            {
                FileSystem.File.Delete(zipPath);
            }
            var unzipPath = FileSystem.Path.Combine(FileSystem.Path.GetTempPath(), "OneTextFile");
            if (FileSystem.Directory.Exists(unzipPath))
            {
                FileSystem.Directory.Delete(unzipPath, true);
            }
        }

    }
}
