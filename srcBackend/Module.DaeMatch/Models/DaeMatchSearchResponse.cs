﻿using Newtonsoft.Json;

namespace Module.DaeMatch.Models
{
    public class DaeMatchSearchResponse
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "error")]
        public string[] Errors { get; set; }

        [JsonProperty(PropertyName = "result")]
        public DaeMatchSearchResult[] Result { get; set; }
    }

    public class DaeMatchSearchResult
    {
        [JsonProperty(PropertyName = "match")]
        public string IsMatch { get; set; }

        [JsonProperty(PropertyName = "distance")]
        public decimal Distance { get; set; }

        [JsonProperty(PropertyName = "threshold")]
        public decimal Threshold { get; set; }

        [JsonProperty(PropertyName = "image_client")]
        public DaeMatchSearchResultImageClient ImageClient { get; set; }

        [JsonProperty(PropertyName = "image_generated")]
        public ImageMatchSearchResultImageGenerated ImageGenerated { get; set; }
    }

    public class DaeMatchSearchResultImageClient
    {
        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }
    }

    public class ImageMatchSearchResultImageGenerated
    {
        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }
    }
}
