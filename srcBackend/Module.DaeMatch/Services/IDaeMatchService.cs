﻿using System.Threading.Tasks;
using Module.DaeMatch.Models;

namespace Module.DaeMatch.Services
{
    public interface IDaeMatchService
    {
        Task<DaeMatchSearchResponse> SearchImage(string imageUrl, string tenantCode, string imageKey);
    }
}
