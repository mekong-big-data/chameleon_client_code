﻿using System.Threading.Tasks;
using Module.DaeMatch.Models;
using Refit;

namespace Module.DaeMatch.Services
{
    public interface IRefitDaeMatchService
    {
        [Get("/api/fmatchdae")]
        Task<DaeMatchSearchResponse> SearchImage(string img, string cid, string iid, string code);

    }
}
