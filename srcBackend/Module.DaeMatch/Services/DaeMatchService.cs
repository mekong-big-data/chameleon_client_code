﻿using System;
using System.Threading.Tasks;
using Module.DaeMatch.Models;
using Refit;

namespace Module.DaeMatch.Services
{
    public class DaeMatchService : IDaeMatchService
    {
        private const string ApiBaseUrl = "https://imagematchcontainerfunctions.azurewebsites.net";
        private const string ApiCodeQueryString = "fn/QqSbCkfF/9QxU1VZXfqZeWA0Go5gzlvnD4b2x0VDkJ3AInU47zw==";


        public async Task<DaeMatchSearchResponse> SearchImage(string imageUrl, string tenantCode, string imageKey)
        {
            var daeMatchService = RestService.For<IRefitDaeMatchService>(ApiBaseUrl);

            try
            {
                return await daeMatchService.SearchImage(imageUrl, tenantCode, imageKey, ApiCodeQueryString);
            }
            catch (ApiException apiEx)
            {
                if (apiEx.StatusCode.ToString() == "400")
                {
                    return new DaeMatchSearchResponse()
                    {
                        Status = "fail",
                        Result = new DaeMatchSearchResult[0],
                        Errors = new string[0]
                    };
                }
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
