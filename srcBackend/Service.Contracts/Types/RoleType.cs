﻿namespace Service.Contracts.Types
{
    public enum RoleType
    {
        Basic,
        Admin,
        SuperAdmin
    }
}
