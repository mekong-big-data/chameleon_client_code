﻿using System;
using Service.Contracts.Types;

namespace Service.Contracts
{
    public class AuthContext
    {
        public AuthTenantContext Tenant { get; set; }
        public AuthTenantUserContext TenantUser { get; set; }

        public AuthContext()
        {
            Tenant = new AuthTenantContext();
            TenantUser = new AuthTenantUserContext();
        }
    }

    public class AuthTenantContext
    {
        public int TenantId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string PrimaryLogoFilePath { get; set; }
        public string SuccessfulMatchText { get; set; }
        public string FailedMatchText { get; set; }
        public string WebsiteUrl { get; set; }
        public string CertificateFilePath { get; set; }
        public string PromotionalVideoFilePath { get; set; }
    }

    public class AuthTenantUserContext
    {
        public int TenantUserId { get; set; }
        public RoleType Role { get; set; }
        public Guid? UniqueIdentifier { get; set; }
        public string EmailAddress { get; set; }
    }
}
