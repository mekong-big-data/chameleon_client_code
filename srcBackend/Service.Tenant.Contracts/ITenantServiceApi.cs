﻿using System;
using Refit;
using System.Threading;
using System.Threading.Tasks;
using Service.Contracts;
using Service.Tenant.Contracts.Models;

namespace Service.Tenant.Contracts
{
    [Headers("Authorization: Basic")]
    public interface ITenantServiceApi
    {
        [Get("/serviceapi/tenant")]
        Task<TenantModel> GetTenant(int tenantId, CancellationToken cancellationToken);

        [Get("/serviceapi/tenant/byname")]
        Task<TenantModel> GetTenantByName(string tenantName, CancellationToken cancellationToken);

        [Get("/serviceapi/tenantuser")]
        Task<TenantUserModel> GetTenantUser(int tenantUserId, CancellationToken cancellationToken);

        [Get("/serviceapi/authcontext")]
        Task<AuthContext> GetAuthContext(string tenantName, Guid authUserId, CancellationToken cancellationToken);

        [Get("/serviceapi/authcontext/createIfNotExists")]
        Task<AuthContext> GetAuthContextCreateIfNotExists(string tenantName, Guid authUserId, CancellationToken cancellationToken);
    }
}
