﻿using System;
using Service.Contracts.Types;

namespace Service.Tenant.Contracts.Models
{
    public class TenantUserModel
    {
        public int TenantUserId { get; set; }
        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public RoleType Role { get; set; }
        public Guid? UniqueIdentifier { get; set; }
        public string EmailAddress { get; set; }
    }
}
