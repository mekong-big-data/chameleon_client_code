﻿namespace Service.Tenant.Contracts.Models
{
    public class TenantModel
    {
        public int TenantId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string TinEyeAccountUserName { get; set; }
        public string TinEyeAccountPassword { get; set; }
        public string PrimaryLogoFilePath { get; set; }
        public string PrimaryLogoFileName { get; set; }
        public string SuccessfulMatchText { get; set; }
        public string FailedMatchText { get; set; }
        public string WebsiteUrl { get; set; }
        public string CertificateFilePath { get; set; }
        public string CertificateFileName { get; set; }
        public string PromotionalVideoFilePath { get; set; }
        public string PromotionalVideoFileName { get; set; }
    }
}
