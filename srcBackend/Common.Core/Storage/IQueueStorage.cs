﻿using System.IO;
using System.Threading.Tasks;

namespace Common.Core.Storage
{
    public interface IQueueStorage
    {
        Task<bool> QueueMessage(string queueName, string message);
    }
}
