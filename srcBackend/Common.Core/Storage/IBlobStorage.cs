﻿using System.IO;
using System.Threading.Tasks;

namespace Common.Core.Storage
{
    public interface IBlobStorage
    {
        Task<bool> Store(string containerName, string blobName, Stream inputStream);
        Task<Stream> Retrieve(string containerName, string blobName);
        Task<bool> Delete(string containerName, string blobName);
        BlobSasToken GenerateSasToken(double minuteExpiry = 5);
        string GenerateBlobPath(string containerName, string blobName);
        string GenerateBlobUrl(string containerName, string blobName);
        string GenerateBlobUrl(string blobPath);
        string RetrieveBlobName(string containerName, string blobPath);
    }
}
