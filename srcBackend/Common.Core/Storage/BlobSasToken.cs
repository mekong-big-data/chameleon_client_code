﻿namespace Common.Core.Storage
{
    public class BlobSasToken
    {
        public string StorageUri { get; set; }
        public string StorageAccessToken { get; set; }
    }
}
