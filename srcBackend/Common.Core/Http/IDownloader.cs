﻿using System;
using System.Threading.Tasks;

namespace Common.Core.Http
{
    public interface IDownloader
    {
        Task DownloadFile(Uri uri, string destinationPath);
    }
}
