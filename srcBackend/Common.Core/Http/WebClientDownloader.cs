﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Common.Core.Http
{
    public class WebClientDownloader : IDownloader
    {

        public async Task DownloadFile(Uri uri, string destinationPath)
        {
            using (var client = new WebClient())
            {
                await client.DownloadFileTaskAsync(uri, destinationPath);
            }
        }

    }
}
