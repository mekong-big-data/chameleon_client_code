﻿using System;

namespace Common.Core.Authentication.Responses
{
    public class UserLookupResponse
    {
        public Guid Id { get; set; }
        public string EmailAddress { get; set; }
    }
}
