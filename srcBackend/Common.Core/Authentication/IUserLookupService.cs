﻿using System;
using System.Threading.Tasks;
using Common.Core.Authentication.Responses;

namespace Common.Core.Authentication
{
    public interface IUserLookupService
    {
        Task<UserLookupResponse> LookupUser(Guid id);
    }
}
