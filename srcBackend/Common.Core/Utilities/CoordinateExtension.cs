﻿using NetTopologySuite;
using NetTopologySuite.Geometries;

namespace Common.Core.Utilities
{
    public static class CoordinateExtension
    {
        public static Point ToPoint(this Coordinate coordinate)
        {
            // create geometry factory object
            // 4326 refers to WGS 84, a standard used in GPS and other geographic systems.
            var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326);

            // create coordinate points
            var point = geometryFactory.CreatePoint(coordinate);

            return point;
        }
    }
}
