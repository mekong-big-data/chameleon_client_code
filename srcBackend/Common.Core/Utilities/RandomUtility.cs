﻿using System;

namespace Common.Core.Utilities
{
    public static class RandomUtility
    {

        public static string GetRandomString(int length)
        {
            if (length > 32)
            {
                throw new ArgumentException("Length cannot exceed 32", nameof(length));
            }
            return Guid.NewGuid().ToString("N").Substring(0, length);
        }

    }
}
