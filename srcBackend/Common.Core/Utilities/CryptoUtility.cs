﻿using System;
using System.Security.Cryptography;

namespace Common.Core.Utilities
{
    public static class CryptoUtility
    {

        public static string GetFileSHA256Hash(byte[] byteArray)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                return Convert.ToBase64String(sha256.ComputeHash(byteArray));
            }
        }

    }
}
