﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Search.UrlShortenerWebApi.Controllers
{
    [ApiController]
    public class UrlController : ControllerBase
    {

        [HttpGet("d/{tenant}/{key}")]
        public RedirectResult RedirectB(string tenant, string key)
        {
            return Redirect($"https://chameleon-pwa-dev.web.app/{tenant}/search/capture/{key}_20_200_10_50_1_6_10");
        }

        [HttpGet("p/{tenant}/{key}")]
        public RedirectResult RedirectA(string tenant, string key)
        {
            return Redirect($"https://chameleon-pwa.web.app/{tenant}/search/capture/{key}_20_200_10_50_1_6_10");
        }

    }
}
