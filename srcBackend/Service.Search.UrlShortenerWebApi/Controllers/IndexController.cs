﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Service.Search.UrlShortenerWebApi.Controllers
{
    [Route("")]
    [ApiController]
    public class IndexController : ControllerBase
    {

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get()
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            return Content($"UrlShortener {environment} OK");
        }

    }
}
