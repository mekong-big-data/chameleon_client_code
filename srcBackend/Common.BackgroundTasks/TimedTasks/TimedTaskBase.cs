﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Common.BackgroundTasks.TimedTasks
{
    public abstract class TimedTaskBase<T> : IHostedService, IDisposable
    {
        protected IServiceProvider Services { get; }
        protected abstract string TaskName { get; }
        protected abstract TimeSpan Interval { get; }
        protected readonly ILogger<T> Logger;
        protected readonly bool IsDevelopmentEnvironment;

        private readonly object _padlock = new object();
        private Timer _timer;

        protected TimedTaskBase(
            IServiceProvider services,
            ILogger<T> logger)
        {
            Services = services;
            Logger = logger;

            IsDevelopmentEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
        }

        protected abstract Task InvokeTask(IServiceScope scope, CancellationToken cancellationToken);

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"{TaskName} task running.");

            _timer = new Timer(state =>
            {
                if (!Monitor.TryEnter(_padlock))
                {
                    Logger.LogWarning($"{TaskName} still being invoked... skipping");
                    return;
                }

                try
                {
                    using (var scope = Services.CreateScope())
                    {
                        InvokeTask(scope, cancellationToken).Wait(cancellationToken);
                    }
                }
                catch (AggregateException aEx)
                {
                    foreach (var ex in aEx.InnerExceptions)
                    {
                        Logger.LogError(ex, ex.Message);
                    }
                }
                finally
                {
                    Monitor.Exit(_padlock);
                }
            }, null, TimeSpan.Zero, Interval);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation($"{TaskName} task is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
