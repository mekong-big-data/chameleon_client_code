using System;
using AutoMapper;
using Common.Core.Authentication;
using Common.Core.Storage;
using Common.WebApi;
using Common.WebApi.Http;
using Common.WebApi.Middleware;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Module.Azure.ActiveDirectory;
using Module.Azure.BlobStorage;
using Module.Azure.QueueStorage;
using Module.TinEye.Services;
using Refit;
using Service.Manage.Core.Database;
using Service.Manage.Core.Mappings;
using Service.Manage.Core.Repositories;
using Service.Manage.Core.Repositories.Interfaces;
using Service.Tenant.Contracts;
using Service.WebApi.Middleware;
using Service.WebApi.Security;
using Service.WebApi.Security.AuthHandlers;
using Service.WebApi.Security.Requirements;
using Service.WebApi.Services;

namespace Service.Manage.WebApi
{
    public class Startup
    {
        public static string ApplicationName = "ChameleonManage";
        public IWebHostEnvironment CurrentEnvironment { get; }
        public IConfiguration Configuration { get; }
        public readonly bool IsDevelopmentEnvironment;

        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            CurrentEnvironment = env;
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            IsDevelopmentEnvironment = !env.IsEnvironment("Production");
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // logging
            var loggerFactory = new LoggerFactory();
            var logger = loggerFactory.CreateLogger<Startup>();

            // todo: configure external logging for prod

            services.AddSingleton<ILoggerFactory>(loggerFactory);
            services.AddSingleton<ILogger>(logger);
            services.AddLogging();

            // auth
            StartupAuthentication.Register(services, Configuration, options =>
            {
                options.AddPolicy(AccessPolicies.IsAdmin, policy => policy.Requirements.Add(new IsAdminRequirement()));
                options.AddPolicy(AccessPolicies.IsSuperAdmin, policy => policy.Requirements.Add(new IsSuperAdminRequirement()));
            });
            //StartupAuthentication.Register(services, Configuration, options =>
            //{
            //    options.AddPolicy(AccessPolicies.IsAdmin, policy => policy.Requirements.Add(new IsAdminRequirement()));
            //    options.AddPolicy(AccessPolicies.IsSuperAdmin, policy => policy.Requirements.Add(new IsSuperAdminRequirement()));
            //}); // todo: uncomment out to re-introduce Auth

            // auto-mapper
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DataMappingProfile>();
            });
            services.AddSingleton(mapperConfig);
            services.AddSingleton(mapperConfig.CreateMapper());

            mapperConfig.AssertConfigurationIsValid();

            // entity framework
            services.AddDbContext<ManageDataContext>(options =>
            {
                options.UseSqlServer(new SqlConnection(Environment.GetEnvironmentVariable("EF_CONNECTION_STRING")),
                    x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, ManageDataContext.ContextSchema));
            });

            // services
            services.AddScoped<IQueueStorage, QueueStorageService>();
            services.AddScoped<IAuthContextService, AuthContextService>();
            services.AddScoped<IBlobStorage, BlobStorageService>();
            services.AddScoped<TinEyeService>();

            // serviceapi
            services.AddTransient<ServiceApiBasicAuthHandler>();
            services.AddRefitClient<ITenantServiceApi>()
                .ConfigureHttpClient(c => c.BaseAddress = new Uri(Configuration.GetValue<string>("ServiceApi:TenantServiceApiUrl")))
                .AddHttpMessageHandler<ServiceApiBasicAuthHandler>();

            // auth handlers
            services.AddScoped<IAuthorizationHandler, IsAdminAuthorizationHandler>();
            services.AddScoped<IAuthorizationHandler, IsSuperAdminAuthorizationHandler>();

            // repos
            services.AddScoped<IImagesRepository, ImagesRepository>();
            services.AddScoped<ICollectionsRepository, CollectionsRepository>();
            services.AddScoped<ICollectionUploadsRepository, CollectionUploadsRepository>();

            // general setup
            WebApiInitialisation.Registration(services, Configuration, IsDevelopmentEnvironment);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (IsDevelopmentEnvironment)
            {
                app.UseDeveloperExceptionPage();

                // swagger
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{ApplicationName} API");
                });
            }

            app.UseCors(
                options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()
            );

            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseRouting();

            app.UseAuthentication();
            app.UseMiddleware<TenantUserContextMiddleware>();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            InitializeDatabase(app);
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<ManageDataContext>().Database.Migrate();
            }
        }
    }
}
