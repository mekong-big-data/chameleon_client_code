﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Common.Core.Storage;
using Microsoft.AspNetCore.Authorization;
using Module.Azure.QueueStorage;
using Service.Manage.Contracts.Models;
using Service.Manage.Contracts.Types;
using Service.Manage.Core.Constants;
using Service.Manage.Core.Repositories.Interfaces;
using Service.WebApi.Security;
using Service.WebApi.Services;

namespace Service.Manage.WebApi.Controllers
{
    [Authorize(Policy = AccessPolicies.IsAdmin)]
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionsController : ControllerBase
    {
        private readonly ICollectionsRepository _collectionsRepository;
        private readonly ICollectionUploadsRepository _collectionUploadsRepository;
        private readonly IAuthContextService _authContextService;
        private readonly IBlobStorage _blobStorage;
        private readonly IQueueStorage _queueStorage;

        public CollectionsController(
            ICollectionsRepository collectionsRepository,
            ICollectionUploadsRepository collectionUploadsRepository,
            IAuthContextService authContextService,
            IBlobStorage blobStorage,
            IQueueStorage queueStorage)
        {
            _collectionsRepository = collectionsRepository;
            _collectionUploadsRepository = collectionUploadsRepository;
            _authContextService = authContextService;
            _blobStorage = blobStorage;
            _queueStorage = queueStorage;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetCollections()
        {
            var tenantId = _authContextService.AuthContext.Tenant.TenantId;

            var collections = await _collectionsRepository.GetCollectionsByTenantId(tenantId);
            foreach (var collection in collections)
            {
                collection.DownloadUrl = _blobStorage.GenerateBlobUrl(
                    BlobContainers.CollectionContainer,
                    $"{collection.Name}_{collection.CollectionId}.zip"
                );
            }
            return Ok(collections);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Post(CollectionModel model)
        {
            return Ok(await _collectionsRepository.CreateCollection(model));
        }

        [HttpDelete]
        [Route("{collectionId}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Delete(int collectionId)
        {
            var collection = await _collectionsRepository.GetCollection(collectionId);

            if (collection != null)
            {
                var collectionUpload = await _collectionUploadsRepository.GetCollectionUpload(collection.CollectionUploadId);
                collectionUpload.Status = CollectionUploadStatus.Discarded;

                await _collectionUploadsRepository.UpdateCollectionUpload(collectionUpload);

                await _queueStorage.QueueMessage(
                    QueueNames.CollectionRemoval,
                    QueueMessageUtility.SerialiseMessage(collectionUpload));
            }

            return Ok(true);
        }

    }
}
