﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Service.Manage.Contracts.Models;
using Service.Manage.Core.Repositories.Interfaces;
using Service.WebApi.Security;
using Common.Core.Storage;

namespace Service.Manage.WebApi.Controllers
{
    [Authorize(Policy = AccessPolicies.IsAdmin)]
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        private readonly IImagesRepository _imagesRepository;
        private readonly IBlobStorage _blobStorage;
        public ImagesController(
            IImagesRepository imagesRepository, IBlobStorage blobStorage)
        {
            _imagesRepository = imagesRepository;
            _blobStorage = blobStorage;
        }

        [HttpGet("collectionImages/{collectionId}")]
        [ProducesResponseType(typeof(IList<ImageModel>), 200)]
        public async Task<IActionResult> GetImages(int collectionId)
        {
            return Ok(await _imagesRepository.GetImages(collectionId));
        }

        [HttpGet]
        [Route("{imageId}")]
        [ProducesResponseType(typeof(ImageModel), 200)]
        public async Task<IActionResult> GetImage(int imageId)
        {
            return Ok(await _imagesRepository.GetImage(imageId));
        }

        [HttpPost]
        [ProducesResponseType(typeof(ImageModel), 200)]
        public async Task<IActionResult> AddImage([FromBody] ImageModel model)
        {
            return Ok(await _imagesRepository.CreateImage(model));
        }

        [HttpDelete]
        [Route("{imageId}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> DeleteImage(int imageId)
        {
            return Ok(await _imagesRepository.DeleteImage(imageId));
        }
    }
}
