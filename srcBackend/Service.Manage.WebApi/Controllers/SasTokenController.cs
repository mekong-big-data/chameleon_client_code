﻿using Microsoft.AspNetCore.Mvc;
using Common.Core.Storage;
using Microsoft.AspNetCore.Authorization;

namespace Service.Manage.WebApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SasTokenController : ControllerBase
    {
        private readonly IBlobStorage _blobStorage;

        public SasTokenController(
            IBlobStorage blobStorage)
        {
            _blobStorage = blobStorage;
        }

        [Authorize]
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Get()
        {
            return Ok(_blobStorage.GenerateSasToken());
        }

        [HttpGet("legacy")]
        [ProducesResponseType(200)]
        public IActionResult GetLegacy()
        {
            return Ok(_blobStorage.GenerateSasToken());
        }
    }
}
