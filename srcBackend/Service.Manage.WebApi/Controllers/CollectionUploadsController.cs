﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Common.Core.Storage;
using Microsoft.AspNetCore.Authorization;
using Module.Azure.QueueStorage;
using Service.Manage.Core.Constants;
using Service.Manage.Contracts.Models;
using Service.Manage.Contracts.Types;
using Service.Manage.Core.Repositories.Interfaces;
using Service.WebApi.Security;
using Service.WebApi.Services;

namespace Service.Manage.WebApi.Controllers
{
    [Authorize(Policy = AccessPolicies.IsAdmin)]
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionUploadsController : ControllerBase
    {
        private readonly IQueueStorage _queueStorage;
        private readonly IAuthContextService _userContextService;
        private readonly ICollectionUploadsRepository _collectionUploadsRepository;

        public CollectionUploadsController(
            IQueueStorage queueStorage,
            IAuthContextService userContextService,
            ICollectionUploadsRepository collectionUploadsRepository)
        {
            _queueStorage = queueStorage;
            _userContextService = userContextService;
            _collectionUploadsRepository = collectionUploadsRepository;
        }

        [HttpGet]
        [Route("{uploadId}")]
        [ProducesResponseType(typeof(ImageModel), 200)]
        public async Task<IActionResult> GetImage(int uploadId)
        {
            return Ok(await _collectionUploadsRepository.GetCollectionUpload(uploadId));
        }


        [HttpPost]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Post(CollectionUploadModel model)
        {
            model.TenantUserId = _userContextService.AuthContext.TenantUser.TenantUserId;

            var newCollectionUpload = await _collectionUploadsRepository.CreateCollectionUpload(model);

            await _queueStorage.QueueMessage(
                QueueNames.CollectionUpload, 
                QueueMessageUtility.SerialiseMessage(newCollectionUpload));

            return Ok(newCollectionUpload);
        }

        [HttpPut]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Update(CollectionUploadModel model)
        {
            return Ok(await _collectionUploadsRepository.UpdateCollectionUpload(model));
        }

    }
}
