﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Web;
using Service.Manage.Core.Repositories.Interfaces;

namespace Service.Manage.WebApi.Controllers.Service
{
    [Route("serviceapi/images")]
    [ApiController]
    public class ServiceImagesController : ControllerBase
    {

        private readonly IImagesRepository _imagesRepository;


        public ServiceImagesController(
            IImagesRepository imagesRepository)
        {
            _imagesRepository = imagesRepository;
        }


        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetByImageFilePath(string imageFilePath)
        {
            var imageFilePathDecoded = HttpUtility.UrlDecode(imageFilePath);
            return Ok(await _imagesRepository.GetImageByImageFilePath(imageFilePathDecoded));
        }

    }
}
