﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using QRCoder;

namespace Service.Manage.QrMonkeyGenConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var patternFilesDirectory = args[0];
            var outputDirectory = args[1];

            Console.WriteLine($"Reading pattern files at {patternFilesDirectory}...");
            Console.WriteLine($"Output directory at {outputDirectory}...");

            if (!Directory.Exists(patternFilesDirectory))
            {
                Console.WriteLine("Error: input directory does not exist!");
                Console.ReadKey();
                return;
            }

            if (!Directory.Exists(outputDirectory))
            {
                Console.WriteLine("Error: output directory does not exist!");
                Console.ReadKey();
                return;
            }

            var imageFilePaths = Directory.GetFiles(patternFilesDirectory, "*.jpg", SearchOption.AllDirectories)
                .ToList();

            if (!imageFilePaths.Any())
            {
                Console.WriteLine("Error: no image files found!");
                Console.ReadKey();
                return;
            }

            foreach (var outputFile in Directory.GetFiles(outputDirectory, "*.*", SearchOption.AllDirectories))
            {
                File.Delete(outputFile);
            }

            var imageCnt = 0;
            foreach (var imageFilePath in imageFilePaths)
            {
                GenerateQrCode(imageFilePath, outputDirectory);
                imageCnt++;
            }

            Console.WriteLine($"Successfully generated {imageCnt} QR/SP images.");
            Console.ReadKey();
        }

        static void GenerateQrCode(string imageFilePath, string outputDirectory)
        {
            if (!File.Exists(imageFilePath))
            {
                Console.WriteLine($"Warn: file not found at {imageFilePath}.");
                return;
            }

            var fileInfo = new FileInfo(imageFilePath);

            var qrGenerator = new QRCodeGenerator();

            var qrCodeData = qrGenerator.CreateQrCode(
                "https://chameleon-pwa.web.app/acme3/search/capture/", 
                QRCodeGenerator.ECCLevel.Q);

            var qrCode = new QRCode(qrCodeData);

            var qrCodeImage = qrCode.GetGraphic(
                20, 
                Color.Black, 
                Color.White, 
                (Bitmap)Image.FromFile(imageFilePath),
                29,
                1);

            qrCodeImage.Save(Path.Combine(outputDirectory, $"QR_{fileInfo.Name}"));
        }

    }
}
