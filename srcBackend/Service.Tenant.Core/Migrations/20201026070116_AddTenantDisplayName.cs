﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Tenant.Core.Migrations
{
    public partial class AddTenantDisplayName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "tenant",
                table: "Tenants",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                schema: "tenant",
                table: "Tenants",
                maxLength: 250,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                schema: "tenant",
                table: "Tenants");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "tenant",
                table: "Tenants",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);
        }
    }
}
