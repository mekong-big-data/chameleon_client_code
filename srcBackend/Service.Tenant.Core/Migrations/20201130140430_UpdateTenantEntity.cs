﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Tenant.Core.Migrations
{
    public partial class UpdateTenantEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FailedMatchText",
                schema: "tenant",
                table: "Tenants",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SuccessfulMatchText",
                schema: "tenant",
                table: "Tenants",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FailedMatchText",
                schema: "tenant",
                table: "Tenants");

            migrationBuilder.DropColumn(
                name: "SuccessfulMatchText",
                schema: "tenant",
                table: "Tenants");
        }
    }
}
