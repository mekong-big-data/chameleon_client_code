﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Tenant.Core.Migrations
{
    public partial class UpdateTenantFieldsEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CertificateFilePath",
                schema: "tenant",
                table: "Tenants",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WebsiteUrl",
                schema: "tenant",
                table: "Tenants",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CertificateFilePath",
                schema: "tenant",
                table: "Tenants");

            migrationBuilder.DropColumn(
                name: "WebsiteUrl",
                schema: "tenant",
                table: "Tenants");
        }
    }
}
