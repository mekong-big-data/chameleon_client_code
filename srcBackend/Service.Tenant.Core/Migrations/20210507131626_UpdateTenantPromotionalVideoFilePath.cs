﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Service.Tenant.Core.Migrations
{
    public partial class UpdateTenantPromotionalVideoFilePath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PromotionalVideoFilePath",
                schema: "tenant",
                table: "Tenants",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PromotionalVideoFilePath",
                schema: "tenant",
                table: "Tenants");
        }
    }
}
