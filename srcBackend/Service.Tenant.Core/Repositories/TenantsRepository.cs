﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Service.Tenant.Core.Database;
using Service.Tenant.Core.Database.Entities;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Service.Tenant.Contracts.Models;
using Service.Tenant.Core.Repositories.Interfaces;

namespace Service.Tenant.Core.Repositories
{
    public class TenantsRepository : ITenantsRepository
    {
        private readonly TenantDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _mapperConfig;

        public TenantsRepository(
            TenantDataContext dataContext,
            IMapper mapper,
            MapperConfiguration mapperConfig)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _mapperConfig = mapperConfig;
        }

        public async Task<IList<TenantModel>> GetTenants()
        {
            var response = new List<TenantModel>();
            var tenants = await _dataContext.Tenants
                .ProjectTo<TenantModel>(_mapperConfig).ToListAsync();
            _mapper.Map(tenants, response);
            return response;
        }

        public async Task<TenantModel> GetTenant(int tenantId)
        {
            var response = new TenantModel();
            var tenant = await _dataContext.Tenants.FindAsync(tenantId);
            if (tenant == null)
            {
                throw new ValidationException("Tenant not found!");
            }
            _mapper.Map(tenant, response);
            return response;
        }

        public async Task<TenantModel> GetTenantByName(string name)
        {
            var response = new TenantModel();
            var tenant = await _dataContext.Tenants.SingleOrDefaultAsync(x => x.Name.ToLower() == name.ToLower());
            if (tenant == null)
            {
                throw new ValidationException("Tenant not found!");
            }
            _mapper.Map(tenant, response);
            return response;
        }

        public async Task<TenantModel> CreateTenant(TenantModel model)
        {
            var existingTenant = await _dataContext.Tenants.AnyAsync(x => x.Name.ToLower() == model.Name.ToLower());
            if (existingTenant)
            {
                throw new ValidationException($"Tenant name already exists");
            }

            var tenant = new TenantAccount();
            _mapper.Map(model, tenant);

            await _dataContext.AddAsync(tenant);
            await _dataContext.SaveChangesAsync();

            _mapper.Map(tenant, model);
            return model;
        }

        public async Task<TenantModel> UpdateTenant(TenantModel model)
        {
            var tenant = await _dataContext.Tenants.FirstOrDefaultAsync(x => x.TenantId == model.TenantId);
            if (tenant == null)
            {
                throw new ValidationException("Tenant not found!");
            }
            _mapper.Map(model, tenant);

            await _dataContext.SaveChangesAsync();

            return model;
        }

        public async Task<bool> DeleteTenant(int tenantId)
        {
            var tenant = await _dataContext.Tenants.Include(x => x.TenantUsers).FirstOrDefaultAsync(x => x.TenantId == tenantId);
            if (tenant == null)
            {
                throw new ValidationException("Tenant not found!");
            }

            if (tenant.TenantUsers.Any())
            {
                throw new ValidationException("This Tenant account cannot be deleted. Tenant users are associated with this account.");
            }

            _dataContext.Tenants.Remove(tenant);
            await _dataContext.SaveChangesAsync();

            return true;
        }
    }
}
