﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Tenant.Contracts.Models;

namespace Service.Tenant.Core.Repositories.Interfaces
{
    public interface ITenantsRepository
    {
        Task<IList<TenantModel>> GetTenants();
        Task<TenantModel> GetTenant(int tenantId);
        Task<TenantModel> GetTenantByName(string name);
        Task<TenantModel> CreateTenant(TenantModel model);
        Task<TenantModel> UpdateTenant(TenantModel model);
        Task<bool> DeleteTenant(int tenantId);
    }
}