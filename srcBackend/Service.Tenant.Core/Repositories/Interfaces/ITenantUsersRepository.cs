﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Tenant.Contracts.Models;

namespace Service.Tenant.Core.Repositories.Interfaces
{
    public interface ITenantUsersRepository
    {
        Task<IList<TenantUserModel>> GetTenantUsers();
        Task<IList<TenantUserModel>> GetTenantUsersByTenantId(int tenantId);
        Task<TenantUserModel> GetTenantUser(int tenantUserId);
        Task<TenantUserModel> GetTenantUserByUniqueIdentifier(int tenantId, Guid uniqueIdentifier);
        Task<TenantUserModel> GetTenantUserByTenantNameAndUniqueIdentifier(string tenantName, Guid uniqueIdentifier);
        Task<TenantUserModel> GetTenantUserByEmailAddress(int tenantId, string emailAddress);
        Task<TenantUserModel> CreateTenantUser(TenantUserModel model);
        Task<TenantUserModel> UpdateTenantUser(TenantUserModel model);
        Task<bool> DeleteTenantUser(int tenantUserId);
    }
}