﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Service.Tenant.Core.Database;
using Service.Tenant.Core.Database.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Service.Tenant.Contracts.Models;
using System.Linq;
using Service.Tenant.Core.Repositories.Interfaces;

namespace Service.Tenant.Core.Repositories
{
    public class TenantUsersRepository : ITenantUsersRepository
    {
        private readonly TenantDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _mapperConfig;

        public TenantUsersRepository(
            TenantDataContext dataContext,
            IMapper mapper,
            MapperConfiguration mapperConfig)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _mapperConfig = mapperConfig;
        }
        public async Task<IList<TenantUserModel>> GetTenantUsers()
        {
            var response = new List<TenantUserModel>();
            var tenantUsers = await _dataContext.TenantUsers
                .ProjectTo<TenantUserModel>(_mapperConfig).ToListAsync();
            _mapper.Map(tenantUsers, response);
            return response;
        }

        public async Task<IList<TenantUserModel>> GetTenantUsersByTenantId(int tenantId)
        {
            var response = new List<TenantUserModel>();
            var tenantUsers = await _dataContext.TenantUsers
               .Include(x => x.Tenant)
               .Where(x => x.TenantId == tenantId).ToListAsync();
            _mapper.Map(tenantUsers, response);
            return response;
        }

        public async Task<TenantUserModel> GetTenantUser(int tenantUserId)
        {
            var response = new TenantUserModel();
            var tenantUser = await _dataContext.TenantUsers
                .Include(x => x.Tenant)
                .SingleOrDefaultAsync(x => x.TenantUserId == tenantUserId);

            if (tenantUser == null)
            {
                throw new ValidationException("Tenant User not found!");
            }
            _mapper.Map(tenantUser, response);
            return response;
        }

        public async Task<TenantUserModel> GetTenantUserByUniqueIdentifier(int tenantId, Guid uniqueIdentifier)
        {
            var response = new TenantUserModel();
            var tenantUser = await _dataContext.TenantUsers
                .Include(x => x.Tenant)
                .SingleOrDefaultAsync(x => x.TenantId == tenantId && x.UniqueIdentifier == uniqueIdentifier);

            if (tenantUser == null)
            {
                return null;
            }
            _mapper.Map(tenantUser, response);
            return response;
        }

        public async Task<TenantUserModel> GetTenantUserByTenantNameAndUniqueIdentifier(string tenantName, Guid uniqueIdentifier)
        {
            var response = new TenantUserModel();
            var tenantUser = await _dataContext.TenantUsers
                .Include(x => x.Tenant)
                .SingleOrDefaultAsync(x => x.Tenant.Name == tenantName && x.UniqueIdentifier == uniqueIdentifier);

            if (tenantUser == null)
            {
                return null;
            }
            _mapper.Map(tenantUser, response);
            return response;
        }

        public async Task<TenantUserModel> GetTenantUserByEmailAddress(int tenantId, string emailAddress)
        {
            var response = new TenantUserModel();
            var tenantUser = await _dataContext.TenantUsers
                .Include(x => x.Tenant)
                .SingleOrDefaultAsync(x => x.TenantId == tenantId && x.EmailAddress == emailAddress.ToLower());

            if (tenantUser == null)
            {
                return null;
            }
            _mapper.Map(tenantUser, response);
            return response;
        }

        public async Task<TenantUserModel> CreateTenantUser(TenantUserModel model)
        {
            var existingTenantUser = await _dataContext.TenantUsers
                .AnyAsync(x => x.TenantId == model.TenantId && x.UniqueIdentifier == model.UniqueIdentifier);
            if (existingTenantUser)
            {
                throw new ValidationException("Tenant user already exists.");
            }

            var tenantUser = new TenantUser();
            _mapper.Map(model, tenantUser);

            await _dataContext.AddAsync(tenantUser);
            await _dataContext.SaveChangesAsync();

            return await GetTenantUser(tenantUser.TenantUserId);
        }

        public async Task<TenantUserModel> UpdateTenantUser(TenantUserModel model)
        {
            var tenantUser = await _dataContext.TenantUsers
                .SingleOrDefaultAsync(x => x.TenantUserId == model.TenantUserId);

            if (tenantUser == null)
            {
                throw new ValidationException("Tenant User not found!");
            }
            _mapper.Map(model, tenantUser);

            await _dataContext.SaveChangesAsync();

            return await GetTenantUser(tenantUser.TenantUserId);
        }

        public async Task<bool> DeleteTenantUser(int tenantUserId)
        {
            var tenantUser = await _dataContext.TenantUsers.FirstOrDefaultAsync(x => x.TenantUserId == tenantUserId);
            if (tenantUser == null)
            {
                throw new ValidationException("Tenant User not found!");
            }

            _dataContext.TenantUsers.Remove(tenantUser);
            await _dataContext.SaveChangesAsync();

            return true;
        }
    }
}
