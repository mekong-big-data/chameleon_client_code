﻿using AutoMapper;
using Service.Tenant.Core.Database.Entities;
using Service.Contracts;
using Service.Tenant.Contracts.Models;

namespace Service.Tenant.Core.Mappings
{
    public class DataMappingProfile : Profile
    {
        public DataMappingProfile()
        {
            CreateMap<TenantUserModel, TenantUser>()
              .ForMember(dest => dest.Tenant, opt => opt.Ignore())
              .ForMember(dest => dest.CreatedAt, opt => opt.Ignore())
              .ForMember(dest => dest.UpdatedAt, opt => opt.Ignore())
              .ForMember(dest => dest.DeletedAt, opt => opt.Ignore())
              .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.EmailAddress.ToLower()));
            CreateMap<TenantUser, TenantUserModel>()
                .ForMember(dest => dest.TenantName, opt => opt.MapFrom(src => src.Tenant != null ? src.Tenant.Name : null));

            CreateMap<TenantModel, TenantAccount>()
              .ForMember(dest => dest.TenantUsers, opt => opt.Ignore())
              .ForMember(dest => dest.CreatedAt, opt => opt.Ignore())
              .ForMember(dest => dest.UpdatedAt, opt => opt.Ignore())
              .ForMember(dest => dest.DeletedAt, opt => opt.Ignore())
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name.ToLower()));
            CreateMap<TenantAccount, TenantModel>()
              .ForMember(dest => dest.PrimaryLogoFileName, opt => opt.Ignore())
              .ForMember(dest => dest.CertificateFileName, opt => opt.Ignore())
              .ForMember(dest => dest.PromotionalVideoFileName, opt => opt.Ignore());

            CreateMap<TenantModel, AuthTenantContext>();
            CreateMap<TenantUserModel, AuthTenantUserContext>();
        }
    }
}
