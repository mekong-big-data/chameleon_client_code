﻿using Microsoft.EntityFrameworkCore;
using Module.EntityFramework;
using Service.Tenant.Core.Database.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Tenant.Core.Database
{
    public class TenantDataContext : DataContextBase<TenantDataContext>
    {

        public static string ContextSchema = "tenant";

        public DbSet<TenantAccount> Tenants { get; set; }
        public DbSet<TenantUser> TenantUsers { get; set; }

        public TenantDataContext(DbContextOptions<TenantDataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(ContextSchema);

            modelBuilder.Entity<TenantAccount>()
                .HasMany(g => g.TenantUsers)
                .WithOne(x => x.Tenant)
                .HasForeignKey(s => s.TenantId);

            modelBuilder.Entity<TenantUser>()
                .HasOne(s => s.Tenant)
                .WithMany(g => g.TenantUsers)
                .HasForeignKey(s => s.TenantId);

            modelBuilder.Entity<TenantAccount>().HasQueryFilter(p => !p.DeletedAt.HasValue);
            modelBuilder.Entity<TenantUser>().HasQueryFilter(p => !p.DeletedAt.HasValue);
        }

    }
}
