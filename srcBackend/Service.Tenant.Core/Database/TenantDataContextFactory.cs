﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Tenant.Core.Database
{
    public class TenantDataContextFactory : IDesignTimeDbContextFactory<TenantDataContext>
    {
        public TenantDataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<TenantDataContext>();
            var connectionString = @"Data Source=localhost;Initial Catalog=ChameleonDb;Integrated Security=SSPI";
            optionsBuilder.UseSqlServer(
                new SqlConnection(connectionString),
                x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, TenantDataContext.ContextSchema));
            return new TenantDataContext(optionsBuilder.Options);
        }
    }
}
