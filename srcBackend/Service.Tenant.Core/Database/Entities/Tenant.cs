﻿using Module.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Service.Tenant.Core.Database.Entities
{
    public class TenantAccount : EntityBase
    {
        [Key]
        public int TenantId { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(250)]
        public string DisplayName { get; set; }
        [MaxLength(50)]
        public string TinEyeAccountUserName { get; set; }
        [MaxLength(250)]
        public string TinEyeAccountPassword { get; set; }
        [MaxLength(250)]
        public string PrimaryLogoFilePath { get; set; }
        [MaxLength(500)]
        public string SuccessfulMatchText { get; set; }
        [MaxLength(500)]
        public string FailedMatchText { get; set; }
        [MaxLength(500)]
        public string WebsiteUrl { get; set; }
        [MaxLength(500)]
        public string CertificateFilePath { get; set; }
        [MaxLength(500)]
        public string PromotionalVideoFilePath { get; set; }
        public virtual ICollection<TenantUser> TenantUsers { get; set; }
    }
}
