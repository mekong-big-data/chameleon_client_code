﻿using Module.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using Service.Contracts.Types;

namespace Service.Tenant.Core.Database.Entities
{
    public class TenantUser : EntityBase
    {
        [Key]
        public int TenantUserId { get; set; }
        public int TenantId { get; set; }
        public RoleType Role { get; set; }
        public Guid? UniqueIdentifier { get; set; }
        [MaxLength(250)]
        public string EmailAddress { get; set; }

        public virtual TenantAccount Tenant { get; set; }
    }
}
