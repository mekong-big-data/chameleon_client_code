﻿namespace Module.ImageCropMatch.Constants
{
    internal static class CropMatchModes
    {
        public static string CId = "cid";
        public static string ImageMatch = "image-match";
        public static string Sift = "sift";
        public static string Crop = "crop";
    }
}
