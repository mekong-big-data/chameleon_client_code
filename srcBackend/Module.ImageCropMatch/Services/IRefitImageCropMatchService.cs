﻿using System.Threading.Tasks;
using Module.ImageCropMatch.Models;
using Refit;

namespace Module.ImageCropMatch.Services
{
    public interface IRefitImageCropMatchService
    {
        [Get("/api/fcropmatchimm")]
        Task<ImageCropMatchSearchResponse> SearchImage(string image_name, string cid, string mode, string code);
        
        [Get("/api/fsplattergenerator")]
        Task<ImageCropMatchGenerationResponse> GenerateImage(string cid, string env, string key, string code);
    }
}
