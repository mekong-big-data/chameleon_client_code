﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Module.ImageCropMatch.Constants;
using Module.ImageCropMatch.Models;
using Refit;

namespace Module.ImageCropMatch.Services
{
    public class ImageCropMatchService : IImageCropMatchService
    {
        private const string ApiBaseUrl = "https://imagematchcontainerfunctions.azurewebsites.net";
        private const string SearchApiCodeQueryString = "fn/QqSbCkfF/9QxU1VZXfqZeWA0Go5gzlvnD4b2x0VDkJ3AInU47zw==";
        private const string GenerateApiCodeQueryString = "3aCU18bUVRgndzu5daSQwWIvRhVf2uU8WeKvdk1cIOQV3gvJT5YL6w==";


        public async Task<ImageCropMatchSearchResponse> SearchImage(string imageName, string tenantCode)
        {
            var imageMatchService = RestService.For<IRefitImageCropMatchService>(ApiBaseUrl);

            try
            {
                return await imageMatchService.SearchImage(imageName, tenantCode, CropMatchModes.Sift, SearchApiCodeQueryString);
            }
            catch (ApiException apiEx)
            {
                if (apiEx.StatusCode.ToString() == "BadRequest")
                {
                    return new ImageCropMatchSearchResponse()
                    {
                        Status = "failed",
                        Result = new ImageCropMatchSearchResult[0],
                        Errors = new string[1] { "Failed to perform match." }
                    };
                }
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ImageCropMatchGenerationResponse> GenerateImage(string tenantCode, string environment, string key)
        {
            var imageMatchService = RestService.For<IRefitImageCropMatchService>(ApiBaseUrl);

            try
            {
                return await imageMatchService.GenerateImage(tenantCode, environment, key, GenerateApiCodeQueryString);
            }
            catch (ApiException apiEx)
            {
                if (apiEx.StatusCode.ToString() == "BadRequest")
                {
                    return new ImageCropMatchGenerationResponse()
                    {
                        Status = "failed",
                        Result = new ImageCropMatchGenerationResult[0],
                        Errors = new string[1] { "Failed to perform generation." }
                    };
                }
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ImageCropMatchSearchResponse> CropImage(string imageName, string tenantCode)
        {
            var imageMatchService = RestService.For<IRefitImageCropMatchService>(ApiBaseUrl);

            try
            {
                return await imageMatchService.SearchImage(imageName, tenantCode, CropMatchModes.Crop, SearchApiCodeQueryString);
            }
            catch (ApiException apiEx)
            {
                if (apiEx.StatusCode.ToString() == "BadRequest")
                {
                    return new ImageCropMatchSearchResponse()
                    {
                        Status = "failed",
                        Result = new ImageCropMatchSearchResult[0],
                        Errors = new string[1] { "Failed to perform crop." }
                    };
                }
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
