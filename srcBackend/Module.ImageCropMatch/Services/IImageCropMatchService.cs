﻿using System.Threading.Tasks;
using Module.ImageCropMatch.Models;

namespace Module.ImageCropMatch.Services
{
    public interface IImageCropMatchService
    {
        Task<ImageCropMatchSearchResponse> SearchImage(string imageName, string tenantCode);
        Task<ImageCropMatchGenerationResponse> GenerateImage(string tenantCode, string environment, string key);
        Task<ImageCropMatchSearchResponse> CropImage(string imageName, string tenantCode);
    }
}
