﻿using Newtonsoft.Json;

namespace Module.ImageCropMatch.Models
{
    public class ImageCropMatchSearchResponse
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "error")]
        public string[] Errors { get; set; }

        [JsonProperty(PropertyName = "result")]
        public ImageCropMatchSearchResult[] Result { get; set; }
    }

    public class ImageCropMatchSearchResult
    {
        [JsonProperty(PropertyName = "match")]
        public string IsMatch { get; set; }

        [JsonProperty(PropertyName = "distance")]
        public decimal Distance { get; set; }

        [JsonProperty(PropertyName = "threshold")]
        public decimal Threshold { get; set; }

        [JsonProperty(PropertyName = "image_client")]
        public ImageCropMatchSearchResultImageClient ImageClient { get; set; }

        [JsonProperty(PropertyName = "image_generated")]
        public ImageCropMatchSearchResultImageGenerated ImageGenerated { get; set; }
    }

    public class ImageCropMatchSearchResultImageClient
    {
        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }
    }

    public class ImageCropMatchSearchResultImageGenerated
    {
        [JsonProperty(PropertyName = "full_crop")]
        public string FullCropPath { get; set; }
        [JsonProperty(PropertyName = "sp_crop")]
        public string SpCropPath { get; set; }
        [JsonProperty(PropertyName = "key_raw")]
        public string KeyRaw { get; set; }
    }
}
