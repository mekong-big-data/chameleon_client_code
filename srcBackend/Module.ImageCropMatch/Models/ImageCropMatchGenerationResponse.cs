﻿using Newtonsoft.Json;

namespace Module.ImageCropMatch.Models
{
    public class ImageCropMatchGenerationResponse
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "error")]
        public string[] Errors { get; set; }

        [JsonProperty(PropertyName = "result")]
        public ImageCropMatchGenerationResult[] Result { get; set; }
    }

    public class ImageCropMatchGenerationResult
    {
        [JsonProperty(PropertyName = "image_generated")]
        public ImageCropMatchGenerationResultImageGenerated ImageGenerated { get; set; }
    }

    public class ImageCropMatchGenerationResultImageGenerated
    {
        [JsonProperty(PropertyName = "paths")]
        public ImageCropMatchGenerationResultImageGeneratedPaths Paths { get; set; }
    }

    public class ImageCropMatchGenerationResultImageGeneratedPaths
    {
        [JsonProperty(PropertyName = "qr_sp")]
        public string QrSpPath { get; set; }
        [JsonProperty(PropertyName = "sp")]
        public string SpPath { get; set; }
    }
}
