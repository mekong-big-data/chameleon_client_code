﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Common.Core.Authentication;
using Service.Contracts;
using Service.Contracts.Types;
using Service.Tenant.Contracts.Models;
using Service.Tenant.Core.Repositories;
using Service.Tenant.Core.Repositories.Interfaces;
using Service.WebApi.Services;

namespace Service.Tenant.WebApi.Services
{
    public class TenantAuthContextService : IAuthContextService
    {

        private AuthContext _authContext;

        private readonly IMapper _mapper;
        private readonly IUserLookupService _userLookupService;
        private readonly ITenantsRepository _tenantsRepository;
        private readonly ITenantUsersRepository _tenantUsersRepository;

        public AuthContext AuthContext => _authContext;

        public TenantAuthContextService(
            IMapper mapper,
            IUserLookupService userLookupService,
            ITenantsRepository tenantsRepository,
            ITenantUsersRepository tenantUsersRepository)
        {
            _mapper = mapper;
            _userLookupService = userLookupService;
            _tenantsRepository = tenantsRepository;
            _tenantUsersRepository = tenantUsersRepository;
        }

        public async Task<AuthContext> FetchOrCreateContext(string tenantName, Guid authUserId)
        {
            if (string.IsNullOrEmpty(tenantName) || authUserId == Guid.Empty)
            {
                return null;
            }

            var tenant = await _tenantsRepository.GetTenantByName(tenantName);
            var tenantUser = await _tenantUsersRepository.GetTenantUserByUniqueIdentifier(tenant.TenantId, authUserId);

            if (tenantUser == null)
            {
                // not an existing tenant user...
                var authUser = await _userLookupService.LookupUser(authUserId);
                tenantUser = await _tenantUsersRepository.GetTenantUserByEmailAddress(tenant.TenantId, authUser.EmailAddress);

                if (tenantUser != null)
                {
                    // associate authUserId with existing user found by email in tenant
                    tenantUser.UniqueIdentifier = authUserId;
                    tenantUser = await _tenantUsersRepository.UpdateTenantUser(tenantUser);
                }
                else
                {
                    // add new user to tenant
                    tenantUser = await _tenantUsersRepository.CreateTenantUser(new TenantUserModel
                    {
                        TenantId = tenant.TenantId,
                        UniqueIdentifier = authUserId,
                        Role = RoleType.Basic,
                        EmailAddress = authUser.EmailAddress
                    });
                }
            }

            var authContext = new AuthContext();
            _mapper.Map(tenant, authContext.Tenant);
            _mapper.Map(tenantUser, authContext.TenantUser);

            return authContext;
        }

        public async Task LoadContext(string tenantName, Guid authUserId)
        {
            _authContext = await FetchOrCreateContext(tenantName, authUserId);
        }
    }
}
