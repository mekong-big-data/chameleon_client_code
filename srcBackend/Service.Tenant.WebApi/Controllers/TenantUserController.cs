﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Common.Core.Authentication;
using Microsoft.AspNetCore.Authorization;
using Service.Contracts.Types;
using Service.Tenant.Contracts.Models;
using Service.Tenant.Core.Repositories;
using Service.Tenant.Core.Repositories.Interfaces;
using Service.WebApi.Security;
using Service.WebApi.Services;

namespace Service.Tenant.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TenantUserController : ControllerBase
    {
        private readonly ITenantUsersRepository _tenantUsersRepository;
        private readonly IAuthContextService _userContextService;

        public TenantUserController(
            ITenantUsersRepository tenantUsersRepository,
            IAuthContextService userContextService)
        {
            _userContextService = userContextService;
            _tenantUsersRepository = tenantUsersRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("me")]
        [ProducesResponseType(200)]
        public IActionResult GetMe()
        {
            if (_userContextService.AuthContext == null)
            {
                return Unauthorized();
            }

            return Ok(_userContextService.AuthContext);
        }
        
        [HttpGet]
        [Authorize(Policy = AccessPolicies.IsSuperAdmin)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetTenantUsers()
        {
            return Ok(await _tenantUsersRepository.GetTenantUsers());
        }

        [HttpGet]
        [Authorize(Policy = AccessPolicies.IsSuperAdmin)]
        [Route("tenantUsersBytenantId/{tenantId}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetTenantUsersBytenantId(int tenantId)
        {
            return Ok(await _tenantUsersRepository.GetTenantUsersByTenantId(tenantId));
        }

        [HttpGet]
        [Authorize(Policy = AccessPolicies.IsSuperAdmin)]
        [Route("{tenantUserId}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetTenantUser(int tenantUserId)
        {
            return Ok(await _tenantUsersRepository.GetTenantUser(tenantUserId));
        }

        [HttpPost]
        [Authorize(Policy = AccessPolicies.IsSuperAdmin)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Post(TenantUserModel model)
        {
            return Ok(await _tenantUsersRepository.CreateTenantUser(model));
        }

        [HttpPut]
        [Authorize(Policy = AccessPolicies.IsSuperAdmin)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Update(TenantUserModel model)
        {
            return Ok(await _tenantUsersRepository.UpdateTenantUser(model));
        }

        [HttpDelete]
        [Authorize(Policy = AccessPolicies.IsSuperAdmin)]
        [Route("{tenantUserId}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> DeleteTenantUser(int tenantUserId)
        {
            return Ok(await _tenantUsersRepository.DeleteTenantUser(tenantUserId));
        }
    }
}
