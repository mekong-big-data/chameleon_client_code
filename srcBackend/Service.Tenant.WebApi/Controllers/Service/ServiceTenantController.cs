﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Common.WebApi.Attributes;
using Service.Tenant.Core.Repositories;
using Service.Tenant.Core.Repositories.Interfaces;

namespace Service.Tenant.WebApi.Controllers.Service
{
    [Route("serviceapi/tenant")]
    [ApiController]
    [IpRangeCheck]
    [ServiceApiBasicAuth]
    public class ServiceTenantController : ControllerBase
    {
        private readonly ITenantsRepository _tenantsRepository;

        public ServiceTenantController(
            ITenantsRepository tenantsRepository)
        {
            _tenantsRepository = tenantsRepository;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get(int tenantId)
        {
            return Ok(await _tenantsRepository.GetTenant(tenantId));
        }

        [HttpGet("byname")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetTenantByName(string tenantName)
        {
            return Ok(await _tenantsRepository.GetTenantByName(tenantName));
        }

    }
}
