﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Common.WebApi.Attributes;
using System;
using AutoMapper;
using Service.Contracts;
using Service.Tenant.Core.Repositories.Interfaces;
using Service.WebApi.Services;

namespace Service.Tenant.WebApi.Controllers.Service
{
    [Route("serviceapi/authcontext")]
    [ApiController]
    [IpRangeCheck]
    [ServiceApiBasicAuth]
    public class ServiceAuthContextController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ITenantsRepository _tenantsRepository;
        private readonly ITenantUsersRepository _tenantUsersRepository;
        private readonly IAuthContextService _authContextService;

        public ServiceAuthContextController(
            IMapper mapper,
            ITenantsRepository tenantsRepository,
            ITenantUsersRepository tenantUsersRepository,
            IAuthContextService authContextService)
        {
            _mapper = mapper;
            _tenantsRepository = tenantsRepository;
            _tenantUsersRepository = tenantUsersRepository;
            _authContextService = authContextService;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get(string tenantName, Guid authUserId)
        {
            if (string.IsNullOrEmpty(tenantName) || authUserId == Guid.Empty)
            {
                return NotFound();
            }

            var tenantUser = await _tenantUsersRepository.GetTenantUserByTenantNameAndUniqueIdentifier(tenantName, authUserId);
            var tenant = await _tenantsRepository.GetTenant(tenantUser.TenantId);

            var authContext = new AuthContext();
            _mapper.Map(tenant, authContext.Tenant);
            _mapper.Map(tenantUser, authContext.TenantUser);

            return Ok(authContext);
        }

        [HttpGet("createIfNotExists")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetCreateIfNotExists(string tenantName, Guid authUserId)
        {
            if (string.IsNullOrEmpty(tenantName) || authUserId == Guid.Empty)
            {
                return NotFound();
            }

            return Ok(await _authContextService.FetchOrCreateContext(tenantName, authUserId));
        }

    }
}
