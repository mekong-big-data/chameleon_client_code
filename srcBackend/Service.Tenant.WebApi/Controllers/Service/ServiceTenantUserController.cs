﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Common.WebApi.Attributes;
using Service.Tenant.Core.Repositories.Interfaces;

namespace Service.Tenant.WebApi.Controllers.Service
{
    [Route("serviceapi/tenantuser")]
    [ApiController]
    [IpRangeCheck]
    [ServiceApiBasicAuth]
    public class ServiceTenantUserController : ControllerBase
    {
        private readonly ITenantUsersRepository _tenantUsersRepository;

        public ServiceTenantUserController(
            ITenantUsersRepository tenantUsersRepository)
        {
            _tenantUsersRepository = tenantUsersRepository;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get(int tenantUserId)
        {
            return Ok(await _tenantUsersRepository.GetTenantUser(tenantUserId));
        }

    }
}
