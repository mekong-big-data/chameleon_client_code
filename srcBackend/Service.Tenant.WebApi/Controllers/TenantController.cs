﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Service.Tenant.Contracts.Models;
using Service.Tenant.Core.Repositories;
using Common.Core.Storage;
using Service.Contracts.Types;
using Service.Tenant.Core.Constants;
using Service.Tenant.Core.Repositories.Interfaces;
using Service.WebApi.Security;
using Service.WebApi.Services;

namespace Service.Tenant.WebApi.Controllers
{
    [Authorize(Policy = AccessPolicies.IsSuperAdmin)]
    [Route("api/[controller]")]
    [ApiController]
    public class TenantController : ControllerBase
    {
        private readonly ITenantsRepository _tenantsRepository;
        private readonly ITenantUsersRepository _tenantUsersRepository;
        private readonly IAuthContextService _authContextService;
        private readonly IBlobStorage _blobStorage;

        public TenantController(
            ITenantsRepository tenantsRepository,
            ITenantUsersRepository tenantUsersRepository,
            IAuthContextService authContextService,
            IBlobStorage blobStorage)
        {
            _tenantsRepository = tenantsRepository;
            _tenantUsersRepository = tenantUsersRepository;
            _authContextService = authContextService;
            _blobStorage = blobStorage;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetTenants()
        {
            return Ok(await _tenantsRepository.GetTenants());
        }

        [HttpGet]
        [Route("{tenantId}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetTenant(int tenantId)
        {
            var tenant = await _tenantsRepository.GetTenant(tenantId);
            if (!string.IsNullOrEmpty(tenant.PrimaryLogoFilePath))
            {
                tenant.PrimaryLogoFileName = tenant.PrimaryLogoFilePath;
                tenant.PrimaryLogoFilePath = _blobStorage.GenerateBlobUrl(tenant.PrimaryLogoFilePath);
            }
            if (!string.IsNullOrEmpty(tenant.CertificateFilePath))
            {
                tenant.CertificateFileName = tenant.CertificateFilePath;
                tenant.CertificateFilePath = _blobStorage.GenerateBlobUrl(tenant.CertificateFilePath);
            }
            if (!string.IsNullOrEmpty(tenant.PromotionalVideoFilePath))
            {
                tenant.PromotionalVideoFileName = tenant.PromotionalVideoFilePath;
                tenant.PromotionalVideoFilePath = _blobStorage.GenerateBlobUrl(tenant.PromotionalVideoFilePath);
            }
            tenant.TinEyeAccountPassword = new string('*', tenant.TinEyeAccountPassword.Length);
            return Ok(tenant);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Post(TenantModel model)
        {
            if (!string.IsNullOrEmpty(model.PrimaryLogoFilePath))
                model.PrimaryLogoFilePath = $"{BlobContainers.TenantContainer}/{model.PrimaryLogoFilePath}";

            if (!string.IsNullOrEmpty(model.CertificateFilePath))
                model.CertificateFilePath = $"{BlobContainers.TenantContainer}/{model.CertificateFilePath}";

            if (!string.IsNullOrEmpty(model.PromotionalVideoFilePath))
                model.PromotionalVideoFilePath = $"{BlobContainers.TenantContainer}/{model.PromotionalVideoFilePath}";

            var newTenant = await _tenantsRepository.CreateTenant(model);

            // add current super admin to the tenant as an admin
            var currentTenantUser = _authContextService.AuthContext.TenantUser;
            await _tenantUsersRepository.CreateTenantUser(new TenantUserModel
            {
                TenantId = newTenant.TenantId,
                EmailAddress = currentTenantUser.EmailAddress,
                UniqueIdentifier = currentTenantUser.UniqueIdentifier,
                Role = RoleType.Admin
            });

            return Ok(newTenant);
        }

        [HttpPut]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Update(TenantModel model)
        {
            if (!string.IsNullOrEmpty(model.PrimaryLogoFilePath))
                model.PrimaryLogoFilePath = $"{BlobContainers.TenantContainer}/{model.PrimaryLogoFilePath}";

            if (!string.IsNullOrEmpty(model.CertificateFilePath))
                model.CertificateFilePath = $"{BlobContainers.TenantContainer}/{model.CertificateFilePath}";

            if (!string.IsNullOrEmpty(model.PromotionalVideoFilePath))
                model.PromotionalVideoFilePath = $"{BlobContainers.TenantContainer}/{model.PromotionalVideoFilePath}";

            var tenant = await _tenantsRepository.GetTenant(model.TenantId);
            if (model.TinEyeAccountPassword == new string('*', tenant.TinEyeAccountPassword.Length))
            {
                // password not changed so set to original before update
                model.TinEyeAccountPassword = tenant.TinEyeAccountPassword;
            }

            return Ok(await _tenantsRepository.UpdateTenant(model));
        }

        [HttpDelete]
        [Route("{tenantId}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> DeleteTenant(int tenantId)
        {
            return Ok(await _tenantsRepository.DeleteTenant(tenantId));
        }
    }
}
