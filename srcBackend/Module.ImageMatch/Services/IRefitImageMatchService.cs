﻿using System.Threading.Tasks;
using Module.ImageMatch.Models;
using Refit;

namespace Module.ImageMatch.Services
{
    public interface IRefitImageMatchService
    {
        [Get("/api/fmatchimm")]
        Task<ImageMatchSearchResponse> SearchImage(string img, string cid, string iid, string code);

    }
}
