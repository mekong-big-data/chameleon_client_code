﻿using System;
using System.Threading.Tasks;
using Module.ImageMatch.Models;
using Refit;

namespace Module.ImageMatch.Services
{
    public class ImageMatchService : IImageMatchService
    {
        private const string ApiBaseUrl = "https://imagematchcontainerfunctions.azurewebsites.net";
        private const string ApiCodeQueryString = "gdTPd8z3tAeMvsEHaoMcH8JuvV9zRdSFOqyAsAnYP901QAMosNi5Tg==";


        public async Task<ImageMatchSearchResponse> SearchImage(string imageUrl, string tenantCode, string imageKey)
        {
            var imageMatchService = RestService.For<IRefitImageMatchService>(ApiBaseUrl);

            try
            {
                return await imageMatchService.SearchImage(imageUrl, tenantCode, imageKey, ApiCodeQueryString);
            }
            catch (ApiException apiEx)
            {
                if (apiEx.StatusCode.ToString() == "400")
                {
                    return new ImageMatchSearchResponse()
                    {
                        Status = "fail",
                        Result = new ImageMatchSearchResult[0],
                        Errors = new string[0]
                    };
                }
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
