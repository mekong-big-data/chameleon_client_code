﻿using System.Threading.Tasks;
using Module.ImageMatch.Models;

namespace Module.ImageMatch.Services
{
    public interface IImageMatchService
    {
        Task<ImageMatchSearchResponse> SearchImage(string imageUrl, string tenantCode, string imageKey);
    }
}
