﻿using Newtonsoft.Json;

namespace Module.ImageMatch.Models
{
    public class ImageMatchSearchResponse
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "error")]
        public string[] Errors { get; set; }

        [JsonProperty(PropertyName = "result")]
        public ImageMatchSearchResult[] Result { get; set; }
    }

    public class ImageMatchSearchResult
    {
        [JsonProperty(PropertyName = "match")]
        public string IsMatch { get; set; }

        [JsonProperty(PropertyName = "distance")]
        public decimal Distance { get; set; }

        [JsonProperty(PropertyName = "threshold")]
        public decimal Threshold { get; set; }

        [JsonProperty(PropertyName = "image_client")]
        public ImageMatchSearchResultImageClient ImageClient { get; set; }

        [JsonProperty(PropertyName = "image_generated")]
        public ImageMatchSearchResultImageGenerated ImageGenerated { get; set; }
    }

    public class ImageMatchSearchResultImageClient
    {
        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }
    }

    public class ImageMatchSearchResultImageGenerated
    {
        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; }
    }
}
