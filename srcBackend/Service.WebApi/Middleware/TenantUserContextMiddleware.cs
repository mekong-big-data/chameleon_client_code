﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Service.WebApi.Services;

namespace Service.WebApi.Middleware
{
    public class TenantUserContextMiddleware
    {
        private const string TenantHeaderKey = "tenant";

        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public TenantUserContextMiddleware(RequestDelegate next, ILogger logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context, IAuthContextService userContextService)
        {
            var tenantName = context.Request.Headers[TenantHeaderKey];
            var authUserIdValue = context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            if (!string.IsNullOrEmpty(tenantName) && Guid.TryParse(authUserIdValue, out var authUserId))
            {
                await userContextService.LoadContext(tenantName, authUserId);
                _logger.LogInformation($"Loaded user context {authUserId} for tenant {tenantName}");
            }
            await _next(context);
        }
    }
}
