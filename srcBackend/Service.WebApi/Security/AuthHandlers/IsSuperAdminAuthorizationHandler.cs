﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Service.Contracts.Types;
using Service.WebApi.Security.Requirements;
using Service.WebApi.Services;

namespace Service.WebApi.Security.AuthHandlers
{
    public class IsSuperAdminAuthorizationHandler : AuthorizationHandler<IsSuperAdminRequirement>
    {
        private readonly IAuthContextService _authContextService;

        public IsSuperAdminAuthorizationHandler(
            IAuthContextService authContextService)
        {
            _authContextService = authContextService;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsSuperAdminRequirement requirement)
        {
            if (_authContextService.AuthContext != null && _authContextService.AuthContext.TenantUser.Role == RoleType.SuperAdmin)
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }

            return Task.CompletedTask;
        }
    }
}
