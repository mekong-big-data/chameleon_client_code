﻿namespace Service.WebApi.Security
{
    public class AccessPolicies
    {
        public const string IsAdmin = "IsAdmin";
        public const string IsSuperAdmin = "IsSuperAdmin";
    }
}
