﻿using Microsoft.AspNetCore.Authorization;

namespace Service.WebApi.Security.Requirements
{
    public class IsSuperAdminRequirement : IAuthorizationRequirement
    {
    }
}
