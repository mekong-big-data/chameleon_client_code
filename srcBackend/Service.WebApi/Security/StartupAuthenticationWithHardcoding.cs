﻿using System;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Common.Core.Authentication;
using Common.Core.Authentication.Responses;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Service.WebApi.Security
{
    public class StartupAuthenticationWithHardCoding
    {

        public static void Register(
            IServiceCollection services,
            Action<AuthorizationOptions> authConfiguration)
        {
            services
                .AddAuthentication(options =>
                {
                    options.DefaultScheme = "HardCoded Scheme";
                })
                .AddHardCodedAuth(options =>
                {

                });

            services.AddAuthorization(authConfiguration);
        }

    }

    public static class HardCodedAuthenticationExtensions
    {
        public static AuthenticationBuilder AddHardCodedAuth(this AuthenticationBuilder builder, Action<HardCodedAuthenticationOptions> configureOptions)
        {
            return builder.AddScheme<HardCodedAuthenticationOptions, HardCodedAuthenticationHandler>("HardCoded Scheme", "HardCoded Auth", configureOptions);
        }
    }

    public class HardCodedAuthenticationHandler : AuthenticationHandler<HardCodedAuthenticationOptions>
    {
        public HardCodedAuthenticationHandler(
            IOptionsMonitor<HardCodedAuthenticationOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var authenticationTicket = new AuthenticationTicket(
                new ClaimsPrincipal(Options.Identity),
                new AuthenticationProperties(),
                "HardCoded Scheme");

            return Task.FromResult(AuthenticateResult.Success(authenticationTicket));
        }
    }

    public class HardCodedAuthenticationOptions : AuthenticationSchemeOptions
    {
        public virtual ClaimsIdentity Identity { get; } = new ClaimsIdentity(new []
        {
            new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", "6935459A-1FED-4217-A297-C12F2ADB2480"),
        }, "hard-code");
    }

    public class UserLookupServiceWithHardCoding : IUserLookupService
    {
        public async Task<UserLookupResponse> LookupUser(Guid id)
        {
            return await Task.FromResult(new UserLookupResponse
            {
                Id = new Guid("6935459A-1FED-4217-A297-C12F2ADB2480"),
                EmailAddress = "dan.boterhoven@gmail.com"
            });
        }
    }

}
