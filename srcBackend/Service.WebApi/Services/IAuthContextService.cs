﻿using System;
using System.Threading.Tasks;
using Service.Contracts;

namespace Service.WebApi.Services
{
    public interface IAuthContextService
    {
        AuthContext AuthContext { get; }
        Task<AuthContext> FetchOrCreateContext(string tenantName, Guid authUserId);
        Task LoadContext(string tenantName, Guid authUserId);
    }
}
