﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Service.Contracts;
using Service.Tenant.Contracts;

namespace Service.WebApi.Services
{
    public class AuthContextService : IAuthContextService
    {
        private readonly ITenantServiceApi _tenantServiceApi;

        public AuthContext AuthContext { get; private set; }

        public AuthContextService(
            ITenantServiceApi tenantServiceApi)
        {
            _tenantServiceApi = tenantServiceApi;
        }

        public Task<AuthContext> FetchOrCreateContext(string tenantName, Guid authUserId)
        {
            throw new NotImplementedException();
        }

        public async Task LoadContext(string tenantName, Guid authUserId)
        {
            if (string.IsNullOrEmpty(tenantName) || authUserId == Guid.Empty)
            {
                return;
            }

            // fetch user context from Tenant service
            AuthContext = await _tenantServiceApi.GetAuthContext(tenantName, authUserId, CancellationToken.None);
        }
    }
}
